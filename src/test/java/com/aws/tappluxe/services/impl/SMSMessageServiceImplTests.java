package com.aws.tappluxe.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.aws.tappluxe.data.entities.SMSMessage;
import com.aws.tappluxe.services.SMSMessageService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@ActiveProfiles("test")
public class SMSMessageServiceImplTests {
	
	@Autowired
	public SMSMessageService smsService;
	
	
	/*
	 * Twilio Tests: https://www.twilio.com/docs/api/rest/test-credentials
	 */
	@Test
	public void testSendingMessage() {
		assertTrue(smsService.sendSMSMessage("+639278940365", "TEST"));
	}
	
	@Test
	public void testSendingMessageNonNumber() {
		assertFalse(smsService.sendSMSMessage("+15005550009", "TEST"));
	}
	
	@Test
	public void testSendingMessageEmpty() {
		assertFalse(smsService.sendSMSMessage("+639278940365", ""));
	}
	// End of Twilio tests
	
	@Test
	public void testCreateMessage() {
		String testNumber = "+639278940365";
		String testText = "TEST";
		SMSMessage sms = smsService.createSMSMessage(testNumber, testText);
		assertNotNull(sms);
		assertEquals(sms.getText(), testText);
		assertEquals(sms.getToNumber(), testNumber);
	}
}
