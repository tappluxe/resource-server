package com.aws.tappluxe.services.impl;

import static org.junit.Assert.assertEquals;

import com.aws.tappluxe.data.entities.*;
import com.aws.tappluxe.exceptions.InvalidFormException;
import com.aws.tappluxe.services.EmailService;
import com.aws.tappluxe.services.SMSMessageService;
import com.aws.tappluxe.services.VerificationCodeService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.aws.tappluxe.data.dao.ManufacturedUnitDao;
import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dao.UserUnitDao;
import com.aws.tappluxe.data.dto.requests.ResetPasswordRequest;
import com.aws.tappluxe.data.dto.requests.UserCreateRequest;
import com.aws.tappluxe.data.dto.requests.UserUnitCreateRequest;
import org.springframework.validation.BindingResult;

import java.util.Date;

@RunWith(MockitoJUnitRunner.class)
public class MembershipServiceImplTests  {
	
	@InjectMocks
	private MembershipServiceImpl membershipService;
	
	@Mock
	private UserDao userDao;
	
	@Mock
	private ManufacturedUnitDao manufacturedUnitDao;
	
	@Mock
	private UserUnitDao userUnitDao;
	
	@Mock
	private BCryptPasswordEncoder encoder;
	
	@Mock
	private UserUnitSocketServiceImpl userUnitSocketService;

	@Mock
    private BindingResult bindingResult;

	@Mock
    private MessageSource msgSource;

	@Mock
    private ApplicationEventPublisher eventPublisher;
	
	@Mock
    private VerificationCodeService verificationCodeService;
	
	@Mock
    private EmailService emailService;
	
	@Mock
    private SMSMessageService smsMessageService;
	
	private QUser userPredicate;
	
	private QUserUnit userUnitPredicate;

	private QManufacturedUnit manufacturedUnitPredicate;
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		userPredicate = QUser.user;
		userUnitPredicate = QUserUnit.userUnit;
		manufacturedUnitPredicate = QManufacturedUnit.manufacturedUnit;
	}
	
	@Test(expected = InvalidFormException.class)
	public void testRegisterEmailExists() {
		String emailMock = "test@test.com";
		String passwordMock = "To@st1";
		String confirmPasswordMock = "test";
		String firstNameMock = "test";
		String lastNameMock = "test";
		String phoneMock = "4444-444-4444";
		String uuidMock = "637be8fa-692c-4a2f-a504-134628468c8a";
		String uNameMock = "test";
		UserUnitCreateRequest uucMock = new UserUnitCreateRequest(uuidMock, uNameMock);
		UserCreateRequest ucMock = new UserCreateRequest(emailMock, passwordMock, confirmPasswordMock, firstNameMock, lastNameMock, phoneMock, uucMock);
		Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(emailMock));
		Mockito.doReturn(true).when(bindingResult).hasErrors();
		membershipService.register(ucMock, bindingResult).getStatusCode();
	}
	
	@Test(expected = InvalidFormException.class)
	public void testRegisterPasswordInvalid() {
		String emailMock = "test@test.com";
		String passwordMock = "test";
		String confirmPasswordMock = "test";
		String firstNameMock = "test";
		String lastNameMock = "test";
		String phoneMock = "4444-444-4444";
		String uuidMock = "637be8fa-692c-4a2f-a504-134628468c8a";
		String uNameMock = "test";
		UserUnitCreateRequest uucMock = new UserUnitCreateRequest(uuidMock, uNameMock);
		UserCreateRequest ucMock = new UserCreateRequest(emailMock, passwordMock, confirmPasswordMock, firstNameMock, lastNameMock, phoneMock, uucMock);
		Mockito.doReturn(false).when(userDao).exists(userPredicate.email.eq(emailMock));
        Mockito.doReturn(true).when(bindingResult).hasErrors();
		membershipService.register(ucMock, bindingResult).getStatusCode();
	}
	
	@Test(expected = InvalidFormException.class)
	public void testRegisterPasswordNotMatched() {
		String emailMock = "test@test.com";
		String passwordMock = "To@st1";
		String confirmPasswordMock = "test";
		String firstNameMock = "test";
		String lastNameMock = "test";
		String phoneMock = "4444-444-4444";
		String uuidMock = "637be8fa-692c-4a2f-a504-134628468c8a";
		String uNameMock = "test";
		UserUnitCreateRequest uucMock = new UserUnitCreateRequest(uuidMock, uNameMock);
		UserCreateRequest ucMock = new UserCreateRequest(emailMock, passwordMock, confirmPasswordMock, firstNameMock, lastNameMock, phoneMock, uucMock);
		Mockito.doReturn(false).when(userDao).exists(userPredicate.email.eq(emailMock));
        Mockito.doReturn(true).when(bindingResult).hasErrors();
		membershipService.register(ucMock, bindingResult).getStatusCode();
	}

	@Test(expected = InvalidFormException.class)
    public void testRegisterProductKeyUsed() {
        String emailMock = "test@test.com";
        String passwordMock = "To@st1";
        String confirmPasswordMock = "test";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        String uuidMock = "637be8fa-692c-4a2f-a504-134628468c8a";
        String uNameMock = "test";
        UserUnitCreateRequest uucMock = new UserUnitCreateRequest(uuidMock, uNameMock);
        UserCreateRequest ucMock = new UserCreateRequest(emailMock, passwordMock, confirmPasswordMock, firstNameMock, lastNameMock, phoneMock, uucMock);
        Mockito.doReturn(false).when(userDao).exists(userPredicate.email.eq(emailMock));
        Mockito.doReturn(true).when(userUnitDao).exists(userUnitPredicate.manufacturedUnit.productKey.eq(uuidMock));
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        membershipService.register(ucMock, bindingResult).getStatusCode();
    }
	
	@Test
	public void testRegisterSuccessful() {
        Long plIdMock = 1L;
        Long uuIdMock = 1L;
		String emailMock = "test@test.com";
		String passwordMock = "To@st1";
		String confirmPasswordMock = "To@st1";
		String firstNameMock = "test";
		String lastNameMock = "test";
		String phoneMock = "4444-444-4444";
		String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
		String uNameMock = "test";
		Long userIdMock = 1L;
		ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
		User userMock = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
		UserUnitCreateRequest uucMock = new UserUnitCreateRequest(productIdMock, uNameMock);
        ManufacturedUnit muMock = new ManufacturedUnit(productIdMock, new Date(), plMock);
		UserCreateRequest ucMock = new UserCreateRequest(emailMock, passwordMock, confirmPasswordMock, firstNameMock, lastNameMock, phoneMock, uucMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMock, userMock);
		Mockito.doReturn(false).when(userDao).exists(userPredicate.email.eq(emailMock));
		Mockito.doReturn(true).when(manufacturedUnitDao).exists(manufacturedUnitPredicate.productKey.eq(productIdMock));
        Mockito.doReturn(muMock).when(manufacturedUnitDao).findOne(manufacturedUnitPredicate.productKey.eq(productIdMock));
        Mockito.doReturn(userUnitMock).when(userUnitDao).saveAndFlush(ArgumentMatchers.any(UserUnit.class));
        Mockito.doReturn("").when(msgSource).getMessage("Success.membership.create", null, LocaleContextHolder.getLocale());
        assertEquals(HttpStatus.OK, membershipService.register(ucMock, bindingResult).getStatusCode());
	}

	@Test
    public void testCheckEmailInvalid() {
	    String emailMock = "test@test.com";
        Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(emailMock));
        assertEquals(HttpStatus.NOT_FOUND, membershipService.checkEmail(emailMock).getStatusCode());
    }

    @Test
    public void testCheckEmailValid() {
        String emailMock = "test@test.com";
        assertEquals(HttpStatus.OK, membershipService.checkEmail(emailMock).getStatusCode());
    }

    @Test
    public void testCheckProductKeyNotExists() {
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        assertEquals(HttpStatus.NOT_FOUND, membershipService.checkProductId(productIdMock).getStatusCode());
    }

    @Test
    public void testCheckProductKeyUsed() {
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Mockito.doReturn(true).when(manufacturedUnitDao).exists(manufacturedUnitPredicate.productKey.eq(productIdMock));
        Mockito.doReturn(true).when(userUnitDao).exists(userUnitPredicate.manufacturedUnit.productKey.eq(productIdMock));
        assertEquals(HttpStatus.CONFLICT, membershipService.checkProductId(productIdMock).getStatusCode());
    }

    @Test
    public void testCheckProductValid() {
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Mockito.doReturn(true).when(manufacturedUnitDao).exists(manufacturedUnitPredicate.productKey.eq(productIdMock));
        assertEquals(HttpStatus.OK, membershipService.checkProductId(productIdMock).getStatusCode());
    }

    @Test
    public void testForgotPasswordEmailNotExists() {
        String emailMock = "test@test.com";
        assertEquals(HttpStatus.NOT_FOUND, membershipService.forgotPassword(emailMock).getStatusCode());
    }

    @Test
    public void testForgotPasswordCodeGenFailed() {
    	String emailMock = "test@test.com";
    	User mockUser = new User();
    	Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(emailMock));
    	Mockito.doReturn(mockUser).when(userDao).findOne(userPredicate.email.eq(emailMock));
    	Mockito.doReturn(null).when(verificationCodeService).generateVerificationCode(emailMock);
    	assertEquals(HttpStatus.PRECONDITION_FAILED, membershipService.forgotPassword(emailMock).getStatusCode());	
    }
    
    @Test
    public void testForgotPasswordCodeGenSuccess() {
    	String emailMock = "test@test.com";
    	User mockUser = new User();
    	mockUser.setEmail(emailMock);
    	VerificationCode code = new VerificationCode();
    	code.setCode("1234");
    	Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(emailMock));
    	Mockito.doReturn(mockUser).when(userDao).findOne(userPredicate.email.eq(emailMock));
    	Mockito.doReturn(code).when(verificationCodeService).generateVerificationCode(emailMock);
    	assertEquals(HttpStatus.OK, membershipService.forgotPassword(emailMock).getStatusCode());
    	
    }
    
    @Test
    public void testResetPasswordEmailNotExists() {
        String emailMock = "test@test.com";
        String testPass = "Awsys+123";
        ResetPasswordRequest request = new ResetPasswordRequest(emailMock, testPass, testPass);
        assertEquals(HttpStatus.NOT_FOUND, membershipService.resetPassword(request).getStatusCode());
    }
    
    @Test
    public void testResetPasswordNotMatch() {    	
        String emailMock = "test@test.com";
        String testPass = "Awsys+123";
        String testConfirmPass = "Awsys+987";
        ResetPasswordRequest request = new ResetPasswordRequest(emailMock, testPass, testConfirmPass);        
        Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(emailMock));
        assertEquals(HttpStatus.PRECONDITION_FAILED, membershipService.resetPassword(request).getStatusCode());
    }
    
    @Test
    public void testResetPasswordMatch() {    	
        String emailMock = "test@test.com";
        String testPass = "Awsys+123";
        User mockUser = new User();
        ResetPasswordRequest request = new ResetPasswordRequest(emailMock, testPass, testPass);
        Mockito.doReturn(mockUser).when(userDao).findOne(userPredicate.email.eq(emailMock));
        Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(emailMock));
        assertEquals(HttpStatus.OK, membershipService.resetPassword(request).getStatusCode());
    }
	
}
