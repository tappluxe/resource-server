package com.aws.tappluxe.services.impl;

import com.aws.tappluxe.data.dao.ScheduleDao;
import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dao.UserUnitSocketDao;
import com.aws.tappluxe.data.dto.requests.ScheduleCreateRequest;
import com.aws.tappluxe.data.dto.requests.ScheduleUpdateRequest;
import com.aws.tappluxe.data.dto.responses.ScheduleDetailsResponse;
import com.aws.tappluxe.data.entities.*;
import com.aws.tappluxe.exceptions.InvalidFormException;
import com.aws.tappluxe.services.utils.EntityDtoConverter;
import com.querydsl.core.BooleanBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ScheduleServiceImplTests {

    @InjectMocks
    private ScheduleServiceImpl scheduleService;

    @Mock
    private UserDao userDao;

    @Mock
    private ScheduleDao scheduleDao;

    @Mock
    private UserUnitSocketDao userUnitSocketDao;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private MessageSource msgSource;

    private QUser userPredicate;

    private QSchedule schedulePredicate;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.userPredicate = QUser.user;
        this.schedulePredicate = QSchedule.schedule;
    }

    @Test
    public void testAll() {
        Long idMock1 = 1L;
        String nameMock1 = "test1";
        String repeatMock1 = "1|2";
        Integer timeBeforeNotifMock1 = 5;
        Boolean notifEnabledMock1 = true;
        Long idMock2 = 2L;
        String nameMock2 = "test2";
        String repeatMock2 = "2|3";
        Integer timeBeforeNotifMock2 = 0;
        Boolean notifEnabledMock2 = false;
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        Schedule scheduleMock1 = new Schedule(idMock1, nameMock1, new Date(), repeatMock1, timeBeforeNotifMock1, notifEnabledMock1, user);
        Schedule scheduleMock2 = new Schedule(idMock2, nameMock2, new Date(), repeatMock2, timeBeforeNotifMock2, notifEnabledMock2, user);
        List<Schedule> mockedSchedules = new ArrayList<>();
        mockedSchedules.add(scheduleMock1);
        mockedSchedules.add(scheduleMock2);
        List<ScheduleDetailsResponse> mockedResponse = new ArrayList<>();
        mockedResponse.add(EntityDtoConverter.toDto(scheduleMock1));
        mockedResponse.add(EntityDtoConverter.toDto(scheduleMock2));
        Mockito.doReturn(mockedSchedules).when(scheduleDao).findAll(schedulePredicate.user.email.eq(emailMock));
        assertEquals(mockedResponse, scheduleService.all(emailMock).getBody());
    }

    @Test(expected = InvalidFormException.class)
    public void testCreateInvalidForm() {
        String emailMock = "test1@test.com";
        String nameMock = "test1";
        String repeatMock = "1|2";
        Integer timeBeforeNotifMock = 5;
        Boolean notifEnabledMock = true;
        ScheduleCreateRequest requestMock = new ScheduleCreateRequest(nameMock, new Date(), repeatMock, timeBeforeNotifMock, notifEnabledMock);
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        scheduleService.create(requestMock, emailMock, bindingResult);
    }

    @Test
    public void testCreateSuccess() {
        String emailMock = "test1@test.com";
        String nameMock = "test1";
        String repeatMock = "1|2";
        Integer timeBeforeNotifMock = 5;
        Boolean notifEnabledMock = true;
        ScheduleCreateRequest requestMock = new ScheduleCreateRequest(nameMock, new Date(), repeatMock, timeBeforeNotifMock, notifEnabledMock);
        assertEquals(HttpStatus.OK, scheduleService.create(requestMock, emailMock, bindingResult).getStatusCode());
    }

    @Test
    public void testShowNotExists() {
        Long idMock = 2L;
        String emailMock = "test1@test.com";
        assertEquals(HttpStatus.NOT_FOUND, scheduleService.show(idMock, emailMock).getStatusCode());
    }

    @Test
    public void testShowSuccess() {
        Long idMock = 2L;
        String nameMock = "test2";
        String repeatMock = "2|3";
        Integer timeBeforeNotifMock = 0;
        Boolean notifEnabledMock = false;
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        Schedule scheduleMock = new Schedule(idMock, nameMock, new Date(), repeatMock, timeBeforeNotifMock, notifEnabledMock, user);
        BooleanBuilder booleanPredicate = new BooleanBuilder(schedulePredicate.id.eq(idMock));
        booleanPredicate.and(schedulePredicate.user.email.eq(emailMock));
        Mockito.doReturn(true).when(scheduleDao).exists(booleanPredicate);
        Mockito.doReturn(scheduleMock).when(scheduleDao).findOne(booleanPredicate);
        assertEquals(EntityDtoConverter.toDto(scheduleMock), scheduleService.show(idMock, emailMock).getBody());
    }

    @Test
    public void testUpdateNotExists() {
        Long idMock = 2L;
        String emailMock = "test1@test.com";
        String nameMock = "test1";
        String repeatMock = "1|2";
        Integer timeBeforeNotifMock = 5;
        Boolean notifEnabledMock = true;
        Boolean activeMock = true;
        ScheduleUpdateRequest requestMock = new ScheduleUpdateRequest(nameMock, new Date(), repeatMock, timeBeforeNotifMock, notifEnabledMock, activeMock);
        assertEquals(HttpStatus.NOT_FOUND, scheduleService.update(idMock, emailMock, requestMock).getStatusCode());
    }

    @Test
    public void testUpdateSuccess() {
        Long idMock = 2L;
        String nameMock = "test1";
        String repeatMock = "1|2";
        Integer timeBeforeNotifMock = 5;
        Boolean notifEnabledMock = true;
        Boolean activeMock = true;
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ScheduleUpdateRequest requestMock = new ScheduleUpdateRequest(nameMock, new Date(), repeatMock, timeBeforeNotifMock, notifEnabledMock, activeMock);
        Schedule scheduleMock = new Schedule(idMock, nameMock, new Date(), repeatMock, timeBeforeNotifMock, notifEnabledMock, user);
        BooleanBuilder booleanPredicate = new BooleanBuilder(schedulePredicate.id.eq(idMock));
        booleanPredicate.and(schedulePredicate.user.email.eq(emailMock));
        Mockito.doReturn(true).when(scheduleDao).exists(booleanPredicate);
        Mockito.doReturn(scheduleMock).when(scheduleDao).findOne(booleanPredicate);
        assertEquals(HttpStatus.OK, scheduleService.update(idMock, emailMock, requestMock).getStatusCode());
    }

    @Test
    public void testDeleteNotExists() {
        Long idMock = 2L;
        String emailMock = "test1@test.com";
        assertEquals(HttpStatus.NOT_FOUND, scheduleService.delete(idMock, emailMock).getStatusCode());
    }

    @Test
    public void testDeleteSuccess() {
        Long idMock = 2L;
        String nameMock = "test1";
        String repeatMock = "1|2";
        Integer timeBeforeNotifMock = 5;
        Boolean notifEnabledMock = true;
        Boolean activeMock = true;
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        Schedule scheduleMock = new Schedule(idMock, nameMock, new Date(), repeatMock, timeBeforeNotifMock, notifEnabledMock, user);
        BooleanBuilder booleanPredicate = new BooleanBuilder(schedulePredicate.id.eq(idMock));
        booleanPredicate.and(schedulePredicate.user.email.eq(emailMock));
        Mockito.doReturn(scheduleMock).when(scheduleDao).findOne(booleanPredicate);
        assertEquals(HttpStatus.OK, scheduleService.delete(idMock, emailMock).getStatusCode());
    }

    @Test
    public void testDeleteWithSocketSuccess() {
        Long idMock = 2L;
        String nameMock = "test1";
        String repeatMock = "1|2";
        Integer timeBeforeNotifMock = 5;
        Boolean notifEnabledMock = true;
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, null, null);
        Schedule scheduleMock = new Schedule(idMock, nameMock, new Date(), repeatMock, timeBeforeNotifMock, notifEnabledMock, user);
        List<UserUnitSocket> mockedSockets = new ArrayList<>();
        mockedSockets.add(socketMock1);
        scheduleMock.setUserUnitSockets(mockedSockets);
        BooleanBuilder booleanPredicate = new BooleanBuilder(schedulePredicate.id.eq(idMock));
        booleanPredicate.and(schedulePredicate.user.email.eq(emailMock));
        Mockito.doReturn(scheduleMock).when(scheduleDao).findOne(booleanPredicate);
        assertEquals(HttpStatus.OK, scheduleService.delete(idMock, emailMock).getStatusCode());
    }

}
