package com.aws.tappluxe.services.impl;

import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dto.requests.UserUpdateRequest;
import com.aws.tappluxe.data.dto.responses.UserDetailsResponse;
import com.aws.tappluxe.data.entities.QUser;
import com.aws.tappluxe.data.entities.User;
import com.aws.tappluxe.exceptions.InvalidFormException;
import com.aws.tappluxe.services.utils.EntityDtoConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests  {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserDao userDao;

    @Mock
    private BCryptPasswordEncoder encoder;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private MessageSource msgSource;

    private QUser userPredicate;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.userPredicate = QUser.user;
    }

    @Test
    public void testAll() {
        Long userIdMock1 = 1L;
        String emailMock1 = "test1@test.com";
        String passwordMock1 = "To@st1";
        String firstNameMock1 = "test";
        String lastNameMock1 = "test";
        String phoneMock1 = "4444-444-4444";
        Long userIdMock2 = 2L;
        String emailMock2 = "test2@test.com";
        String passwordMock2 = "To@st1";
        String firstNameMock2 = "test";
        String lastNameMock2 = "test";
        String phoneMock2 = "4444-444-4444";
        User user1 = new User(userIdMock1, emailMock1, passwordMock1, firstNameMock1, lastNameMock1, phoneMock1, new Date());
        User user2 = new User(userIdMock2, emailMock2, passwordMock2, firstNameMock2, lastNameMock2, phoneMock2, new Date());
        List<User> mockedUsers = new ArrayList<>();
        mockedUsers.add(user1);
        mockedUsers.add(user2);
        List<UserDetailsResponse> mockedResponse = new ArrayList<>();
        mockedResponse.add(EntityDtoConverter.toDto(user1));
        mockedResponse.add(EntityDtoConverter.toDto(user2));
        Mockito.doReturn(mockedUsers).when(userDao).findAll();
        assertEquals(mockedResponse, userService.all().getBody());
    }

    @Test
    public void testShowNotExists() {
        String emailMock = "test1@test.com";
        assertEquals(HttpStatus.NOT_FOUND, userService.show(emailMock).getStatusCode());
    }

    @Test
    public void testShowSuccess() {
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(emailMock));
        Mockito.doReturn(user).when(userDao).findOne(userPredicate.email.eq(emailMock));
        assertEquals(EntityDtoConverter.toDto(user), userService.show(emailMock).getBody());
    }

    @Test
    public void testUpdateNotExists() {
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        String newPasswordMock = "Awsys+123";
        String confirmPasswordMock = "Awsys+123";
        UserUpdateRequest requestMock = new UserUpdateRequest(emailMock, passwordMock, newPasswordMock, confirmPasswordMock, firstNameMock, lastNameMock, phoneMock);
        assertEquals(HttpStatus.NOT_FOUND, userService.update(emailMock, requestMock, bindingResult).getStatusCode());
    }

    @Test(expected = InvalidFormException.class)
    public void testUpdateEmailConflict() {
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        String newEmailMock = "test2@test.com";
        String newPasswordMock = "Awsys+123";
        String confirmPasswordMock = "Awsys+123";
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        UserUpdateRequest requestMock = new UserUpdateRequest(newEmailMock, passwordMock, newPasswordMock, confirmPasswordMock, firstNameMock, lastNameMock, phoneMock);
        Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(emailMock));
        Mockito.doReturn(user).when(userDao).findOne(userPredicate.email.eq(emailMock));
        Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(newEmailMock));
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        userService.update(emailMock, requestMock, bindingResult);
    }

    @Test(expected = InvalidFormException.class)
    public void testUpdateOldPasswordNotMatched() {
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        String newEmailMock = "test2@test.com";
        String oldPasswordMock = "Toost1";
        String newPasswordMock = "Awsys+123";
        String confirmPasswordMock = "Awsys+123";
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        UserUpdateRequest requestMock = new UserUpdateRequest(newEmailMock, oldPasswordMock, newPasswordMock, confirmPasswordMock, firstNameMock, lastNameMock, phoneMock);
        Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(emailMock));
        Mockito.doReturn(user).when(userDao).findOne(userPredicate.email.eq(emailMock));
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        userService.update(emailMock, requestMock, bindingResult);
    }

    @Test
    public void testUpdateSuccess() {
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        String newEmailMock = "test2@test.com";
        String oldPasswordMock = "Toost1";
        String newPasswordMock = "Awsys+123";
        String confirmPasswordMock = "Awsys+123";
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        UserUpdateRequest requestMock = new UserUpdateRequest(newEmailMock, oldPasswordMock, newPasswordMock, confirmPasswordMock, firstNameMock, lastNameMock, phoneMock);
        Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(emailMock));
        Mockito.doReturn(user).when(userDao).findOne(userPredicate.email.eq(emailMock));
        assertEquals(HttpStatus.OK, userService.update(emailMock, requestMock, bindingResult).getStatusCode());
    }

    @Test
    public void testDeleteNotExists() {
        String emailMock = "test1@test.com";
        assertEquals(HttpStatus.NOT_FOUND, userService.delete(emailMock).getStatusCode());
    }

    @Test
    public void testDeleteSuccess() {
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(emailMock));
        Mockito.doReturn(user).when(userDao).findOne(userPredicate.email.eq(emailMock));
        assertEquals(HttpStatus.OK, userService.show(emailMock).getStatusCode());
    }

}
