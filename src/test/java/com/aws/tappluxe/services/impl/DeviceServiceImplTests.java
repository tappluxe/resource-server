package com.aws.tappluxe.services.impl;

import com.aws.tappluxe.data.dao.*;
import com.aws.tappluxe.data.dto.requests.DeviceCreateRequest;
import com.aws.tappluxe.data.dto.requests.DeviceUpdateRequest;
import com.aws.tappluxe.data.dto.responses.DeviceDetailsResponse;
import com.aws.tappluxe.data.entities.*;
import com.aws.tappluxe.exceptions.InvalidFormException;
import com.aws.tappluxe.services.utils.EntityDtoConverter;
import com.querydsl.core.BooleanBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class DeviceServiceImplTests {

    @InjectMocks
    private DeviceServiceImpl deviceService;

    @Mock
    private UserUnitSocketDao userUnitSocketDao;

    @Mock
    private UserUnitDao userUnitDao;

    @Mock
    private UserDao userDao;

    @Mock
    private ScheduleDao scheduleDao;

    @Mock
    private DeviceDao deviceDao;

    @Mock
    private MessageSource msgSource;

    @Mock
    private BindingResult bindingResult;

    private QDevice devicePredicate;

    private QUserUnitSocket userUnitSocketPredicate;

    private QUser userPredicate;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.devicePredicate = QDevice.device;
        this.userUnitSocketPredicate = QUserUnitSocket.userUnitSocket;
        this.userPredicate = QUser.user;
    }

    @Test
    public void testAllAttachedNull() {
        Long idMock1 = 1L;
        String nameMock1 = "test1";
        Long idMock2 = 2L;
        String nameMock2 = "test2";
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        Device device1 = new Device(idMock1, nameMock1, user, null);
        Device device2 = new Device(idMock2, nameMock2, user, socketMock1);
        List<Device> mockedDevices = new ArrayList<>();
        mockedDevices.add(device1);
        mockedDevices.add(device2);
        List<DeviceDetailsResponse>  mockedResponse = new ArrayList<>();
        mockedResponse.add(EntityDtoConverter.toDto(device1));
        mockedResponse.add(EntityDtoConverter.toDto(device2));
        BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.user.email.eq(emailMock));
        Mockito.doReturn(mockedDevices).when(deviceDao).findAll(booleanPredicate);
        assertEquals(mockedResponse, deviceService.all(null, emailMock).getBody());
    }

    @Test
    public void testAllNotAttached() {
        Long idMock1 = 1L;
        String nameMock1 = "test1";
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        Device device = new Device(idMock1, nameMock1, user, null);
        List<Device> mockedDevices = new ArrayList<>();
        mockedDevices.add(device);
        List<DeviceDetailsResponse>  mockedResponse = new ArrayList<>();
        mockedResponse.add(EntityDtoConverter.toDto(device));
        BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.user.email.eq(emailMock));
        booleanPredicate.and(devicePredicate.userUnitSocket.isNull());
        Mockito.doReturn(mockedDevices).when(deviceDao).findAll(booleanPredicate);
        assertEquals(mockedResponse, deviceService.all(false, emailMock).getBody());
    }

    @Test
    public void testAllAttached() {
        Long idMock2 = 2L;
        String nameMock2 = "test2";
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        Device device = new Device(idMock2, nameMock2, user, socketMock1);
        List<Device> mockedDevices = new ArrayList<>();
        mockedDevices.add(device);
        List<DeviceDetailsResponse>  mockedResponse = new ArrayList<>();
        mockedResponse.add(EntityDtoConverter.toDto(device));
        BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.user.email.eq(emailMock));
        booleanPredicate.and(devicePredicate.userUnitSocket.isNotNull());
        Mockito.doReturn(mockedDevices).when(deviceDao).findAll(booleanPredicate);
        assertEquals(mockedResponse, deviceService.all(true, emailMock).getBody());
    }

    @Test(expected = InvalidFormException.class)
    public void testCreateNameConflict() {
        String nameMock = "test";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long userIdMock = 1L;
        String emailMock = "test@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        DeviceCreateRequest requestMock = new DeviceCreateRequest(nameMock, socketIdMock);
        Mockito.doReturn(socketMock1).when(userUnitSocketDao).findOne(socketIdMock);
        Mockito.doReturn(true).when(deviceDao).exists(devicePredicate.name.eq(nameMock));
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        deviceService.create(requestMock, emailMock, bindingResult);
    }

    @Test(expected = InvalidFormException.class)
    public void testCreateUnitSocketNull() {
        String nameMock = "test";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long userIdMock = 1L;
        String emailMock = "test@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        DeviceCreateRequest requestMock = new DeviceCreateRequest(nameMock, socketIdMock);
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        deviceService.create(requestMock, emailMock, bindingResult);
    }

    @Test(expected = InvalidFormException.class)
    public void testCreateUnitSocketConflict() {
        Long idMock = 1L;
        String nameMock = "test";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long userIdMock = 1L;
        String emailMock = "test@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        Device deviceMock = new Device(idMock, nameMock, user, socketMock1);
        socketMock1.setDevice(deviceMock);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        DeviceCreateRequest requestMock = new DeviceCreateRequest(nameMock, socketIdMock);
        Mockito.doReturn(socketMock1).when(userUnitSocketDao).findOne(socketIdMock);
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        deviceService.create(requestMock, emailMock, bindingResult);
    }

    @Test
    public void testCreateSuccess() {
        String nameMock = "test";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long userIdMock = 1L;
        String emailMock = "test@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        DeviceCreateRequest requestMock = new DeviceCreateRequest(nameMock, socketIdMock);
        Mockito.doReturn(socketMock1).when(userUnitSocketDao).findOne(socketIdMock);
        assertEquals(HttpStatus.OK, deviceService.create(requestMock, emailMock, bindingResult).getStatusCode());
    }

    @Test
    public void testShowNotExists() {
        Long idMock = 1L;
        String emailMock = "test@test.com";
        assertEquals(HttpStatus.NOT_FOUND, deviceService.show(idMock, emailMock).getStatusCode());
    }

    @Test
    public void testShowSuccess() {
        Long idMock2 = 2L;
        String nameMock2 = "test2";
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        Device device = new Device(idMock2, nameMock2, user, socketMock1);
        BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.id.eq(idMock2));
        booleanPredicate.and(devicePredicate.user.email.eq(emailMock));
        Mockito.doReturn(device).when(deviceDao).findOne(booleanPredicate);
        assertEquals(EntityDtoConverter.toDto(device), deviceService.show(idMock2, emailMock).getBody());
    }

    @Test
    public void testUpdateDeviceNotExists() {
        Long idMock = 1L;
        String emailMock = "test@test.com";
        assertEquals(HttpStatus.NOT_FOUND, deviceService.update(idMock, emailMock, new DeviceUpdateRequest(), bindingResult).getStatusCode());
    }

    @Test(expected = InvalidFormException.class)
    public void testUpdateSameName() {
        Long idMock2 = 2L;
        String nameMock2 = "test2";
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        Device device = new Device(idMock2, nameMock2, user, socketMock1);
        DeviceUpdateRequest updateRequestMock = new DeviceUpdateRequest(nameMock2, null);
        BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.id.eq(idMock2));
        booleanPredicate.and(devicePredicate.user.email.eq(emailMock));
        Mockito.doReturn(device).when(deviceDao).findOne(booleanPredicate);
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        deviceService.update(idMock2, emailMock, updateRequestMock, bindingResult);
    }

    @Test(expected = InvalidFormException.class)
    public void testUpdateConflictName() {
        Long idMock2 = 2L;
        String nameMock1 = "test1";
        String nameMock2 = "test2";
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        Device device = new Device(idMock2, nameMock1, user, socketMock1);
        DeviceUpdateRequest updateRequestMock = new DeviceUpdateRequest(nameMock2, null);
        BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.id.eq(idMock2));
        booleanPredicate.and(devicePredicate.user.email.eq(emailMock));
        Mockito.doReturn(device).when(deviceDao).findOne(booleanPredicate);
        Mockito.doReturn(true).when(deviceDao).exists(devicePredicate.name.eq(nameMock2)
                .and(devicePredicate.ne(device))
                .and(devicePredicate.user.email.eq(emailMock)));
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        deviceService.update(idMock2, emailMock, updateRequestMock, bindingResult);
    }

    @Test(expected = InvalidFormException.class)
    public void testUpdateUnitSocketNotExists() {
        Long idMock2 = 2L;
        String nameMock1 = "test1";
        String nameMock2 = "test2";
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        Device device = new Device(idMock2, nameMock1, user, null);
        DeviceUpdateRequest updateRequestMock = new DeviceUpdateRequest(nameMock2, socketIdMock);
        BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.id.eq(idMock2));
        booleanPredicate.and(devicePredicate.user.email.eq(emailMock));
        Mockito.doReturn(device).when(deviceDao).findOne(booleanPredicate);
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        deviceService.update(idMock2, emailMock, updateRequestMock, bindingResult);
    }

    @Test(expected = InvalidFormException.class)
    public void testUpdateUnitSocketConflict() {
        Long idMock2 = 2L;
        String nameMock1 = "test1";
        String nameMock2 = "test2";
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        Device device = new Device(idMock2, nameMock1, user, socketMock1);
        socketMock1.setDevice(device);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        DeviceUpdateRequest updateRequestMock = new DeviceUpdateRequest(nameMock2, socketIdMock);
        BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.id.eq(idMock2));
        booleanPredicate.and(devicePredicate.user.email.eq(emailMock));
        Mockito.doReturn(device).when(deviceDao).findOne(booleanPredicate);
        Mockito.doReturn(socketMock1).when(userUnitSocketDao).findOne(userUnitSocketPredicate.id.eq(socketIdMock)
                .and(userUnitSocketPredicate.userUnit.user.email.eq(emailMock)));
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        deviceService.update(idMock2, emailMock, updateRequestMock, bindingResult);
    }

    @Test
    public void updateSuccessful() {
        Long idMock2 = 2L;
        String nameMock1 = "test1";
        String nameMock2 = "test2";
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        Device device = new Device(idMock2, nameMock1, user, null);
        DeviceUpdateRequest updateRequestMock = new DeviceUpdateRequest(nameMock2, socketIdMock);
        BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.id.eq(idMock2));
        booleanPredicate.and(devicePredicate.user.email.eq(emailMock));
        Mockito.doReturn(device).when(deviceDao).findOne(booleanPredicate);
        Mockito.doReturn(socketMock1).when(userUnitSocketDao).findOne(userUnitSocketPredicate.id.eq(socketIdMock)
                .and(userUnitSocketPredicate.userUnit.user.email.eq(emailMock)));
        assertEquals(HttpStatus.OK, deviceService.update(idMock2, emailMock, updateRequestMock, bindingResult).getStatusCode());
    }

    @Test
    public void testUpdateRemoveFromSocket() {
        Long idMock2 = 2L;
        String nameMock1 = "test1";
        String nameMock2 = "test2";
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        Device device = new Device(idMock2, nameMock1, user, null);
        DeviceUpdateRequest updateRequestMock = new DeviceUpdateRequest(nameMock2, -1L);
        BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.id.eq(idMock2));
        booleanPredicate.and(devicePredicate.user.email.eq(emailMock));
        Mockito.doReturn(device).when(deviceDao).findOne(booleanPredicate);
        assertEquals(HttpStatus.OK, deviceService.update(idMock2, emailMock, updateRequestMock, bindingResult).getStatusCode());
    }

    @Test
    public void testDeleteNotExists() {
        Long idMock = 2L;
        String emailMock = "test1@test.com";
        assertEquals(HttpStatus.NOT_FOUND, deviceService.delete(idMock, emailMock).getStatusCode());
    }

    @Test
    public void testDeleteSuccessful() {Long idMock2 = 2L;
        String nameMock1 = "test1";
        Long userIdMock = 1L;
        String emailMock = "test1@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        User user = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, user);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        Device device = new Device(idMock2, nameMock1, user, null);
        BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.id.eq(idMock2));
        booleanPredicate.and(devicePredicate.user.email.eq(emailMock));
        Mockito.doReturn(device).when(deviceDao).findOne(booleanPredicate);
        assertEquals(HttpStatus.OK, deviceService.delete(idMock2, emailMock).getStatusCode());
    }

}
