package com.aws.tappluxe.services.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import com.aws.tappluxe.data.entities.ManufacturedUnit;
import com.aws.tappluxe.exceptions.InvalidFormException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;

import com.aws.tappluxe.data.dao.ProductLineDao;
import com.aws.tappluxe.data.dto.requests.ProductLineCreateRequest;
import com.aws.tappluxe.data.dto.requests.ProductLineUpdateRequest;
import com.aws.tappluxe.data.dto.responses.ProductLineDetailsResponse;
import com.aws.tappluxe.data.entities.ProductLine;
import com.aws.tappluxe.data.entities.QProductLine;
import com.aws.tappluxe.services.utils.EntityDtoConverter;
import org.springframework.validation.BindingResult;

@RunWith(MockitoJUnitRunner.class)
public class ProductLineServiceImplTests {

	@InjectMocks
	private ProductLineServiceImpl productLineService;
	
	@Mock
	private ProductLineDao productLineDao;

	@Mock
    private BindingResult bindingResult;

	@Mock
    private MessageSource msgSource;
	
	private QProductLine productLinePredicate;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.productLinePredicate = QProductLine.productLine;
	}
	
	@Test
	public void testAll() {
		List<ProductLine> mockedProductLine = new ArrayList<>();
		ProductLine plMock1 = new ProductLine(1L, "Test1", 1, 500);
		ProductLine plMock2 = new ProductLine(2L, "Test2", 2, 500);		
		mockedProductLine.add(plMock1);
		mockedProductLine.add(plMock2);
		Mockito.doReturn(mockedProductLine).when(productLineDao).findAll();
		List<ProductLineDetailsResponse> mockedResponseBody = new ArrayList<>();
		mockedProductLine.forEach(productLine -> {
			mockedResponseBody.add(EntityDtoConverter.toDto(productLine));
		});
		assertEquals(mockedResponseBody, productLineService.all().getBody());
	}
	
	@Test
	public void testCreateExists() {
		ProductLineCreateRequest plcMock = new ProductLineCreateRequest("Test", 2, 500);
		Mockito.doReturn(true).when(productLineDao).exists(productLinePredicate.name.eq(plcMock.getName()));
		Mockito.doReturn("").when(msgSource).getMessage("Conflict.productLine.name", null, LocaleContextHolder.getLocale());
		assertEquals(HttpStatus.CONFLICT, productLineService.create(plcMock, bindingResult).getStatusCode());
	}
	
	@Test(expected = InvalidFormException.class)
	public void testCreateInvalidSockets() {
		ProductLineCreateRequest plcMock = new ProductLineCreateRequest("Test", -1, 500);
		Mockito.doReturn(true).when(bindingResult).hasErrors();
		productLineService.create(plcMock, bindingResult);
	}
	
	@Test(expected = InvalidFormException.class)
	public void testCreateInvalidCapacity() {
		ProductLineCreateRequest plcMock = new ProductLineCreateRequest("Test", 2, 0);
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        productLineService.create(plcMock, bindingResult);
	}
	
	@Test
	public void testCreateSuccessful() {
		ProductLineCreateRequest plcMock = new ProductLineCreateRequest("Test", 2, 500);
		assertEquals(HttpStatus.OK, productLineService.create(plcMock, bindingResult).getStatusCode());
	}
	
	@Test
	public void testShowNotExists() {
		Long idMock = 5L;
        Mockito.doReturn(false).when(productLineDao).exists(idMock);
		assertEquals(HttpStatus.NOT_FOUND, productLineService.show(idMock).getStatusCode());
	}
	
	@Test
	public void testShowExists() {
		ProductLine plMock = new ProductLine(1L, "Test1", 1, 500);
        Mockito.doReturn(true).when(productLineDao).exists(plMock.getId());
        Mockito.doReturn(plMock).when(productLineDao).findOne(plMock.getId());
		assertEquals(EntityDtoConverter.toDto(plMock), productLineService.show(plMock.getId()).getBody());		
	}
	
	@Test
	public void testUpdateNotExists() {
		Long idMock = 5L;
		ProductLineUpdateRequest pluMock = new ProductLineUpdateRequest("Test1", 4, 500);
        Mockito.doReturn(false).when(productLineDao).exists(idMock);
		assertEquals(HttpStatus.NOT_FOUND, productLineService.update(idMock, pluMock, bindingResult).getStatusCode());
	}
	
	@Test
	public void testUpdateNameConflict() {
		Long idMock = 5L;
		ProductLineUpdateRequest pluMock = new ProductLineUpdateRequest("Test1", 4, 500);
        Mockito.doReturn(true).when(productLineDao).exists(idMock);
        Mockito.doReturn(true).when(productLineDao).exists(productLinePredicate.name.eq(pluMock.getName()));
		assertEquals(HttpStatus.CONFLICT, productLineService.update(idMock, pluMock, bindingResult).getStatusCode());
	}
	
	@Test(expected = InvalidFormException.class)
	public void testUpdateInvalidSockets() {
		Long idMock = 5L;
		ProductLineUpdateRequest pluMock = new ProductLineUpdateRequest("Test1", -1, 500);
        Mockito.doReturn(true).when(productLineDao).exists(idMock);
        Mockito.doReturn(false).when(productLineDao).exists(productLinePredicate.name.eq(pluMock.getName()));
        Mockito.doReturn(true).when(bindingResult).hasErrors();
		productLineService.update(idMock, pluMock, bindingResult);
	}
	
	@Test(expected = InvalidFormException.class)
	public void testUpdateInvalidCapacity() {
		Long idMock = 5L;
		ProductLineUpdateRequest pluMock = new ProductLineUpdateRequest("Test1", 4, 0);
        Mockito.doReturn(true).when(productLineDao).exists(idMock);
        Mockito.doReturn(false).when(productLineDao).exists(productLinePredicate.name.eq(pluMock.getName()));
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        productLineService.update(idMock, pluMock, bindingResult);
    }
	
	@Test
	public void testUpdateSuccessfulNameOnly() {
		Long idMock = 5L;
		ProductLineUpdateRequest pluMock = new ProductLineUpdateRequest("Test1", null, null);
		ProductLine plMock = new ProductLine(idMock, "Test1", 1, 500);
        Mockito.doReturn(true).when(productLineDao).exists(idMock);
        Mockito.doReturn(false).when(productLineDao).exists(productLinePredicate.name.eq(pluMock.getName()));
        Mockito.doReturn(plMock).when(productLineDao).findOne(idMock);
		assertEquals(HttpStatus.OK, productLineService.update(idMock, pluMock, bindingResult).getStatusCode());
	}
	
	@Test
	public void testUpdateSuccessfulSocketOnly() {
		Long idMock = 5L;
		ProductLineUpdateRequest pluMock = new ProductLineUpdateRequest("", 4, null);
		ProductLine plMock = new ProductLine(idMock, "Test1", 1, 500);
        Mockito.doReturn(true).when(productLineDao).exists(idMock);
        Mockito.doReturn(plMock).when(productLineDao).findOne(idMock);
		assertEquals(HttpStatus.OK, productLineService.update(idMock, pluMock, bindingResult).getStatusCode());
	}

	@Test
	public void testUpdateSuccessfulCapacityOnly() {
		Long idMock = 5L;
		ProductLineUpdateRequest pluMock = new ProductLineUpdateRequest("", null, 500);
		ProductLine plMock = new ProductLine(idMock, "Test1", 1, 500);
        Mockito.doReturn(true).when(productLineDao).exists(idMock);
        Mockito.doReturn(plMock).when(productLineDao).findOne(idMock);
		assertEquals(HttpStatus.OK, productLineService.update(idMock, pluMock, bindingResult).getStatusCode());
	}
	
	@Test
	public void testDeleteNotExists() {
		Long idMock = 5L;
		assertEquals(HttpStatus.NOT_FOUND, productLineService.delete(idMock).getStatusCode());			
	}

	@Test
    public void testDeleteHasChildren() {
        Long idMock = 5L;
        ProductLine plMock = new ProductLine(idMock, "Test1", 1, 500);
        List<ManufacturedUnit> manufacturedUnits = new ArrayList<>();
        manufacturedUnits.add(new ManufacturedUnit());
        plMock.setManufacturedUnits(manufacturedUnits);
        Mockito.doReturn(plMock).when(productLineDao).findOne(idMock);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, productLineService.delete(idMock).getStatusCode());
    }
	
	@Test
	public void testDeleteSuccessful() {
		Long idMock = 5L;
        ProductLine plMock = new ProductLine(idMock, "Test1", 1, 500);
        Mockito.doReturn(plMock).when(productLineDao).findOne(idMock);
		assertEquals(HttpStatus.OK, productLineService.delete(idMock).getStatusCode());			
	}

}
