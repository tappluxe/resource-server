package com.aws.tappluxe.services.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aws.tappluxe.data.entities.*;
import com.aws.tappluxe.services.MqttService;
import com.aws.tappluxe.services.UserUnitAvailabilityRequestService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;

import com.aws.tappluxe.data.dao.ManufacturedUnitDao;
import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dao.UserUnitDao;
import com.aws.tappluxe.data.dto.requests.UserUnitCreateRequest;
import com.aws.tappluxe.data.dto.requests.UserUnitUpdateRequest;
import com.aws.tappluxe.data.dto.responses.UserUnitDetailsResponse;
import com.aws.tappluxe.exceptions.InvalidFormException;
import com.aws.tappluxe.services.utils.EntityDtoConverter;
import com.querydsl.core.BooleanBuilder;

@RunWith(MockitoJUnitRunner.class)
public class UserUnitServiceImplTests {

    @InjectMocks
    private UserUnitServiceImpl userUnitService;

    @Mock
    private UserDao userDao;

    @Mock
    private UserUnitDao userUnitDao;

    @Mock
    private ManufacturedUnitDao manufacturedUnitDao;

    @Mock
    private UserUnitSocketServiceImpl userUnitSocketService;

    @Mock
    private UserUnitAvailabilityRequestService userUnitAvailabilityRequestService;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private MqttService mqttService;

    @Mock
    private MessageSource msgSource;

    private QUserUnit userUnitPredicate;
    
    private QManufacturedUnit manufacturedUnitPredicate;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.userUnitPredicate = QUserUnit.userUnit;
        manufacturedUnitPredicate = QManufacturedUnit.manufacturedUnit;
    }

    @Test
    public void testAll() {
        Long plIdMock = 1L;
        Long uuIdMock1 = 1L;
        Long uuIdMock2 = 1L;
        String productIdMock1 = "637be8fa-692c-4a2f-a504-134678468c3a";
        String productIdMock2 = "637be8fa-692c-4a2f-a504-134678468c3b";
        String uNameMock1 = "test1";
        String uNameMock2 = "test2";
        Long userIdMock = 1L;
        String emailMock = "test@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long socketIdMock1 = 1L;
        Long socketIdMock2 = 2L;
        Integer socketNumberMock1 = 1;
        Integer socketNumberMock2 = 1;
        Long statusIdMock1 = 1L;
        Long statusIdMock2 = 2L;
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMock1 = new ManufacturedUnit(productIdMock1, new Date(), plMock);
        ManufacturedUnit muMock2 = new ManufacturedUnit(productIdMock2, new Date(), plMock);
        User userMock = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        UserUnit uuMock1 = new UserUnit(uuIdMock1, uNameMock1, new Date(), muMock1, userMock);
        UserUnit uuMock2 = new UserUnit(uuIdMock2, uNameMock2, new Date(), muMock2, userMock);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock1, socketNumberMock1, null, uuMock1, null);
        UserUnitSocket socketMock2 = new UserUnitSocket(socketIdMock2, socketNumberMock2, null, uuMock2, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock1, new Date(), true, socketMock1);
        UserUnitSocketStatus statusMock2 = new UserUnitSocketStatus(statusIdMock2, new Date(), true, socketMock2);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        List<UserUnitSocketStatus> mockedStatuses2 = new ArrayList<>();
        mockedStatuses2.add(statusMock2);
        socketMock1.setStatusHistory(mockedStatuses1);
        socketMock2.setStatusHistory(mockedStatuses2);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        List<UserUnitSocket> mockedSockets2 = new ArrayList<>();
        mockedSockets2.add(socketMock2);
        uuMock1.setUserUnitSocket(mockedSockets1);
        uuMock2.setUserUnitSocket(mockedSockets2);
        List<UserUnit> mockedUserUnits = new ArrayList<>();
        mockedUserUnits.add(uuMock1);
        mockedUserUnits.add(uuMock2);
        List<UserUnitDetailsResponse> mockedResponse = new ArrayList<>();
        mockedResponse.add(EntityDtoConverter.toDto(uuMock1));
        mockedResponse.add(EntityDtoConverter.toDto(uuMock2));
        Mockito.doReturn(mockedUserUnits).when(userUnitDao).findAll(userUnitPredicate.user.email.eq(emailMock));
        assertEquals(mockedResponse, userUnitService.all(emailMock).getBody());
    }

    @Test(expected = InvalidFormException.class)
    public void testCreateProductKeyDoesNotExists() {
        String emailMock = "test@test.com";
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        String uNameMock = "test1";
        UserUnitCreateRequest createRequestMock = new UserUnitCreateRequest(productIdMock, uNameMock);
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        userUnitService.create(createRequestMock, emailMock, bindingResult);
    }

    @Test(expected = InvalidFormException.class)
    public void testCreateProductKeyRegistered() {
        String emailMock = "test@test.com";
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        String uNameMock = "test1";
        UserUnitCreateRequest createRequestMock = new UserUnitCreateRequest(productIdMock, uNameMock);
        Mockito.doReturn(true).when(manufacturedUnitDao).exists(manufacturedUnitPredicate.productKey.eq(productIdMock));
        Mockito.doReturn(true).when(userUnitDao).exists(userUnitPredicate.manufacturedUnit.productKey.eq(productIdMock));
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        userUnitService.create(createRequestMock, emailMock, bindingResult);
    }

    @Test(expected = InvalidFormException.class)
    public void testCreateNameExists() {
        String emailMock = "test@test.com";
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        String uNameMock = "test1";
        BooleanBuilder booleanPredicateConflict = new BooleanBuilder(userUnitPredicate.name.eq(uNameMock));
        booleanPredicateConflict.and(userUnitPredicate.user.email.eq(emailMock));
        UserUnitCreateRequest createRequestMock = new UserUnitCreateRequest(productIdMock, uNameMock);
        Mockito.doReturn(true).when(manufacturedUnitDao).exists(manufacturedUnitPredicate.productKey.eq(productIdMock));
        Mockito.doReturn(true).when(userUnitDao).exists(booleanPredicateConflict);
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        userUnitService.create(createRequestMock, emailMock, bindingResult);
    }

    @Test
    public void testCreateSuccess() {
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long userIdMock = 1L;
        String emailMock = "test@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        User userMock = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        BooleanBuilder booleanPredicateConflict = new BooleanBuilder(userUnitPredicate.name.eq(uNameMock));
        booleanPredicateConflict.and(userUnitPredicate.user.email.eq(emailMock));
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, userMock);
        UserUnitCreateRequest createRequestMock = new UserUnitCreateRequest(productIdMock, uNameMock);
        Mockito.doReturn(true).when(manufacturedUnitDao).exists(manufacturedUnitPredicate.productKey.eq(productIdMock));
        Mockito.doReturn(muMockResult).when(manufacturedUnitDao).findOne(manufacturedUnitPredicate.productKey.eq(productIdMock));
        Mockito.doReturn(userUnitMock).when(userUnitDao).saveAndFlush(ArgumentMatchers.any(UserUnit.class));
        assertEquals(HttpStatus.OK, userUnitService.create(createRequestMock, emailMock, bindingResult).getStatusCode());
    }

    @Test
    public void testShowNotExists() {
        Long uuIdMock = 1L;
        String emailMock = "test@test.com";
        assertEquals(HttpStatus.NOT_FOUND, userUnitService.show(uuIdMock, emailMock).getStatusCode());
    }

    @Test
    public void testShowExists() {
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long userIdMock = 1L;
        String emailMock = "test@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        User userMock = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, userMock);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        BooleanBuilder booleanPredicate = new BooleanBuilder(userUnitPredicate.id.eq(uuIdMock));
        booleanPredicate.and(userUnitPredicate.user.email.eq(emailMock));
        Mockito.doReturn(true).when(userUnitDao).exists(booleanPredicate);
        Mockito.doReturn(userUnitMock).when(userUnitDao).findOne(booleanPredicate);
        assertEquals(EntityDtoConverter.toDto(userUnitMock), userUnitService.show(uuIdMock, emailMock).getBody());
    }

    @Test
    public void testUpdateNotExists() {
        String emailMock = "test@test.com";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        String ssidMock = "AWS";
        UserUnitUpdateRequest updateRequestMock = new UserUnitUpdateRequest(uNameMock, ssidMock);
        BooleanBuilder booleanPredicateExists = new BooleanBuilder(userUnitPredicate.id.eq(uuIdMock));
        booleanPredicateExists.and(userUnitPredicate.user.email.eq(emailMock));
        assertEquals(HttpStatus.NOT_FOUND, userUnitService.update(uuIdMock, emailMock, updateRequestMock, bindingResult).getStatusCode());
    }

    @Test
    public void testUpdateEmptyFields() {
        String emailMock = "test@test.com";
        Long uuIdMock = 1L;
        UserUnitUpdateRequest updateRequestMock = new UserUnitUpdateRequest();
        BooleanBuilder booleanPredicateExists = new BooleanBuilder(userUnitPredicate.id.eq(uuIdMock));
        booleanPredicateExists.and(userUnitPredicate.user.email.eq(emailMock));
        Mockito.doReturn(true).when(userUnitDao).exists(booleanPredicateExists);
        assertEquals(HttpStatus.OK, userUnitService.update(uuIdMock, emailMock, updateRequestMock, bindingResult).getStatusCode());
    }

    @Test(expected = InvalidFormException.class)
    public void testUpdateNameConflict() {
        String emailMock = "test@test.com";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        String ssidMock = "AWS";
        UserUnitUpdateRequest updateRequestMock = new UserUnitUpdateRequest(uNameMock, ssidMock);
        BooleanBuilder booleanPredicateExists = new BooleanBuilder(userUnitPredicate.id.eq(uuIdMock));
        booleanPredicateExists.and(userUnitPredicate.user.email.eq(emailMock));
        BooleanBuilder booleanPredicateConflict = new BooleanBuilder(userUnitPredicate.name.eq(uNameMock))
                .and(userUnitPredicate.user.email.eq(emailMock));
        Mockito.doReturn(true).when(userUnitDao).exists(booleanPredicateExists);
        Mockito.doReturn(true).when(userUnitDao).exists(booleanPredicateConflict);
        Mockito.doReturn(true).when(bindingResult).hasErrors();
        userUnitService.update(uuIdMock, emailMock, updateRequestMock, bindingResult);
    }

    @Test
    public void testUpdateSuccess() {
        Long plIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3a";
        Long userIdMock = 1L;
        String emailMock = "test@test.com";
        String passwordMock = "To@st1";
        String firstNameMock = "test";
        String lastNameMock = "test";
        String phoneMock = "4444-444-4444";
        Long uuIdMock = 1L;
        String uNameMock = "test1";
        String ssidMock = "AWS";
        Long socketIdMock = 1L;
        Integer socketNumberMock = 1;
        Long statusIdMock = 1L;
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        User userMock = new User(userIdMock, emailMock, passwordMock, firstNameMock, lastNameMock, phoneMock, new Date());
        UserUnit userUnitMock = new UserUnit(uuIdMock, uNameMock, new Date(), muMockResult, userMock);
        UserUnitSocket socketMock1 = new UserUnitSocket(socketIdMock, socketNumberMock, null, userUnitMock, null);
        UserUnitSocketStatus statusMock1 = new UserUnitSocketStatus(statusIdMock, new Date(), true, socketMock1);
        List<UserUnitSocketStatus> mockedStatuses1 = new ArrayList<>();
        mockedStatuses1.add(statusMock1);
        socketMock1.setStatusHistory(mockedStatuses1);
        List<UserUnitSocket> mockedSockets1 = new ArrayList<>();
        mockedSockets1.add(socketMock1);
        userUnitMock.setUserUnitSocket(mockedSockets1);
        UserUnitUpdateRequest updateRequestMock = new UserUnitUpdateRequest(uNameMock, ssidMock);
        BooleanBuilder booleanPredicateExists = new BooleanBuilder(userUnitPredicate.id.eq(uuIdMock));
        booleanPredicateExists.and(userUnitPredicate.user.email.eq(emailMock));
        Mockito.doReturn(true).when(userUnitDao).exists(booleanPredicateExists);
        Mockito.doReturn(userUnitMock).when(userUnitDao).findOne(uuIdMock);
        assertEquals(HttpStatus.OK, userUnitService.update(uuIdMock, emailMock, updateRequestMock, bindingResult).getStatusCode());
    }

    @Test
    public void testDeleteNotExists() {
        Long uuIdMock = 1L;
        String emailMock = "test@test.com";
        assertEquals(HttpStatus.NOT_FOUND, userUnitService.delete(uuIdMock, emailMock).getStatusCode());
    }

    @Test
    public void testDeleteSuccess() {
        Long uuIdMock = 1L;
        String emailMock = "test@test.com";
        BooleanBuilder booleanPredicate = new BooleanBuilder(userUnitPredicate.id.eq(uuIdMock));
        booleanPredicate.and(userUnitPredicate.user.email.eq(emailMock));
        Mockito.doReturn(true).when(userUnitDao).exists(booleanPredicate);
        assertEquals(HttpStatus.OK, userUnitService.delete(uuIdMock, emailMock).getStatusCode());
    }

    @Test
    public void testAvailabilityNotExists() {
        Long uuIdMock = 1L;
        String emailMock = "test@test.com";
        assertEquals(HttpStatus.NOT_FOUND, userUnitService.availability(uuIdMock, emailMock).getStatusCode());
    }

}
