package com.aws.tappluxe.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dao.VerificationCodeDao;
import com.aws.tappluxe.data.entities.QUser;
import com.aws.tappluxe.data.entities.QVerificationCode;
import com.aws.tappluxe.data.entities.User;
import com.aws.tappluxe.data.entities.VerificationCode;
import com.aws.tappluxe.services.EmailService;
import com.aws.tappluxe.services.SMSMessageService;
import com.querydsl.core.types.Predicate;

@RunWith(MockitoJUnitRunner.class)
public class VerificationCodeServiceImplTests {
	
	@InjectMocks
	private VerificationCodeServiceImpl verificationCodeService;
	
	@Mock
	private VerificationCodeDao verificationCodeDao;
	
	@Mock
	private UserDao userDao;

	private QUser userPredicate;

	@Mock
	public EmailService emailService;
	
	@Mock
	public SMSMessageService smsMessageService;

	
	private QVerificationCode verificationCodePredicate;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		userPredicate = QUser.user;
		verificationCodePredicate = QVerificationCode.verificationCode;
	}

	@Test
	public void testCodeGeneration() {
		String testMail = "jay.arnel@gmail.com";
		Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(testMail));
		
		Mockito.doReturn(false).when(verificationCodeDao)
			.exists(verificationCodePredicate.user.email.eq(testMail)
					.and(verificationCodePredicate.isValid.eq(true)));
		assertEquals(verificationCodeDao.count(), 0);
		VerificationCode code = verificationCodeService.generateVerificationCode("jay.arnel@gmail.com");
		assertNotNull(code);
		assertTrue(code.isValid());
	}
	
	@Test
	public void testCodeGenerationExistingCode() {
		String testMail = "jay.arnel@gmail.com";
		VerificationCode oldCode = new VerificationCode();
		Predicate predicate = verificationCodePredicate.user.email.eq(testMail)
				.and(verificationCodePredicate.isValid.eq(true)); 
		Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(testMail));
		Mockito.doReturn(true).when(verificationCodeDao).exists(predicate);
		Mockito.doReturn(oldCode).when(verificationCodeDao).findOne(predicate);
		VerificationCode newCode = verificationCodeService.generateVerificationCode("jay.arnel@gmail.com");
		assertNotNull(newCode);
		assertTrue(newCode.isValid());
		assertFalse(oldCode.isValid());
	}

	@Test
	public void testCodeGenerationNonExistentUser() {
		String testMail = "jay.arnel@gmail.com";
		Mockito.doReturn(false).when(userDao).exists(userPredicate.email.eq(testMail));
		VerificationCode code = verificationCodeService.generateVerificationCode(testMail);
		assertNull(code);
	}
	
	@Test
	public void testCheckVerificationCodeNonExistentUser() {
		String testMail = "jay.arnel@gmail.com";
		String code = "1234";
		Mockito.doReturn(false).when(userDao).exists(userPredicate.email.eq(testMail));
		ResponseEntity<?> response = verificationCodeService.checkVerificationCode(code, testMail);
		assertEquals(response.getStatusCode(), HttpStatus.PRECONDITION_FAILED);
	}
	
	@Test
	public void testCheckVerificationCodeIncorrectCode() {
		String testMail = "jay.arnel@gmail.com";
		String code = "1234";
		User user = new User();
		user.setEmail(testMail);
		Predicate predicate = verificationCodePredicate.user.eq(user)
				.and(verificationCodePredicate.isValid.eq(true))
				.and(verificationCodePredicate.code.eq(code));
		Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(testMail));
		Mockito.doReturn(user).when(userDao).findOne(userPredicate.email.eq(testMail));
		Mockito.doReturn(false).when(verificationCodeDao).exists(predicate);
		ResponseEntity<?> response = verificationCodeService.checkVerificationCode(code, testMail);
		assertEquals(response.getStatusCode(), HttpStatus.PRECONDITION_FAILED);
	}
	
	@Test
	public void testCheckVerificationCodeCorrectCode() {
		String testMail = "jay.arnel@gmail.com";
		String code = "1234";
		User user = new User();
		user.setEmail(testMail);
		user.setVerified(false);
		VerificationCode verificationCode = new VerificationCode();
		verificationCode.setValid(true);
		Predicate predicate = verificationCodePredicate.user.eq(user)
				.and(verificationCodePredicate.isValid.eq(true))
				.and(verificationCodePredicate.code.eq(code));

		Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(testMail));
		Mockito.doReturn(user).when(userDao).findOne(userPredicate.email.eq(testMail));
		Mockito.doReturn(true).when(verificationCodeDao).exists(predicate);
		Mockito.doReturn(verificationCode).when(verificationCodeDao).findOne(predicate);
		

		ResponseEntity<?> response = verificationCodeService.checkVerificationCode(code, testMail);
		assertEquals(response.getStatusCode(), HttpStatus.OK);
	}
	
	@Test
	public void testResendCode() {
		String testMail = "jay.arnel@gmail.com";
		User user = new User();
		user.setPhone("+15005550006");
		Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(testMail));
		Mockito.doReturn(user).when(userDao).findOne(userPredicate.email.eq(testMail));
		Mockito.doReturn(false).when(verificationCodeDao)
			.exists(verificationCodePredicate.user.email.eq(testMail)
					.and(verificationCodePredicate.isValid.eq(true)));
		ResponseEntity<?> response = verificationCodeService.resendCode(testMail);
		assertEquals(response.getStatusCode(), HttpStatus.OK);
		
	}
	
	@Test
	public void testResendCodeNonExistentUser() {
		String testMail = "jay.arnel@gmail.com";
		Mockito.doReturn(false).when(userDao).exists(userPredicate.email.eq(testMail));
		ResponseEntity<?> response = verificationCodeService.resendCode(testMail);
		assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void testIsVerifiedNonExistentUser() {
		String testMail = "jay.arnel@gmail.com";
		Mockito.doReturn(false).when(userDao).exists(userPredicate.email.eq(testMail));
		ResponseEntity<?> response = verificationCodeService.isVerified(testMail);
		assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void testIsVerifiedTrue() throws ParseException {
		String testMail = "jay.arnel@gmail.com";
		User user = new User();
		user.setVerified(true);
		Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(testMail));
		Mockito.doReturn(user).when(userDao).findOne(userPredicate.email.eq(testMail));
		ResponseEntity<?> response = verificationCodeService.isVerified(testMail);
		assertEquals(response.getStatusCode(), HttpStatus.OK);
		JSONParser parser = new JSONParser();
		JSONObject obj = (JSONObject) parser.parse(response.getBody().toString());
		assertEquals(obj.get("verified"), true);
	}
	
	@Test
	public void testIsVerifiedFalse() throws ParseException {
		String testMail = "jay.arnel@gmail.com";
		User user = new User();
		user.setVerified(false);
		Mockito.doReturn(true).when(userDao).exists(userPredicate.email.eq(testMail));
		Mockito.doReturn(user).when(userDao).findOne(userPredicate.email.eq(testMail));
		ResponseEntity<?> response = verificationCodeService.isVerified(testMail);
		assertEquals(response.getStatusCode(), HttpStatus.OK);
		JSONParser parser = new JSONParser();
		JSONObject obj = (JSONObject) parser.parse(response.getBody().toString());
		assertEquals(obj.get("verified"), false);
	}
}
