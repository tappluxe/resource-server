package com.aws.tappluxe.services.impl;

import com.aws.tappluxe.data.dao.ManufacturedUnitDao;
import com.aws.tappluxe.data.dao.ProductLineDao;
import com.aws.tappluxe.data.dto.responses.ManufacturedUnitDetailsResponse;
import com.aws.tappluxe.data.entities.*;
import com.aws.tappluxe.services.utils.EntityDtoConverter;
import com.aws.tappluxe.services.utils.QRCodeGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ManufacturedUnitServiceImplTests {

    @InjectMocks
    private ManufacturedUnitServiceImpl manufacturedUnitService;

    @Mock
    private ManufacturedUnitDao manufacturedUnitDao;

    @Mock
    private ProductLineDao productLineDao;

    @Mock
    private MessageSource messageSource;

    private QProductLine productLinePredicate;

    private QManufacturedUnit manufacturedUnitPredicate;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.productLinePredicate = QProductLine.productLine;
        this.manufacturedUnitPredicate = QManufacturedUnit.manufacturedUnit;
    }

    @Test
    public void testAll() {
        Long plIdMock = 1L;
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        String productIdMock1 = "637be8fa-692c-4a2f-a504-134678468c3a";
        String productIdMock2 = "637be8fa-692c-4a2f-a504-134678468c3b";
        ManufacturedUnit muMock1 = new ManufacturedUnit(productIdMock1, new Date(), plMock);
        ManufacturedUnit muMock2 = new ManufacturedUnit(productIdMock2, new Date(), plMock);
        List<ManufacturedUnit> mockedManufacturedUnits = new ArrayList<>();
        mockedManufacturedUnits.add(muMock1);
        mockedManufacturedUnits.add(muMock2);
        List<ManufacturedUnitDetailsResponse> mockedResponse = new ArrayList<>();
        mockedResponse.add(EntityDtoConverter.toDto(muMock1));
        mockedResponse.add(EntityDtoConverter.toDto(muMock2));
        Mockito.doReturn(mockedManufacturedUnits).when(manufacturedUnitDao).findAll();
        assertEquals(mockedResponse, manufacturedUnitService.all().getBody());
    }

    @Test
    public void testCreateProductLineNotExists() {
        Long plIdMock = 1L;
        assertEquals(HttpStatus.NOT_FOUND, manufacturedUnitService.create(plIdMock).getStatusCode());
    }

    @Test
    public void testCreateSuccess() {
        ReflectionTestUtils.setField(QRCodeGenerator.class, "location", "../tappluxe_qr_codes");
        ReflectionTestUtils.setField(QRCodeGenerator.class, "fileType", "png");
        ReflectionTestUtils.setField(QRCodeGenerator.class, "size", 250);
        Long plIdMock = 1L;
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3b";
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        Mockito.doReturn(true).when(productLineDao).exists(plIdMock);
        Mockito.doReturn(plMock).when(productLineDao).findOne(plIdMock);
        Mockito.doReturn(muMockResult).when(manufacturedUnitDao).saveAndFlush(ArgumentMatchers.any(ManufacturedUnit.class));
        assertEquals(HttpStatus.OK, manufacturedUnitService.create(plIdMock).getStatusCode());
        String filename = QRCodeGenerator.resolveFilePath(productIdMock);
        File testFile = new File(filename);
        assertTrue(testFile.exists());
        testFile.delete();
    }

    @Test
    public void testShowNotExists() {
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3b";
        Long muIdMock = 1L;
        assertEquals(HttpStatus.NOT_FOUND, manufacturedUnitService.show(muIdMock).getStatusCode());
    }

    @Test
    public void testShowExists() {
        Long plIdMock = 1L;
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        Long muIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3b";
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        Mockito.doReturn(true).when(manufacturedUnitDao).exists(muIdMock);
        Mockito.doReturn(muMockResult).when(manufacturedUnitDao).findOne(muIdMock);
        assertEquals(EntityDtoConverter.toDto(muMockResult), manufacturedUnitService.show(muIdMock).getBody());
    }

    @Test
    public void testDeleteNotExists() {
        Long muIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3b";
        assertEquals(HttpStatus.NOT_FOUND, manufacturedUnitService.delete(muIdMock).getStatusCode());
    }

    @Test
    public void testDeleteHasChildren() {
        Long plIdMock = 1L;
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        Long muIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3b";
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        muMockResult.setUserUnit(new UserUnit());
        Mockito.doReturn(muMockResult).when(manufacturedUnitDao).findOne(muIdMock);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, manufacturedUnitService.delete(muIdMock).getStatusCode());
    }

    @Test
    public void testDeleteSuccess() {
        Long plIdMock = 1L;
        ProductLine plMock = new ProductLine(plIdMock, "Test1", 1, 500);
        Long muIdMock = 1L;
        String productIdMock = "637be8fa-692c-4a2f-a504-134678468c3b";
        ManufacturedUnit muMockResult = new ManufacturedUnit(productIdMock, new Date(), plMock);
        Mockito.doReturn(muMockResult).when(manufacturedUnitDao).findOne(muIdMock);
        assertEquals(HttpStatus.OK, manufacturedUnitService.delete(muIdMock).getStatusCode());
    }

}
