package com.aws.tappluxe.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.aws.tappluxe.data.dao.QueuedEmailDao;

import org.jvnet.mock_javamail.Mailbox;
import javax.mail.Message;
import javax.mail.MessagingException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@ActiveProfiles("test")
public class EmailServiceImplTests {

	@Autowired
	private EmailServiceImpl emailService;
	
	@Autowired
	public QueuedEmailDao queuedEmailDao;
	
	@Before
	public void setUp() {
		Mailbox.clearAll();
	}
	
	@Test
	public void sendEmail() throws MessagingException, IOException {
		String subject = "Email Test";
		String body = "Testing email service";
		String to = "test.dst@tappluxe.com";
		assertTrue(emailService.sendSimpleMessage(to, subject, body));
		List<Message> inbox = Mailbox.get(to);
  
		assertEquals(inbox.size(), 1);  
		assertEquals(subject, inbox.get(0).getSubject());
  		assertEquals(body, inbox.get(0).getContent());
	}
	
	@Test
	public void testCreatingEmail() {
		String subject = "Email Test";
		String body = "Testing email service";
		String to = "test.dst@tappluxe.com";
		assertEquals(queuedEmailDao.count(), 0);
		emailService.createEmail(to, subject, body);
		assertEquals(queuedEmailDao.count(), 1);
	}
}
