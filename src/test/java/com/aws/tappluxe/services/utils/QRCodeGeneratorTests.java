package com.aws.tappluxe.services.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class QRCodeGeneratorTests {
	
//	@Autowired
	private QRCodeGenerator qrCodeGenerator;
	
	@Test
	public void testGenerate() {
		qrCodeGenerator = new QRCodeGenerator();
		ReflectionTestUtils.setField(QRCodeGenerator.class, "location", "../tappluxe_qr_codes");
		ReflectionTestUtils.setField(QRCodeGenerator.class, "fileType", "png");
		ReflectionTestUtils.setField(QRCodeGenerator.class, "size", 250);
		String content = UUID.randomUUID().toString();
		System.err.println(content);
		String filePath = QRCodeGenerator.resolveFilePath(content);
		File file = new File(filePath);
		System.out.println(file.getAbsolutePath());
		assertFalse(file.exists());
		QRCodeGenerator.generate(content);
		assertTrue(file.exists());
//		file.delete();
	}

}
