package com.aws.tappluxe.tasks;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.aws.tappluxe.data.dao.DeviceDao;
import com.aws.tappluxe.data.dao.SMSMessageDao;
import com.aws.tappluxe.data.dao.ScheduleDao;
import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dao.UserUnitDao;
import com.aws.tappluxe.data.dao.UserUnitSocketDao;
import com.aws.tappluxe.data.entities.Device;
import com.aws.tappluxe.data.entities.Schedule;
import com.aws.tappluxe.data.entities.User;
import com.aws.tappluxe.data.entities.UserUnit;
import com.aws.tappluxe.data.entities.UserUnitSocket;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@ActiveProfiles("test")
public class ScheduleNotificationTaskTests {
	
	@Autowired
	public ScheduleNotificationTask task;
	
	@Autowired
	public UserDao userDao;
	
	@Autowired
	public UserUnitDao unitDao;

	@Autowired
	public ScheduleDao scheduleDao;
	
	@Autowired
	public UserUnitSocketDao socketDao;
	
	@Autowired
	public DeviceDao deviceDao;
	
	@Autowired
	public SMSMessageDao smsMessageDao;
	
	@Test
	public void testNotification() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		String deviceName = "testDevice";
		String schedName = "testSchedule";
		String unitName = "testUnit";
		String userPhone = "09278940365";
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+ 5);
		cal.set(Calendar.SECOND, cal.get(Calendar.SECOND)+ 20);
		Device device = new Device();
		device.setName(deviceName);
		device = deviceDao.saveAndFlush(device);
		System.err.println(device);
		User user = new User();
		user.setPhone(userPhone);
		user = userDao.saveAndFlush(user);
		UserUnit unit = new UserUnit();
		unit.setName(unitName);
		unit.setUser(user);
		unit = unitDao.saveAndFlush(unit);
		Schedule schedule = new Schedule();
		schedule.setId(1L);
		schedule.setName(schedName);
		schedule.setTimeBeforeNotification(5);
		schedule.setTime(cal.getTime());
		schedule.setActive(true);
		schedule.setUser(user);
		schedule.setRepeat(Integer.toString(day));
		schedule = scheduleDao.saveAndFlush(schedule);
		UserUnitSocket socket = new UserUnitSocket();
		socket.setUserUnit(unit);
		socket.setSchedule(schedule);
		socket.setSocketNumber(1);
		socket = socketDao.saveAndFlush(socket);
		device.setUserUnitSocket(socket);
		device = deviceDao.saveAndFlush(device);
		assertEquals(0, smsMessageDao.count());
		task.sendScheduleNotifications();
		assertEquals(1, smsMessageDao.count());
	}
	
	@Test
	public void testNotificationNotEqualToTimeBeforeNotification() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		String deviceName = "testDevice";
		String schedName = "testSchedule";
		String unitName = "testUnit";
		String userPhone = "09278940365";
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+ 6);
		cal.set(Calendar.SECOND, cal.get(Calendar.SECOND)+ 20);
		Device device = new Device();
		device.setName(deviceName);
		device = deviceDao.saveAndFlush(device);
		System.err.println(device);
		User user = new User();
		user.setPhone(userPhone);
		user = userDao.saveAndFlush(user);
		UserUnit unit = new UserUnit();
		unit.setName(unitName);
		unit.setUser(user);
		unit = unitDao.saveAndFlush(unit);
		Schedule schedule = new Schedule();
		schedule.setId(1L);
		schedule.setName(schedName);
		schedule.setTimeBeforeNotification(5);
		schedule.setTime(cal.getTime());
		schedule.setActive(true);
		schedule.setUser(user);
		schedule.setRepeat(Integer.toString(day));
		schedule = scheduleDao.saveAndFlush(schedule);
		UserUnitSocket socket = new UserUnitSocket();
		socket.setUserUnit(unit);
		socket.setSchedule(schedule);
		socket.setSocketNumber(1);
		socket = socketDao.saveAndFlush(socket);
		device.setUserUnitSocket(socket);
		device = deviceDao.saveAndFlush(device);
		assertEquals(0, smsMessageDao.count());
		task.sendScheduleNotifications();
		assertEquals(0, smsMessageDao.count());
	}
	
	@Test
	public void testNotificationNoRepeatToday() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		String deviceName = "testDevice";
		String schedName = "testSchedule";
		String unitName = "testUnit";
		String userPhone = "09278940365";
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+ 5);
		cal.set(Calendar.SECOND, cal.get(Calendar.SECOND)+ 20);
		Device device = new Device();
		device.setName(deviceName);
		device = deviceDao.saveAndFlush(device);
		System.err.println(device);
		User user = new User();
		user.setPhone(userPhone);
		user = userDao.saveAndFlush(user);
		UserUnit unit = new UserUnit();
		unit.setName(unitName);
		unit.setUser(user);
		unit = unitDao.saveAndFlush(unit);
		Schedule schedule = new Schedule();
		schedule.setId(1L);
		schedule.setName(schedName);
		schedule.setTimeBeforeNotification(5);
		schedule.setTime(cal.getTime());
		schedule.setActive(true);
		schedule.setUser(user);
		schedule.setRepeat(Integer.toString(day + 1));
		schedule = scheduleDao.saveAndFlush(schedule);
		UserUnitSocket socket = new UserUnitSocket();
		socket.setUserUnit(unit);
		socket.setSchedule(schedule);
		socket.setSocketNumber(1);
		socket = socketDao.saveAndFlush(socket);
		device.setUserUnitSocket(socket);
		device = deviceDao.saveAndFlush(device);
		assertEquals(0, smsMessageDao.count());
		task.sendScheduleNotifications();
		assertEquals(0, smsMessageDao.count());
	}
	
	@Test
	public void testNotificationDeletedSchedule() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		String deviceName = "testDevice";
		String schedName = "testSchedule";
		String unitName = "testUnit";
		String userPhone = "09278940365";
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+ 5);
		cal.set(Calendar.SECOND, cal.get(Calendar.SECOND)+ 20);
		Device device = new Device();
		device.setName(deviceName);
		device = deviceDao.saveAndFlush(device);
		System.err.println(device);
		User user = new User();
		user.setPhone(userPhone);
		user = userDao.saveAndFlush(user);
		UserUnit unit = new UserUnit();
		unit.setName(unitName);
		unit.setUser(user);
		unit = unitDao.saveAndFlush(unit);
		Schedule schedule = new Schedule();
		schedule.setId(1L);
		schedule.setName(schedName);
		schedule.setTimeBeforeNotification(5);
		schedule.setTime(cal.getTime());
		schedule.setActive(true);
		schedule.setDeleted(true);
		schedule.setUser(user);
		schedule.setRepeat(Integer.toString(day + 1));
		schedule = scheduleDao.saveAndFlush(schedule);
		UserUnitSocket socket = new UserUnitSocket();
		socket.setUserUnit(unit);
		socket.setSchedule(schedule);
		socket.setSocketNumber(1);
		socket = socketDao.saveAndFlush(socket);
		device.setUserUnitSocket(socket);
		device = deviceDao.saveAndFlush(device);
		assertEquals(0, smsMessageDao.count());
		task.sendScheduleNotifications();
		assertEquals(0, smsMessageDao.count());
	}
	
	@Test
	public void testNotificationInActiveSchedule() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		String deviceName = "testDevice";
		String schedName = "testSchedule";
		String unitName = "testUnit";
		String userPhone = "09278940365";
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE)+ 5);
		cal.set(Calendar.SECOND, cal.get(Calendar.SECOND)+ 20);
		Device device = new Device();
		device.setName(deviceName);
		device = deviceDao.saveAndFlush(device);
		System.err.println(device);
		User user = new User();
		user.setPhone(userPhone);
		user = userDao.saveAndFlush(user);
		UserUnit unit = new UserUnit();
		unit.setName(unitName);
		unit.setUser(user);
		unit = unitDao.saveAndFlush(unit);
		Schedule schedule = new Schedule();
		schedule.setId(1L);
		schedule.setName(schedName);
		schedule.setTimeBeforeNotification(5);
		schedule.setTime(cal.getTime());
		schedule.setActive(false);
		schedule.setDeleted(true);
		schedule.setUser(user);
		schedule.setRepeat(Integer.toString(day + 1));
		schedule = scheduleDao.saveAndFlush(schedule);
		UserUnitSocket socket = new UserUnitSocket();
		socket.setUserUnit(unit);
		socket.setSchedule(schedule);
		socket.setSocketNumber(1);
		socket = socketDao.saveAndFlush(socket);
		device.setUserUnitSocket(socket);
		device = deviceDao.saveAndFlush(device);
		assertEquals(0, smsMessageDao.count());
		task.sendScheduleNotifications();
		assertEquals(0, smsMessageDao.count());
	}
	

}
