package com.aws.tappluxe.data.dto.responses;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserUnitSocketDetailsResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @JsonProperty
    private Long id;

    @JsonProperty
    private Integer socketNumber;

    @JsonProperty
    private Long userUnitId;

    @JsonProperty
    private Long deviceId;

    @JsonProperty
    private ScheduleDetailsResponse schedule;

    @JsonProperty
    private List<UserUnitSocketStatusDetailsResponse> statuses;

    public UserUnitSocketDetailsResponse(Long id, Integer socketNumber, Long userUnitId, Long deviceId, ScheduleDetailsResponse schedule, List<UserUnitSocketStatusDetailsResponse> statuses) {
        this.id = id;
        this.socketNumber = socketNumber;
        this.userUnitId = userUnitId;
        this.deviceId = deviceId;
        this.schedule = schedule;
        this.statuses = statuses;
    }

    public UserUnitSocketDetailsResponse() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSocketNumber() {
        return socketNumber;
    }

    public void setSocketNumber(Integer socketNumber) {
        this.socketNumber = socketNumber;
    }

    public Long getUserUnitId() {
        return userUnitId;
    }

    public void setUserUnitId(Long userUnitId) {
        this.userUnitId = userUnitId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public ScheduleDetailsResponse getSchedule() {
        return schedule;
    }

    public void setSchedule(ScheduleDetailsResponse schedule) {
        this.schedule = schedule;
    }

    public List<UserUnitSocketStatusDetailsResponse> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<UserUnitSocketStatusDetailsResponse> statuses) {
        this.statuses = statuses;
    }

    @Override
    public String toString() {
        return "UserUnitSocketDetailsResponse{" +
                "id=" + id +
                ", socketNumber=" + socketNumber +
                ", userUnitId=" + userUnitId +
                ", deviceId=" + deviceId +
                ", schedule=" + schedule +
                ", statuses=" + statuses +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserUnitSocketDetailsResponse that = (UserUnitSocketDetailsResponse) o;

        if (!getId().equals(that.getId())) return false;
        if (!getUserUnitId().equals(that.getUserUnitId())) return false;
        if (getSchedule() != null ? !getSchedule().equals(that.getSchedule()) : that.getSchedule() != null)
            return false;
        return getStatuses().equals(that.getStatuses());
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getSocketNumber().hashCode();
        result = 31 * result + getUserUnitId().hashCode();
        result = 31 * result + (getSchedule() != null ? getSchedule().hashCode() : 0);
        return result;
    }

}
