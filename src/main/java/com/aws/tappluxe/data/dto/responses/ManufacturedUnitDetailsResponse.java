package com.aws.tappluxe.data.dto.responses;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ManufacturedUnitDetailsResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty
	private Long productId;
	
	@JsonProperty
	private String productKey;
	
	@JsonProperty
	private Date manufacturedDate;

	public ManufacturedUnitDetailsResponse(Long productId, String productKey, Date manufacturedDate) {
		this.productId = productId;
		this.productKey = productKey;
		this.manufacturedDate = manufacturedDate;
	}

	public ManufacturedUnitDetailsResponse() {
	}

	public String getProductKey() {
		return productKey;
	}

	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	public Date getManufacturedDate() {
		return manufacturedDate;
	}

	public void setManufacturedDate(Date manufacturedDate) {
		this.manufacturedDate = manufacturedDate;
	}
	
	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "ManufacturedUnitDetailsResponse{" +
				"productId='" + productId + '\'' +
				",productKey='" + productKey + '\'' +
				", manufacturedDate=" + manufacturedDate +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ManufacturedUnitDetailsResponse that = (ManufacturedUnitDetailsResponse) o;

		if (!getProductKey().equals(that.getProductKey())) return false;
		return getManufacturedDate().equals(that.getManufacturedDate());
	}

	@Override
	public int hashCode() {
		int result = getProductKey().hashCode();
		result = 31 * result + getManufacturedDate().hashCode();
		return result;
	}

}
