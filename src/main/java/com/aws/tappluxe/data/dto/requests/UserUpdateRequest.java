package com.aws.tappluxe.data.dto.requests;

import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Pattern;

public class UserUpdateRequest {

	@JsonProperty
	@Email
	private String email;
	
	@JsonProperty
	private String oldPassword;
	
	@JsonProperty
	@Pattern(regexp = "(^$|^((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$/^&*();,+~`_|]).{6,20})$)")
	private String newPassword;
	
	@JsonProperty
	private String confirmPassword;
	
	@JsonProperty
	private String firstName;
	
	@JsonProperty
	private String lastName;
	
	@JsonProperty
	private String phone;

	@AssertTrue
	private boolean isPasswordEqual() {
		return newPassword.equals(confirmPassword);
	}

	public UserUpdateRequest(String email, String oldPassword, String newPassword, String confirmPassword,
			String firstName, String lastName, String phone) {
		this.email = email;
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;
		this.confirmPassword = confirmPassword;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
	}

	public UserUpdateRequest() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	
}
