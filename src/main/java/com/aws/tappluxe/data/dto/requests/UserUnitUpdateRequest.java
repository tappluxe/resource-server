package com.aws.tappluxe.data.dto.requests;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserUnitUpdateRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty
	private String name;
	
	@JsonProperty
	private String ssid;

	public UserUnitUpdateRequest(String name, String ssid) {
		this.name = name;
		this.ssid = ssid;
	}

	public UserUnitUpdateRequest() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	
}
