package com.aws.tappluxe.data.dto.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GeneralResponse {
	
	@JsonProperty
	private String message;
	
	@JsonProperty
	private String requestFor;	

	public GeneralResponse(String requestFor) {
		this.requestFor = requestFor;
	}	

	public GeneralResponse() {
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRequestFor() {
		return requestFor;
	}

	public void setRequestFor(String requestFor) {
		this.requestFor = requestFor;
	}
	
	@Override
	public String toString(){
		return this.message;
	}

}
