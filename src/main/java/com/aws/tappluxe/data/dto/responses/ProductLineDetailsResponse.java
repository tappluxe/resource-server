package com.aws.tappluxe.data.dto.responses;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductLineDetailsResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty
	private Long id;
	
	@JsonProperty
	private String name;
	
	@JsonProperty
	private Integer numOfSockets;
	
	@JsonProperty
	private Integer loadCapacity;

	public ProductLineDetailsResponse(Long id, String name, Integer numOfSockets, Integer loadCapacity) {
		this.id = id;
		this.name = name;
		this.numOfSockets = numOfSockets;
		this.loadCapacity = loadCapacity;
	}

	public ProductLineDetailsResponse() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumOfSockets() {
		return numOfSockets;
	}

	public void setNumOfSockets(Integer numOfSockets) {
		this.numOfSockets = numOfSockets;
	}

	public Integer getLoadCapacity() {
		return loadCapacity;
	}

	public void setLoadCapacity(Integer loadCapacity) {
		this.loadCapacity = loadCapacity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((loadCapacity == null) ? 0 : loadCapacity.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((numOfSockets == null) ? 0 : numOfSockets.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductLineDetailsResponse other = (ProductLineDetailsResponse) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (loadCapacity == null) {
			if (other.loadCapacity != null)
				return false;
		} else if (!loadCapacity.equals(other.loadCapacity))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (numOfSockets == null) {
			if (other.numOfSockets != null)
				return false;
		} else if (!numOfSockets.equals(other.numOfSockets))
			return false;
		return true;
	}
	
}
