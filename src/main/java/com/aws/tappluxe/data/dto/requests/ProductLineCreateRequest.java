package com.aws.tappluxe.data.dto.requests;

import java.io.Serializable;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductLineCreateRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty
	@NotBlank
	private String name;
	
	@JsonProperty
	@NotNull
	@DecimalMin(value = "0", inclusive = false)
	private Integer numOfSockets;
	
	@JsonProperty
	@NotNull
	@DecimalMin(value = "0", inclusive = false)
	private Integer loadCapacity;

	public ProductLineCreateRequest(String name, Integer numOfSockets, Integer loadCapacity) {
		this.name = name;
		this.numOfSockets = numOfSockets;
		this.loadCapacity = loadCapacity;
	}

	public ProductLineCreateRequest() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumOfSockets() {
		return numOfSockets;
	}

	public void setNumOfSockets(Integer numOfSockets) {
		this.numOfSockets = numOfSockets;
	}

	public Integer getLoadCapacity() {
		return loadCapacity;
	}

	public void setLoadCapacity(Integer loadCapacity) {
		this.loadCapacity = loadCapacity;
	}

}
