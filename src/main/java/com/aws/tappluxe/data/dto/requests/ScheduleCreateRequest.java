package com.aws.tappluxe.data.dto.requests;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScheduleCreateRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty
	@NotBlank
	private String name;
	
	@JsonProperty
	@NotNull
	private Date time;
	
	@JsonProperty
	@NotBlank
	private String repeat;
	
	@JsonProperty
	@NotNull
	private Integer timeBeforeNotification;

	public ScheduleCreateRequest(String name, Date time, String repeat, Integer timeBeforeNotification, Boolean notificationEnabled) {
		this.name = name;
		this.time = time;
		this.repeat = repeat;
		this.timeBeforeNotification = timeBeforeNotification;
	}

	public ScheduleCreateRequest() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getRepeat() {
		return repeat;
	}

	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}

	public Integer getTimeBeforeNotification() {
		return timeBeforeNotification;
	}

	public void setTimeBeforeNotification(Integer timeBeforeNotification) {
		this.timeBeforeNotification = timeBeforeNotification;
	}

}
