package com.aws.tappluxe.data.dto.responses;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDetailsResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty
	private Long id;
	
	@JsonProperty
	private String email;
	
	@JsonProperty
	private String firstName;
	
	@JsonProperty
	private String lastName;
	
	@JsonProperty
	private String phone;
	
	@JsonProperty
	private Date registrationDate;
	
	@JsonProperty
	private Boolean isVerified;

	public UserDetailsResponse(Long id, String email, String firstName, String lastName, String phone,
			Date registrationDate, Boolean isVerified) {
		this.id = id;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.registrationDate = registrationDate;
		this.isVerified = isVerified;
	}

	public UserDetailsResponse() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	
	public Boolean isVerified() {
		return isVerified;
	}

	public void setVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	@Override
	public String toString() {
		return "UserDetailsResponse{" +
				"id=" + id +
				", email='" + email + '\'' +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", phone='" + phone + '\'' +
				", registrationDate=" + registrationDate +
				", isVerified=" + isVerified + 
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		UserDetailsResponse that = (UserDetailsResponse) o;

		if (!getId().equals(that.getId())) return false;
		if (!getEmail().equals(that.getEmail())) return false;
		if (!getFirstName().equals(that.getFirstName())) return false;
		if (!getLastName().equals(that.getLastName())) return false;
		if (!getPhone().equals(that.getPhone())) return false;
		return getRegistrationDate().equals(that.getRegistrationDate());
	}

	@Override
	public int hashCode() {
		int result = getId().hashCode();
		result = 31 * result + getEmail().hashCode();
		result = 31 * result + getFirstName().hashCode();
		result = 31 * result + getLastName().hashCode();
		result = 31 * result + getPhone().hashCode();
		result = 31 * result + getRegistrationDate().hashCode();
		return result;
	}

}
