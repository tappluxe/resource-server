package com.aws.tappluxe.data.dto.requests;

import java.io.Serializable;

import javax.validation.Valid;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserCreateRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty
	@NotBlank
	@Email
	private String email;
	
	@JsonProperty
	@NotBlank
	@Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$/^&*();,+~`_|]).{6,20})")
	private String password;
	
	@JsonProperty
	@NotBlank
	private String confirmPassword;
	
	@JsonProperty
	@NotBlank
	private String firstName;
	
	@JsonProperty
	@NotBlank
	private String lastName;
	
	@JsonProperty
	@NotBlank
	private String phone;
	
	@Valid
	@NotNull
	private UserUnitCreateRequest userUnit;

	@AssertTrue
	private boolean isPasswordEqual() {
		return password.equals(confirmPassword);
	}

	public UserCreateRequest(String email, String password, String confirmPassword, String firstName, String lastName,
			String phone, UserUnitCreateRequest userUnit) {
		this.email = email;
		this.password = password;
		this.confirmPassword = confirmPassword;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.userUnit = userUnit;
	}

	public UserCreateRequest() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public UserUnitCreateRequest getUserUnit() {
		return userUnit;
	}

	public void setUserUnit(UserUnitCreateRequest userUnit) {
		this.userUnit = userUnit;
	}
	
}
