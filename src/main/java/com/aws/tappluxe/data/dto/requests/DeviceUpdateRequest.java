package com.aws.tappluxe.data.dto.requests;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeviceUpdateRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty
	private String name;
	
	@JsonProperty
	private Long userUnitSocketId;

	public DeviceUpdateRequest(String name, Long userUnitSocketId) {
		this.name = name;
		this.userUnitSocketId = userUnitSocketId;
	}

	public DeviceUpdateRequest() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getUserUnitSocketId() {
		return userUnitSocketId;
	}

	public void setUserUnitSocketId(Long userUnitSocketId) {
		this.userUnitSocketId = userUnitSocketId;
	}

}
