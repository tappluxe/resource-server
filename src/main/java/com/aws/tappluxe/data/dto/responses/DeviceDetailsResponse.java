package com.aws.tappluxe.data.dto.responses;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeviceDetailsResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	private Long id;
	
	@JsonProperty
	private String name;
	
	@JsonProperty
	private Long userUnitSocketId;

	public DeviceDetailsResponse(Long id, String name, Long userUnitSocketId, ScheduleDetailsResponse schedule) {
		this.id = id;
		this.name = name;
		this.userUnitSocketId = userUnitSocketId;
	}

	public DeviceDetailsResponse() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getUserUnitSocketId() {
		return userUnitSocketId;
	}

	public void setUserUnitSocketId(Long userUnitSocketId) {
		this.userUnitSocketId = userUnitSocketId;
	}

	@Override
	public String toString() {
		return "DeviceDetailsResponse{" +
				"id=" + id +
				", name='" + name + '\'' +
				", userUnitSocketId=" + userUnitSocketId +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DeviceDetailsResponse that = (DeviceDetailsResponse) o;

		if (!getId().equals(that.getId())) return false;
		return getName().equals(that.getName());
	}

	@Override
	public int hashCode() {
		int result = getId().hashCode();
		result = 31 * result + getName().hashCode();
		result = 31 * result + getUserUnitSocketId().hashCode();
		return result;
	}

}
