package com.aws.tappluxe.data.dto.responses;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserUnitDetailsResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	private Long id;
	
	@JsonProperty
	private String name;

	@JsonProperty
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
	private Date registrationDate;
	
	@JsonProperty
	private Long userId;
	
	@JsonProperty
	private ManufacturedUnitDetailsResponse manufacturedUnitDetails;
	
	@JsonProperty
	private List<UserUnitSocketDetailsResponse> sockets;
	
	@JsonProperty
	private String ssid;

	public UserUnitDetailsResponse(Long id, String name, Date registrationDate, Double longitude,
								   Double latitude, Long userId, ManufacturedUnitDetailsResponse manufacturedUnitDetails,
								   List<UserUnitSocketDetailsResponse> sockets, String ssid) {
		this.id = id;
		this.name = name;
		this.registrationDate = registrationDate;
		this.userId = userId;
		this.manufacturedUnitDetails = manufacturedUnitDetails;
		this.sockets = sockets;
		this.ssid = ssid;
	}

	public UserUnitDetailsResponse() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public ManufacturedUnitDetailsResponse getManufacturedUnitDetails() {
		return manufacturedUnitDetails;
	}

	public void setManufacturedUnitDetails(ManufacturedUnitDetailsResponse manufacturedUnitDetails) {
		this.manufacturedUnitDetails = manufacturedUnitDetails;
	}

	public List<UserUnitSocketDetailsResponse> getSockets() {
		return sockets;
	}

	public void setSockets(List<UserUnitSocketDetailsResponse> sockets) {
		this.sockets = sockets;
	}
	
	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	@Override
	public String toString() {
		return "UserUnitDetailsResponse{" +
				"id=" + id +
				", name='" + name + '\'' +
				", registrationDate=" + registrationDate +
				", userId=" + userId +
				", productId=" + manufacturedUnitDetails.getProductId() +
				", productKey=" + manufacturedUnitDetails.getProductKey() +
				", sockets=" + sockets +
				", ssid=" + ssid +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		UserUnitDetailsResponse that = (UserUnitDetailsResponse) o;

		if (!getId().equals(that.getId())) return false;
		if (!getName().equals(that.getName())) return false;
		if (!getRegistrationDate().equals(that.getRegistrationDate())) return false;
		if (!getUserId().equals(that.getUserId())) return false;
		if (!getManufacturedUnitDetails().equals(that.getManufacturedUnitDetails())) return false;
		return getSockets().equals(that.getSockets());
	}

	@Override
	public int hashCode() {
		int result = getId().hashCode();
		result = 31 * result + getName().hashCode();
		result = 31 * result + getRegistrationDate().hashCode();
		result = 31 * result + getUserId().hashCode();
		result = 31 * result + getManufacturedUnitDetails().hashCode();
		result = 31 * result + getSockets().hashCode();
		return result;
	}
}
