package com.aws.tappluxe.data.dto.responses;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserUnitSocketStatusDetailsResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	private Long id;
	
	@JsonProperty
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
	private Date updateDate;
	
	@JsonProperty
	private Long socketId;
	
	@JsonProperty
	private boolean state;

	public UserUnitSocketStatusDetailsResponse(Long id, Date updateDate, Long socketId, boolean state) {
		this.id = id;
		this.updateDate = updateDate;
		this.socketId = socketId;
		this.state = state;
	}

	public UserUnitSocketStatusDetailsResponse() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getSocketId() {
		return socketId;
	}

	public void setSocketId(Long socketId) {
		this.socketId = socketId;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "UserUnitSocketStatusDetailsResponse{" +
				"id=" + id +
				", updateDate=" + updateDate +
				", socketId=" + socketId +
				", state=" + state +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		UserUnitSocketStatusDetailsResponse that = (UserUnitSocketStatusDetailsResponse) o;

		if (isState() != that.isState()) return false;
		if (!getId().equals(that.getId())) return false;
		if (!getUpdateDate().equals(that.getUpdateDate())) return false;
		return getSocketId().equals(that.getSocketId());
	}

	@Override
	public int hashCode() {
		int result = getId().hashCode();
		result = 31 * result + getUpdateDate().hashCode();
		result = 31 * result + getSocketId().hashCode();
		result = 31 * result + (isState() ? 1 : 0);
		return result;
	}

}
