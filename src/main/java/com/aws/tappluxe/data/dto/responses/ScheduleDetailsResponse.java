package com.aws.tappluxe.data.dto.responses;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ScheduleDetailsResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty
	private Long id;
	
	@JsonProperty
	private String name;
	
	@JsonProperty
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss.SSSZ")
	private Date time;
	
	@JsonProperty
	private String repeat;
	
	@JsonProperty
	private Integer timeBeforeNotification;

	@JsonProperty("isActive")
	private Boolean active;

	public ScheduleDetailsResponse(Long id, String name, Date time, String repeat, Integer timeBeforeNotification, Boolean notificationEnabled, Boolean active) {
		this.id = id;
		this.name = name;
		this.time = time;
		this.repeat = repeat;
		this.timeBeforeNotification = timeBeforeNotification;
		this.active = active;
	}

	public ScheduleDetailsResponse() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getRepeat() {
		return repeat;
	}

	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}

	public Integer getTimeBeforeNotification() {
		return timeBeforeNotification;
	}

	public void setTimeBeforeNotification(Integer timeBeforeNotification) {
		this.timeBeforeNotification = timeBeforeNotification;
	}
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "ScheduleDetailsResponse{" +
				"id=" + id +
				", name='" + name + '\'' +
				", time=" + time +
				", repeat='" + repeat + '\'' +
				", timeBeforeNotification=" + timeBeforeNotification +
				", isActive=" + active +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ScheduleDetailsResponse that = (ScheduleDetailsResponse) o;

		if (!getId().equals(that.getId())) return false;
		if (!getName().equals(that.getName())) return false;
		if (!getTime().equals(that.getTime())) return false;
		if (!getRepeat().equals(that.getRepeat())) return false;
		if (!getTimeBeforeNotification().equals(that.getTimeBeforeNotification())) return false;
		return active.equals(that.active);
	}

	@Override
	public int hashCode() {
		int result = getId().hashCode();
		result = 31 * result + getName().hashCode();
		result = 31 * result + getTime().hashCode();
		result = 31 * result + getRepeat().hashCode();
		result = 31 * result + getTimeBeforeNotification().hashCode();
		result = 31 * result + active.hashCode();
		return result;
	}

}
