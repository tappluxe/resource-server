package com.aws.tappluxe.data.dto.requests;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.DecimalMin;

public class ProductLineUpdateRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty
	private String name;
	
	@JsonProperty
	@DecimalMin(value = "0", inclusive = false)
	private Integer numOfSockets;
	
	@JsonProperty
	@DecimalMin(value = "0", inclusive = false)
	private Integer loadCapacity;

	public ProductLineUpdateRequest(String name, Integer numOfSockets, Integer loadCapacity) {
		this.name = name;
		this.numOfSockets = numOfSockets;
		this.loadCapacity = loadCapacity;
	}

	public ProductLineUpdateRequest() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumOfSockets() {
		return numOfSockets;
	}

	public void setNumOfSockets(Integer numOfSockets) {
		this.numOfSockets = numOfSockets;
	}

	public Integer getLoadCapacity() {
		return loadCapacity;
	}

	public void setLoadCapacity(Integer loadCapacity) {
		this.loadCapacity = loadCapacity;
	}

}
