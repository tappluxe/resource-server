package com.aws.tappluxe.data.dto.requests;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserUnitCreateRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	@NotBlank
	private String productKey;
	
	@JsonProperty
	@NotBlank
	private String name;

	public UserUnitCreateRequest(String productKey, String name) {
		this.productKey = productKey;
		this.name = name;
	}

	public UserUnitCreateRequest() {
	}

	public String getProductKey() {
		return productKey;
	}

	public void setProductId(String productId) {
		this.productKey = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
