package com.aws.tappluxe.data.dto.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by franc.margallo on 3/11/2017.
 */
public class ScheduleUpdateRequest implements Serializable {

    @JsonProperty
    private String name;

    @JsonProperty
    private Date time;

    @JsonProperty
    private String repeat;

    @JsonProperty
    private Integer timeBeforeNotification;

    @JsonProperty
    private Boolean isActive;

    public ScheduleUpdateRequest(String name, Date time, String repeat, Integer timeBeforeNotification, Boolean notificationEnabled, Boolean isActive) {
        this.name = name;
        this.time = time;
        this.repeat = repeat;
        this.timeBeforeNotification = timeBeforeNotification;
        this.isActive = isActive;
    }

    public ScheduleUpdateRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public Integer getTimeBeforeNotification() {
        return timeBeforeNotification;
    }

    public void setTimeBeforeNotification(Integer timeBeforeNotification) {
        this.timeBeforeNotification = timeBeforeNotification;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

}
