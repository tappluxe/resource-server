package com.aws.tappluxe.data.dto.requests;

import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResetPasswordRequest {

	@JsonProperty
	private String email;
	
	@JsonProperty
	@Pattern(regexp = "(^$|^((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$/^&*();,+~`_|]).{6,20})$)")
	private String newPassword;
	
	@JsonProperty
	private String confirmPassword;
	
	public String getEmail() {
		return email;
	}
	
	public ResetPasswordRequest(){
	}
	
	public ResetPasswordRequest(String email, String newPassword, String confirmPassword){
		this.email = email;
		this.newPassword = newPassword;
		this.confirmPassword = confirmPassword;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	

}
