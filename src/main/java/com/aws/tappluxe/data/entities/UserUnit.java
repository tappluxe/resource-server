package com.aws.tappluxe.data.entities;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "user_units")
@SQLDelete(sql = "UPDATE user_units SET deleted=true WHERE id=?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted=false")
public class UserUnit {

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "registration_date")
	private Date registrationDate;

	@Column(name = "ssid")
	private String ssid;
	
	@OneToOne
	@JoinColumn
	private ManufacturedUnit manufacturedUnit;
	
	@ManyToOne
	private User user;

	@Column(name = "deleted")
	private Boolean deleted;
	
	@OneToMany(mappedBy = "userUnit", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<UserUnitSocket> userUnitSocket;

	public UserUnit(Long id, String name, Date registrationDate,
			ManufacturedUnit manufacturedUnit, User user) {
		this.id = id;
		this.name = name;
		this.registrationDate = registrationDate;
		this.manufacturedUnit = manufacturedUnit;
		this.user = user;
		this.deleted = false;
	}

	public UserUnit() {
		this.deleted = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public ManufacturedUnit getManufacturedUnit() {
		return manufacturedUnit;
	}

	public void setManufacturedUnit(ManufacturedUnit manufacturedUnit) {
		this.manufacturedUnit = manufacturedUnit;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<UserUnitSocket> getUserUnitSocket() {
		return userUnitSocket;
	}

	public void setUserUnitSocket(List<UserUnitSocket> userUnitSocket) {
		this.userUnitSocket = userUnitSocket;
	}
	
	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

}
