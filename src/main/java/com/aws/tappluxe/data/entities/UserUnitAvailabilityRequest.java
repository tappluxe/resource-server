package com.aws.tappluxe.data.entities;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by franc.margallo on 3/21/2017.
 */
@Entity
@Table(name = "user_unit_availability_requests")
@SQLDelete(sql = "UPDATE user_unit_availability_requests SET deleted=1=true WHERE id=?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted=false")
public class UserUnitAvailabilityRequest {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "acknowledged")
    private Boolean acknowledged;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "request_time")
    private Date requestTime;

    @ManyToOne
    private UserUnit userUnit;

    @Column(name = "deleted")
    private Boolean deleted;

    public UserUnitAvailabilityRequest(Long id, Date requestTime, UserUnit userUnit) {
        this.id = id;
        this.acknowledged = false;
        this.requestTime = requestTime;
        this.userUnit = userUnit;
        this.deleted = false;
    }

    public UserUnitAvailabilityRequest() {
        this.acknowledged = false;
        this.deleted = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(Boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public UserUnit getUserUnit() {
        return userUnit;
    }

    public void setUserUnit(UserUnit userUnit) {
        this.userUnit = userUnit;
    }
}
