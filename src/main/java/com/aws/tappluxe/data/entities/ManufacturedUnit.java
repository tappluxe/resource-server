package com.aws.tappluxe.data.entities;

import org.hibernate.annotations.GenericGenerator;

import java.util.Date;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Unique;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "manufactured_units")
public class ManufacturedUnit {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "product_key")
	private String productKey;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "manufactured_date")
	private Date manufacturedDate;
	
	@ManyToOne
	private ProductLine productLine;

	@OneToOne(mappedBy = "manufacturedUnit", fetch = FetchType.EAGER)
	private UserUnit userUnit;

	public ManufacturedUnit(String productKey, Date manufacturedDate, ProductLine productLine) {
		this.productKey = productKey;
		this.manufacturedDate = manufacturedDate;
		this.productLine = productLine;
	}

	public ManufacturedUnit() {
	}

	public String getProductKey() {
		return productKey;
	}

	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	public Date getManufacturedDate() {
		return manufacturedDate;
	}

	public void setManufacturedDate(Date manufacturedDate) {
		this.manufacturedDate = manufacturedDate;
	}

	public ProductLine getProductLine() {
		return productLine;
	}

	public void setProductLine(ProductLine productLine) {
		this.productLine = productLine;
	}

	public UserUnit getUserUnit() {
		return userUnit;
	}

	public void setUserUnit(UserUnit userUnit) {
		this.userUnit = userUnit;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
