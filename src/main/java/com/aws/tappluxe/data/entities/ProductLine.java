package com.aws.tappluxe.data.entities;

import javax.jdo.annotations.Column;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "product_lines")
public class ProductLine {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "num_of_sockets")
	private Integer numOfSockets;
	
	@Column(name = "load_capacity")
	private Integer loadCapacity;

	@OneToMany(mappedBy = "productLine", fetch = FetchType.EAGER)
	private List<ManufacturedUnit> manufacturedUnits;

	public ProductLine(Long id, String name, Integer numOfSockets, Integer loadCapacity) {
		this.id = id;
		this.name = name;
		this.numOfSockets = numOfSockets;
		this.loadCapacity = loadCapacity;
	}

	public ProductLine() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumOfSockets() {
		return numOfSockets;
	}

	public void setNumOfSockets(Integer numOfSockets) {
		this.numOfSockets = numOfSockets;
	}

	public Integer getLoadCapacity() {
		return loadCapacity;
	}

	public void setLoadCapacity(Integer loadCapacity) {
		this.loadCapacity = loadCapacity;
	}

	public List<ManufacturedUnit> getManufacturedUnits() {
		return manufacturedUnits;
	}

	public void setManufacturedUnits(List<ManufacturedUnit> manufacturedUnits) {
		this.manufacturedUnits = manufacturedUnits;
	}

}
