package com.aws.tappluxe.data.entities;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "devices")
@SQLDelete(sql = "UPDATE devices SET deleted=true WHERE id=?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted=false")
public class Device {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne
	private User user;

	@OneToOne
	@JoinColumn
	private UserUnitSocket userUnitSocket;

	@Column(name = "deleted")
	private Boolean deleted;

	public Device(Long id, String name, User user, UserUnitSocket userUnitSocket) {
		this.id = id;
		this.name = name;
		this.user = user;
		this.userUnitSocket = userUnitSocket;
		deleted = false;
	}

	public Device() {
		deleted = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserUnitSocket getUserUnitSocket() {
		return userUnitSocket;
	}

	public void setUserUnitSocket(UserUnitSocket userUnitSocket) {
		this.userUnitSocket = userUnitSocket;
	}
	
}
