package com.aws.tappluxe.data.entities;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "user_unit_sockets")
@SQLDelete(sql = "UPDATE user_unit_sockets SET deleted=true WHERE id=?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted=false")
public class UserUnitSocket {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "socket_number")
	private Integer socketNumber;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "userUnitSocket")
	private Device device;
	
	@ManyToOne
	private UserUnit userUnit;

	@ManyToOne
	private Schedule schedule;

	@Column(name = "deleted")
	private Boolean deleted;

	@OneToMany(mappedBy = "userUnitSocket", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("updateDate desc")
	private List<UserUnitSocketStatus> statusHistory;

	public UserUnitSocket(Long id, Integer socketNumber, Device device, UserUnit userUnit, Schedule schedule) {
		this.id = id;
		this.socketNumber = socketNumber;
		this.device = device;
		this.userUnit = userUnit;
		this.schedule = schedule;
		this.deleted = false;
	}

	public UserUnitSocket() {
		this.deleted = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSocketNumber() {
		return socketNumber;
	}

	public void setSocketNumber(Integer socketNumber) {
		this.socketNumber = socketNumber;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public UserUnit getUserUnit() {
		return userUnit;
	}

	public void setUserUnit(UserUnit userUnit) {
		this.userUnit = userUnit;
	}

	public List<UserUnitSocketStatus> getStatusHistory() {
		return statusHistory;
	}

	public void setStatusHistory(List<UserUnitSocketStatus> statusHistory) {
		this.statusHistory = statusHistory;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	@Override
	public String toString() {
		return "UserUnitSocket{" +
				"id=" + id +
				", socketNumber=" + socketNumber +
				", device=" + device +
				", userUnit=" + userUnit +
				", schedule=" + schedule +
				", deleted=" + deleted +
				", statusHistory=" + statusHistory +
				'}';
	}

}
