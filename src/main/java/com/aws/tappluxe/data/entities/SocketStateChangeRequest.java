package com.aws.tappluxe.data.entities;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "socket_state_change_requests")
@SQLDelete(sql = "UPDATE socket_state_change_requests SET deleted=true WHERE id=?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted=false")
public class SocketStateChangeRequest {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	private UserUnitSocket userUnitSocket;

	@Column(name = "state")
	private Boolean state;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_requested")
	private Date dateRequested;
	
	@Column(name = "acknowledged")
	private Boolean acknowledged;

	@Column(name = "time_out")
	private Boolean timedOut;
	
	@ManyToOne
	private User user;

	@Column(name = "deleted")
	private Boolean deleted;

	public SocketStateChangeRequest(UserUnitSocket userUnitSocket, Boolean state, Date dateRequested, User user) {
		this.userUnitSocket = userUnitSocket;
		this.state = state;
		this.dateRequested = dateRequested;
		this.acknowledged = false;
		this.timedOut = false;
		this.user = user;
		this.deleted = false;
	}

	public SocketStateChangeRequest() {
		this.acknowledged = false;
		this.timedOut = false;
		this.deleted = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserUnitSocket getUserUnitSocket() {
		return userUnitSocket;
	}

	public void setUserUnitSocket(UserUnitSocket userUnitSocket) {
		this.userUnitSocket = userUnitSocket;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public Date getDateRequested() {
		return dateRequested;
	}

	public void setDateRequested(Date dateRequested) {
		this.dateRequested = dateRequested;
	}

	public Boolean getAcknowledged() {
		return acknowledged;
	}

	public void setAcknowledged(Boolean acknowledged) {
		this.acknowledged = acknowledged;
	}

	public Boolean getTimedOut() {
		return timedOut;
	}

	public void setTimedOut(Boolean timedOut) {
		this.timedOut = timedOut;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
