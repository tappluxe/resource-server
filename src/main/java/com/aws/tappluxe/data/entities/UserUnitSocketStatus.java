package com.aws.tappluxe.data.entities;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "user_unit_socket_statuses")
@SQLDelete(sql = "UPDATE user_unit_socket_statuses SET deleted=true WHERE id=?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted=false")
public class UserUnitSocketStatus {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	private Date updateDate;
	
	@Column(name = "state")
	private Boolean state;
	
	@ManyToOne
	private UserUnitSocket userUnitSocket;

	@Column(name = "deleted")
	private Boolean deleted;

	public UserUnitSocketStatus(Long id, Date updateDate, Boolean state, UserUnitSocket userUnitSocket) {
		this.id = id;
		this.updateDate = updateDate;
		this.state = state;
		this.userUnitSocket = userUnitSocket;
		this.deleted = false;
	}

	public UserUnitSocketStatus() {
		this.deleted = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public UserUnitSocket getUserUnitSocket() {
		return userUnitSocket;
	}

	public void setUserUnitSocket(UserUnitSocket userUnitSocket) {
		this.userUnitSocket = userUnitSocket;
	}
	
}
