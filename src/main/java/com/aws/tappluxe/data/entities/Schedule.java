package com.aws.tappluxe.data.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "user_schedules")
@SQLDelete(sql = "UPDATE user_schedules SET deleted=true WHERE id=?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted=false")
public class Schedule {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "schedule_time")
	private Date time;
	
	@Column(name = "schedule_repeat")
	private String repeat;
	
	@Column(name = "time_before_notification")
	private Integer timeBeforeNotification;
	
	@Column(name = "active")
	private Boolean isActive;
	
	@ManyToOne
	private User user;

	@OneToMany(mappedBy = "schedule")
	private List<UserUnitSocket> userUnitSockets;

	@Column(name = "deleted")
	private Boolean deleted;

	public Schedule(Long id, String name, Date time, String repeat, Integer timeBeforeNotification, Boolean notificationEnabled, User user) {
		this.id = id;
		this.name = name;
		this.time = time;
		this.repeat = repeat;
		this.timeBeforeNotification = timeBeforeNotification;
		this.user = user;
		this.isActive = true;
		this.deleted = false;
	}

	public Schedule() {
		this.isActive = true;
		this.deleted = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getRepeat() {
		return repeat;
	}

	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}

	public Integer getTimeBeforeNotification() {
		return timeBeforeNotification;
	}

	public void setTimeBeforeNotification(Integer timeBeforeNotification) {
		this.timeBeforeNotification = timeBeforeNotification;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean isActive() {
		return isActive;
	}

	public void setActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public List<UserUnitSocket> getUserUnitSockets() {
		return userUnitSockets;
	}

	public void setUserUnitSockets(List<UserUnitSocket> userUnitSockets) {
		this.userUnitSockets = userUnitSockets;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

}
