package com.aws.tappluxe.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.aws.tappluxe.data.entities.UserUnit;

@Repository
public interface UserUnitDao extends JpaRepository<UserUnit, Long>, QueryDslPredicateExecutor<UserUnit> {

}
