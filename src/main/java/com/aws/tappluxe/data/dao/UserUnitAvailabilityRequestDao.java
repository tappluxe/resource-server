package com.aws.tappluxe.data.dao;

import com.aws.tappluxe.data.entities.UserUnitAvailabilityRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by franc.margallo on 3/21/2017.
 */
@Repository
@Transactional
public interface UserUnitAvailabilityRequestDao extends JpaRepository<UserUnitAvailabilityRequest, Long>, QueryDslPredicateExecutor<UserUnitAvailabilityRequest> {
}
