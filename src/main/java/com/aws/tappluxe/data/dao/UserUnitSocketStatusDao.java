package com.aws.tappluxe.data.dao;

import com.aws.tappluxe.data.entities.UserUnitSocketStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by franc.margallo on 3/11/2017.
 */
@Repository
@Transactional
public interface UserUnitSocketStatusDao extends JpaRepository<UserUnitSocketStatus, Long>, QueryDslPredicateExecutor<UserUnitSocketStatus> {
}
