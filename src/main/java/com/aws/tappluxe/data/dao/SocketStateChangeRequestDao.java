package com.aws.tappluxe.data.dao;

import javax.transaction.Transactional;

import com.aws.tappluxe.data.entities.SocketStateChangeRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface SocketStateChangeRequestDao extends JpaRepository<SocketStateChangeRequest, Long>, QueryDslPredicateExecutor<SocketStateChangeRequest> {

}
