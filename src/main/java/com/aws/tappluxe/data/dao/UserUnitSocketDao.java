package com.aws.tappluxe.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.aws.tappluxe.data.entities.UserUnitSocket;

@Repository
public interface UserUnitSocketDao extends JpaRepository<UserUnitSocket, Long>, QueryDslPredicateExecutor<UserUnitSocket> {

}
