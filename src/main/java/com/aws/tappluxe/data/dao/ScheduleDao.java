package com.aws.tappluxe.data.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.aws.tappluxe.data.entities.Schedule;

@Repository
@Transactional
public interface ScheduleDao extends JpaRepository<Schedule, Long>, QueryDslPredicateExecutor<Schedule> {

}
