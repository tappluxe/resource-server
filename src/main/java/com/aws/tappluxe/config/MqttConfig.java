package com.aws.tappluxe.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

import com.aws.tappluxe.services.MqttService;

@Component
public class MqttConfig {
	
	@Autowired
	private MqttService mqttService;
	
	@Bean
	public MqttPahoClientFactory mqttClientFactory() {
		DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
		factory.setServerURIs("tcp://localhost:1883");
		factory.setUserName("test");
		factory.setPassword("test");
		return factory;
	}
	
	@Bean
	public MessageProducer mqttInbound() {
		MqttPahoMessageDrivenChannelAdapter adapter = new MqttPahoMessageDrivenChannelAdapter("1f00a5a3-9726-4673-ab80-1825711d44fd", mqttClientFactory(), "testtest");
		adapter.setAutoStartup(true);
		adapter.setCompletionTimeout(5000);
		adapter.setConverter(new DefaultPahoMessageConverter());
		adapter.setOutputChannel(mqttInputChannel());
		return adapter;
	}
	
	@Bean
	@ServiceActivator(inputChannel = "mqttOutputChannel", autoStartup = "true")
	public MessageHandler mqttOutbound() {
		MqttPahoMessageHandler messageHandler = new MqttPahoMessageHandler("server", mqttClientFactory());
		messageHandler.setAsync(true);
		messageHandler.setDefaultTopic("testtest");
		return messageHandler;
	}
	
	@Bean
	public MessageChannel mqttInputChannel() {
		DirectChannel inputChannel = new DirectChannel();
		inputChannel.subscribe(inboundHandler());
		return inputChannel;
	}
	
	@Bean
	public MessageChannel mqttOutputChannel() {
		DirectChannel outputChannel = new DirectChannel();
		outputChannel.subscribe(mqttOutbound());
		return outputChannel;
	}	
	
	@MessagingGateway(defaultRequestChannel = "mqttOutputChannel")
	public interface MqttOutbound {

	    void sendToMqtt(String data);

	}
	
	@Bean
	@ServiceActivator(inputChannel = "mqttInputChannel")
	public MessageHandler inboundHandler() {
		return new MessageHandler() {			
			@Override
			public void handleMessage(Message<?> message) throws MessagingException {
				mqttService.processInboundMessage(message.getPayload().toString());
			}
		};		
		
	}
	
}
