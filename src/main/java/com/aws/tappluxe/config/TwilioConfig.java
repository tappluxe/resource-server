package com.aws.tappluxe.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.twilio.sdk.TwilioRestClient;

@Configuration
public class TwilioConfig {
	
    @Value("${twilio.accountSID}")
    private String accountSID;
    
    @Value("${twilio.authToken}")
    private String authToken;

    @Bean
    public TwilioRestClient twilioRestClient() {
        return new TwilioRestClient(accountSID, authToken);
    }
    
    @Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
