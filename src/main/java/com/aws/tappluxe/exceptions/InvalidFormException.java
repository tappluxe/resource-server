package com.aws.tappluxe.exceptions;

import org.springframework.validation.Errors;

/**
 * Created by franc.margallo on 3/10/2017.
 */
@SuppressWarnings("serial")
public class InvalidFormException extends RuntimeException {

    private Errors errors;

    public InvalidFormException(Errors errors) {
        super("Invalid Request");
        this.errors = errors;
    }

    public InvalidFormException(String message, Errors errors) {
        super(message);
        this.errors = errors;
    }

    public Errors getErrors() {
        return errors;
    }

}
