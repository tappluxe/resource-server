package com.aws.tappluxe.tasks;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.aws.tappluxe.data.dao.ScheduleDao;
import com.aws.tappluxe.data.entities.QSchedule;
import com.aws.tappluxe.data.entities.QUserUnitSocket;
import com.aws.tappluxe.data.entities.Schedule;
import com.aws.tappluxe.data.entities.UserUnitSocket;
import com.aws.tappluxe.services.SMSMessageService;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.hibernate.HibernateQuery;

@Component
@Transactional
public class ScheduleNotificationTask {
	
	@Autowired
	public ScheduleDao scheduleDao;

	@Autowired
	public SMSMessageService smsMessageService;
	
	@Autowired
    private SessionFactory sessionFactory;
	
	@Scheduled(cron = "0 * * * * *")
	public void sendScheduleNotifications() {
		Date now = new Date();
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		QSchedule schedulePredicate = QSchedule.schedule;
		Predicate predicate = schedulePredicate.isActive.eq(true)
				.and(schedulePredicate.deleted.eq(false))
				.and(schedulePredicate.repeat.contains(Integer.toString(day)))
				.and(schedulePredicate.timeBeforeNotification.gt(0));
		Iterable<Schedule> all = scheduleDao.findAll(predicate);
		QUserUnitSocket socketPredicate = QUserUnitSocket.userUnitSocket;
		for (Schedule sched: all) {
			Date scheduled = resolveDate(sched.getTime());
			double diff = diffInMins(now, scheduled);
			if ((int)diff == sched.getTimeBeforeNotification()) {
				String text = String.format("Schedule Notification\n\nSocket(s) assigned to the schedule named \"%s\" will be turned off in %d minutes.\n\n", sched.getName(), sched.getTimeBeforeNotification());
				HibernateQuery<UserUnitSocket> socketQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
				List<UserUnitSocket> sockets = socketQuery.from(socketPredicate).where(socketPredicate.schedule.id.eq(sched.getId())).fetch();
				for (UserUnitSocket socket: sockets) {
					text += String.format("* %s(Socket%d): %s\n\n", socket.getUserUnit().getName(), socket.getSocketNumber(), socket.getDevice().getName());
				}

				smsMessageService.createSMSMessage(sched.getUser().getPhone(), text);
			}
		}
	}
	
	public double diffInMins(Date earlier, Date later) {
	    return ((later.getTime()/60000.0) - (earlier.getTime()/60000.0));
	}
	
	public Date resolveDate(Date time) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(time);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		int mill = cal.get(Calendar.MILLISECOND);
		cal.setTime(new Date());
		cal.setTimeZone(TimeZone.getDefault());
		cal.set(Calendar.HOUR_OF_DAY,hour);
		cal.set(Calendar.MINUTE,minute);
		cal.set(Calendar.SECOND,second);
		cal.set(Calendar.MILLISECOND,mill);

		Date d = cal.getTime();
		return d;
	}
	

}
