package com.aws.tappluxe.tasks;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.aws.tappluxe.data.dao.VerificationCodeDao;
import com.aws.tappluxe.data.entities.QVerificationCode;
import com.aws.tappluxe.data.entities.VerificationCode;
import com.querydsl.core.types.Predicate;

@Component
public class VerificationCodeTasks {
	
	@Autowired
	public VerificationCodeDao verificationCodeDao;
	
	
	@Scheduled(cron = "0 * * * * *")
	public void checkVerificationCodeExpiration() {
		QVerificationCode verificationCodePredicate = QVerificationCode.verificationCode;
		Predicate predicate = verificationCodePredicate.isValid.eq(false)
			.or(verificationCodePredicate.created.before(
					new Date(System.currentTimeMillis() - (VerificationCode.validity * 60 * 60 * 1000))));
		Iterable<VerificationCode> codes = verificationCodeDao.findAll(predicate);
		verificationCodeDao.deleteInBatch(codes);
	}
}
