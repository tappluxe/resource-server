package com.aws.tappluxe.tasks;

import com.aws.tappluxe.data.dao.ScheduleDao;
import com.aws.tappluxe.data.dao.UserUnitSocketDao;
import com.aws.tappluxe.data.entities.QSchedule;
import com.aws.tappluxe.data.entities.QUserUnitSocket;
import com.aws.tappluxe.data.entities.SocketStateChangeRequest;
import com.aws.tappluxe.services.MqttService;
import com.aws.tappluxe.services.SMSMessageService;
import com.aws.tappluxe.services.SocketStateChangeRequestService;
import com.querydsl.core.BooleanBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by franc.margallo on 3/23/2017.
 */
@Component
@Transactional
public class ScheduleExecutorTask {

    @Autowired
    private ScheduleDao scheduleDao;

    @Autowired
    private UserUnitSocketDao userUnitSocketDao;

    @Autowired
    private SocketStateChangeRequestService socketStateChangeRequestService;

    @Autowired
    private SMSMessageService smsMessageService;

    @Autowired
    private MessageSource msgSource;
    
    private QSchedule schedulePredicate;

    private QUserUnitSocket userUnitSocketPredicate;

    @Autowired
    public ScheduleExecutorTask() {
        this.schedulePredicate = QSchedule.schedule;
        this.userUnitSocketPredicate = QUserUnitSocket.userUnitSocket;
    }

    @Scheduled(cron = "0 * * * * *")
    public void executeSchedule() {
        LocalDateTime now = LocalDateTime.now();
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        BooleanBuilder booleanPredicate = new BooleanBuilder(schedulePredicate.repeat.contains(Integer.toString(day)));
        booleanPredicate.and(schedulePredicate.time.hour().eq(now.getHour()));
        booleanPredicate.and(schedulePredicate.time.minute().eq(now.getMinute()));
        booleanPredicate.and(schedulePredicate.isActive.eq(true));
        scheduleDao.findAll(booleanPredicate).forEach(schedule -> {
            userUnitSocketDao.findAll(userUnitSocketPredicate.schedule.eq(schedule)).forEach(unitSocket -> {
                SocketStateChangeRequest request = socketStateChangeRequestService.create(schedule.getUser(), unitSocket, false);
                try {
                    byte result = socketStateChangeRequestService.execute(request);
                    if (result == 1) {

                        smsMessageService.createSMSMessage(schedule.getUser().getPhone(), msgSource.getMessage("Success.schedule.execute",
                                new Object[] {schedule.getName(), unitSocket.getUserUnit().getName(), unitSocket.getSocketNumber()},
                                LocaleContextHolder.getLocale()));
                    } else {
                        smsMessageService.createSMSMessage(schedule.getUser().getPhone(), msgSource.getMessage("Fail.schedule.execute",
                                new Object[] {schedule.getName()},
                                LocaleContextHolder.getLocale()));
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        });
    }

}
