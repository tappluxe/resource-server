package com.aws.tappluxe.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.aws.tappluxe.data.dao.QueuedEmailDao;
import com.aws.tappluxe.data.entities.QQueuedEmail;
import com.aws.tappluxe.data.entities.QueuedEmail;
import com.aws.tappluxe.services.EmailService;

@Component
public class SendEmailTask {
	
	@Autowired
	public QueuedEmailDao queuedEmailDao;
	
	@Autowired
	public EmailService emailService;
	
	@Scheduled(fixedRate = 5000)
	public void sendQueuedEmails() {
		QQueuedEmail queuedEmailPredicate = QQueuedEmail.queuedEmail;
		Iterable<QueuedEmail> queue = queuedEmailDao.findAll(queuedEmailPredicate.isSent.eq(false));
		for (QueuedEmail email: queue) {
			boolean sent = emailService.sendSimpleMessage(email.getToAddress(), email.getSubject(), email.getBody());
			if (sent) {
				email.setSent();
				queuedEmailDao.saveAndFlush(email);
			}
		}
	}
}
