package com.aws.tappluxe.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.aws.tappluxe.data.dao.SMSMessageDao;
import com.aws.tappluxe.data.entities.QSMSMessage;
import com.aws.tappluxe.data.entities.SMSMessage;
import com.aws.tappluxe.services.SMSMessageService;

@Component
public class SendSMSTask {
	
	@Autowired
	public SMSMessageDao smsMessageDao;
	
	@Autowired
	public SMSMessageService smsMessageService;
	
	@Scheduled(fixedRate = 5000)
	public void sendSMSMessages() {
		QSMSMessage smsPredicate = QSMSMessage.sMSMessage;
		Iterable<SMSMessage> queue = smsMessageDao.findAll(smsPredicate.isSent.eq(false));
		for (SMSMessage sms: queue) {
			boolean sent = smsMessageService.sendSMSMessage(sms.getToNumber(), sms.getText());
			if (sent) {
				sms.setSent();
				smsMessageDao.saveAndFlush(sms);
			}
		}
	}
}
