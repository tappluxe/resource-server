package com.aws.tappluxe.controllers;

import com.aws.tappluxe.exceptions.InvalidFormException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by franc.margallo on 3/10/2017.
 */
@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource msgSource;

    @ExceptionHandler({InvalidFormException.class})
    protected ResponseEntity<?> handleInvalidRequest(RuntimeException runtimeException, WebRequest webRequest) {
        InvalidFormException invalidRequestException = (InvalidFormException) runtimeException;
        Map<String, String> errors = new HashMap<>();
        for (FieldError fieldError : invalidRequestException.getErrors().getFieldErrors()) {
            errors.put(fieldError.getField(), msgSource.getMessage(fieldError, null));
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(runtimeException, errors, headers, HttpStatus.UNPROCESSABLE_ENTITY, webRequest);
    }

}
