package com.aws.tappluxe.controllers;

import com.aws.tappluxe.services.UserUnitSocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.concurrent.Callable;

/**
 * Created by franc.margallo on 3/11/2017.
 */
@RestController
@RequestMapping("/api/user_units/{userUnitId}/socket/{socketId}")
public class  UserUnitSocketController {

    @Autowired
    private UserUnitSocketService userUnitSocketService;

    @GetMapping
    public ResponseEntity<?> show(@PathVariable("userUnitId") Long userUnitId,
                                  @PathVariable("socketId") Long socketId,
                                  Principal principal) {
        return userUnitSocketService.show(userUnitId, socketId, principal.getName());
    }

    @GetMapping("/change_state")
    public Callable<ResponseEntity<?>> changeState(@PathVariable("userUnitId") Long userUnitId,
                                                         @PathVariable("socketId") Long socketId,
                                                         @RequestParam("state") Boolean state,
                                                         Principal principal) {
        return () -> userUnitSocketService.changeState(userUnitId, socketId, state, principal.getName());
    }

    @GetMapping("/manage_schedule")
    public ResponseEntity<?> manageSchedule(@PathVariable("userUnitId") Long userUnitId,
                                            @PathVariable("socketId") Long socketId,
                                            @RequestParam("schedule_id") Long scheduleId,
                                            Principal principal) {
        return userUnitSocketService.manageSchedule(userUnitId, socketId, scheduleId, principal.getName());
    }

}
