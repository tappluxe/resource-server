package com.aws.tappluxe.controllers;

import java.security.Principal;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.aws.tappluxe.data.dto.requests.UserUnitCreateRequest;
import com.aws.tappluxe.data.dto.requests.UserUnitUpdateRequest;
import com.aws.tappluxe.services.UserUnitService;

@RestController
@RequestMapping("/api/user_units")
public class UserUnitController {
	
	@Autowired
	private UserUnitService userUnitService;
	
	@GetMapping
	public ResponseEntity<?> all(Principal principal) {
		return userUnitService.all(principal.getName());
	}
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody @Validated UserUnitCreateRequest userUnitCreate, BindingResult result, Principal principal) {
		return userUnitService.create(userUnitCreate, principal.getName(), result);
	}
	
	@GetMapping("/{userUnitId}")
	public ResponseEntity<?> show(@PathVariable("userUnitId") Long deviceId, Principal principal) {
		return userUnitService.show(deviceId, principal.getName());
	}
	
	@PostMapping("/{userUnitId}")
	public ResponseEntity<?> update(
			@PathVariable("userUnitId") Long userUnitId, 
			@RequestBody UserUnitUpdateRequest userUnitUpdate,
			BindingResult result,
			Principal principal) {
		return userUnitService.update(userUnitId, principal.getName(), userUnitUpdate, result);
	}
	
	@DeleteMapping("/{userUnitId}")
	public ResponseEntity<?> delete(@PathVariable("userUnitId") Long userUnitId, Principal principal) {
		return userUnitService.delete(userUnitId, principal.getName());
	}

	@GetMapping("/{userUnitId}/availability")
	public Callable<ResponseEntity<?>> availability(@PathVariable("userUnitId") Long userUnitId, Principal principal) {
		return () -> userUnitService.availability(userUnitId, principal.getName());
	}

}
