package com.aws.tappluxe.controllers;

import java.security.Principal;

import com.aws.tappluxe.data.dto.requests.ScheduleUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aws.tappluxe.data.dto.requests.ScheduleCreateRequest;
import com.aws.tappluxe.services.ScheduleService;

@RestController
@RequestMapping("/api/schedules")
public class ScheduleController {

	@Autowired
	private ScheduleService scheduleService;
	
	@GetMapping
	public ResponseEntity<?> all(Principal principal) {
		return scheduleService.all(principal.getName());
	}
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody @Validated ScheduleCreateRequest scheduleCreate, BindingResult result, Principal principal) {
		return scheduleService.create(scheduleCreate, principal.getName(), result);
	}
	
	@GetMapping("/{scheduleId}")
	public ResponseEntity<?> show(@PathVariable("scheduleId") Long deviceId, Principal principal) {
		return scheduleService.show(deviceId, principal.getName());
	}
	
	@PostMapping("/{scheduleId}")
	public ResponseEntity<?> update(
			@PathVariable("scheduleId") Long deviceId,
			@RequestBody ScheduleUpdateRequest scheduleUpdate,
			Principal principal) {
		return scheduleService.update(deviceId, principal.getName(), scheduleUpdate);
	}
	
	@DeleteMapping("/{scheduleId}")
	public ResponseEntity<?> delete(@PathVariable("scheduleId") Long deviceId, Principal principal) {
		return scheduleService.delete(deviceId, principal.getName());
	}
	
}
