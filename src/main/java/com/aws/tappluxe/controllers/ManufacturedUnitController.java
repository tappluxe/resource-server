package com.aws.tappluxe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.aws.tappluxe.services.ManufacturedUnitService;

@RestController
@RequestMapping("/api/admin/manufactured_units")
public class ManufacturedUnitController {
	
	@Autowired
	private ManufacturedUnitService manufacturedUnitService;
	
	@GetMapping
	public ResponseEntity<?> all() {
		return manufacturedUnitService.all();
	}
	
	@PostMapping
	public ResponseEntity<?> create(@RequestParam("productLineId") Long productLineId) {
		return manufacturedUnitService.create(productLineId);
	}
	
	@GetMapping("/{unitId}")
	public ResponseEntity<?> show(@PathVariable("unitId") Long unitId) {
		return manufacturedUnitService.show(unitId);
	}
	
	@DeleteMapping("/{unitId}")
	public ResponseEntity<?> delete(@PathVariable("unitId") Long unitId) {
		return manufacturedUnitService.delete(unitId);
	}

}
