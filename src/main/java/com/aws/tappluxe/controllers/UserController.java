package com.aws.tappluxe.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aws.tappluxe.data.dto.requests.UserUpdateRequest;
import com.aws.tappluxe.services.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping
	public ResponseEntity<?> show(Principal principal) {
		return userService.show(principal.getName());
	}
	
	@PostMapping
	public ResponseEntity<?> update(@RequestBody UserUpdateRequest userUpdate, BindingResult result, Principal principal) {
		return userService.update(principal.getName(), userUpdate, result);
	}
	
	@DeleteMapping
	public ResponseEntity<?> delete(Principal principal) {
		return userService.delete(principal.getName());
	}

}
