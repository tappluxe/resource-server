package com.aws.tappluxe.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.aws.tappluxe.data.dto.requests.DeviceCreateRequest;
import com.aws.tappluxe.data.dto.requests.DeviceUpdateRequest;
import com.aws.tappluxe.services.DeviceService;

@RestController
@RequestMapping("/api/devices")
public class DeviceController {
	
	@Autowired
	private DeviceService deviceService;
	
	@GetMapping
	public ResponseEntity<?> all(@RequestParam(value = "attached_to", required = false) Boolean attachedTo, Principal principal) {
		return deviceService.all(attachedTo, principal.getName());
	}
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody @Validated DeviceCreateRequest deviceCreate, BindingResult result, Principal principal) {
		return deviceService.create(deviceCreate, principal.getName(),result);
	}
	
	@GetMapping("/{deviceId}")
	public ResponseEntity<?> show(@PathVariable("deviceId") Long deviceId, Principal principal) {
		return deviceService.show(deviceId, principal.getName());
	}
	
	@PostMapping("/{deviceId}")
	public ResponseEntity<?> update(
			@PathVariable("deviceId") Long deviceId,
			@RequestBody DeviceUpdateRequest deviceUpdate,
			BindingResult result,
			Principal principal) {
		return deviceService.update(deviceId, principal.getName(), deviceUpdate, result);
	}
	
	@DeleteMapping("/{deviceId}")
	public ResponseEntity<?> delete(@PathVariable("deviceId") Long deviceId, Principal principal) {
		return deviceService.delete(deviceId, principal.getName());
	}
	
}
