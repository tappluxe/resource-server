package com.aws.tappluxe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aws.tappluxe.data.dto.requests.ResetPasswordRequest;
import com.aws.tappluxe.data.dto.requests.UserCreateRequest;
import com.aws.tappluxe.services.MembershipService;

@RestController
@RequestMapping("/api/membership")
public class MembershipController {

	@Autowired
	private MembershipService membershipService;

	@PostMapping
	public ResponseEntity<?> register(@RequestBody @Validated UserCreateRequest userCreate, BindingResult result) {
		return membershipService.register(userCreate, result);
	}

	@GetMapping("/check/email")
	public ResponseEntity<?> checkEmail(@RequestParam("email") String email) {
		return membershipService.checkEmail(email);
	}

	@GetMapping("/check/product_id")
	public ResponseEntity<?> checkProductKey(@RequestParam("productId") String productId) {
		return membershipService.checkProductId(productId);
	}
	
	@GetMapping("/forgot_password")
	public ResponseEntity<?> forgotPassword(@RequestParam("email") String email) {
		return membershipService.forgotPassword(email);
	}
	
	@PostMapping("/reset_password")
	public ResponseEntity<?> resetPassword(@RequestBody @Validated ResetPasswordRequest resetRequest) {
		return membershipService.resetPassword(resetRequest);
	}
}
