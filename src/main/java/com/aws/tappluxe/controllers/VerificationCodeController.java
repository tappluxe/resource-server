package com.aws.tappluxe.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aws.tappluxe.services.VerificationCodeService;

@RestController
@RequestMapping("/api/verification")
public class VerificationCodeController {
	
	@Autowired
	private VerificationCodeService verificationCodeService;
	
	@PostMapping("/check")
	public ResponseEntity<?> checkCode(@RequestParam("code") String code, @RequestParam("email") String email) {
		return verificationCodeService.checkVerificationCode(code, email);
	}
	
	@GetMapping("/resend")
	public ResponseEntity<?> resendCode(@RequestParam("email") String email) {
		return verificationCodeService.resendCode(email);
	}
	
	@GetMapping("/isVerified")
	public ResponseEntity<?> isVerified(Principal principal) {
		return verificationCodeService.isVerified(principal.getName());
	}

}
