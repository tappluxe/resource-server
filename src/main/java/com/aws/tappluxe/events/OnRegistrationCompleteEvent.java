package com.aws.tappluxe.events;

import org.springframework.context.ApplicationEvent;

import com.aws.tappluxe.data.entities.User;

public class OnRegistrationCompleteEvent extends ApplicationEvent {

	private static final long serialVersionUID = -7709015796134604475L;
	
	private User user;
	
	public OnRegistrationCompleteEvent(User user) {
		super(user);
		this.user = user;
	}
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
