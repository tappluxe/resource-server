package com.aws.tappluxe.events.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.aws.tappluxe.data.dao.SMSMessageDao;
import com.aws.tappluxe.data.entities.SMSMessage;
import com.aws.tappluxe.data.entities.User;
import com.aws.tappluxe.data.entities.VerificationCode;
import com.aws.tappluxe.events.OnRegistrationCompleteEvent;
import com.aws.tappluxe.services.EmailService;
import com.aws.tappluxe.services.VerificationCodeService;

@Component
public class RegistrationListener {
	
	@Autowired
	public VerificationCodeService verificationCodeService;
	
	@Autowired
	public EmailService emailService;
	
	@Autowired
	public SMSMessageDao smsMessageDao;
	
	@Async
	@EventListener
	public void verifyAccount(OnRegistrationCompleteEvent event) {
		User user = event.getUser();
		VerificationCode code = verificationCodeService.generateVerificationCode(user.getEmail());
		emailService.createEmail(user.getEmail(), "Tappluxe - Email Verification", "Verification Code: " + code.getCode());
			
		SMSMessage sms = new SMSMessage();
		sms.setText("Tappluxe Verification Code: " + code.getCode());
		sms.setToNumber(user.getPhone());
		sms.setIsSent(false);
		smsMessageDao.saveAndFlush(sms);
	}
	

}
