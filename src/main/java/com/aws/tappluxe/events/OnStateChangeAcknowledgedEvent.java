package com.aws.tappluxe.events;

import org.springframework.context.ApplicationEvent;

/**
 * Created by franc.margallo on 3/11/2017.
 */
public class OnStateChangeAcknowledgedEvent extends ApplicationEvent {

    private Long requestId;

    public OnStateChangeAcknowledgedEvent(Long requestId) {
        super(requestId);
        this.requestId = requestId;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

}
