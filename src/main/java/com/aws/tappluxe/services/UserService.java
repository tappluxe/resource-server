package com.aws.tappluxe.services;

import org.springframework.http.ResponseEntity;
import com.aws.tappluxe.data.dto.requests.UserUpdateRequest;
import org.springframework.validation.BindingResult;

public interface UserService {

	public ResponseEntity<?> all();
	
	public ResponseEntity<?> show(String email);
	
	public ResponseEntity<?> update(String email, UserUpdateRequest userUpdate, BindingResult result);
	
	public ResponseEntity<?> delete(String email);
		
}
