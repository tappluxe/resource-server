package com.aws.tappluxe.services;

import com.aws.tappluxe.data.entities.SMSMessage;

public interface SMSMessageService {
	
	public boolean sendSMSMessage(String toNumber, String text);
	
	public SMSMessage createSMSMessage(String toNumber, String text);
}
