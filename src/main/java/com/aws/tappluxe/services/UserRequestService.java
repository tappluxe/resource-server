package com.aws.tappluxe.services;

import com.aws.tappluxe.data.entities.SocketStateChangeRequest;

/**
 * Created by franc.margallo on 3/16/2017.
 */
public interface UserRequestService {

    public SocketStateChangeRequest newRequest(SocketStateChangeRequest socketStateChangeRequest);

    public SocketStateChangeRequest selectRequest(Long requestId);

}
