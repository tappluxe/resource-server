package com.aws.tappluxe.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import com.aws.tappluxe.data.dao.QueuedEmailDao;
import com.aws.tappluxe.data.entities.QueuedEmail;
import com.aws.tappluxe.services.EmailService;


@Service
@Configurable
public class EmailServiceImpl implements EmailService {
	
	@Autowired
	private MailSender mailSender;
	
	@Autowired
	private QueuedEmailDao queuedEmailDao;

	@Override
	public boolean sendSimpleMessage(String to, String subject, String text) {
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setTo(to);
		simpleMailMessage.setSubject(subject);
		simpleMailMessage.setText(text);
		this.mailSender.send(simpleMailMessage);
		return true;
	}

	@Override
	public QueuedEmail createEmail(String to, String subject, String text) {
		QueuedEmail email = new QueuedEmail();
		email.setToAddress(to);
		email.setSubject(subject);
		email.setBody(text);
		queuedEmailDao.saveAndFlush(email);
		return email;
	}

}
