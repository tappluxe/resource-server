package com.aws.tappluxe.services.impl;

import java.util.*;

import javax.transaction.Transactional;

import com.aws.tappluxe.data.dao.UserUnitSocketDao;
import com.aws.tappluxe.data.dto.requests.ScheduleUpdateRequest;
import com.aws.tappluxe.exceptions.InvalidFormException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aws.tappluxe.data.dao.ScheduleDao;
import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dto.requests.ScheduleCreateRequest;
import com.aws.tappluxe.data.dto.responses.GeneralResponse;
import com.aws.tappluxe.data.dto.responses.ScheduleDetailsResponse;
import com.aws.tappluxe.data.entities.QSchedule;
import com.aws.tappluxe.data.entities.QUser;
import com.aws.tappluxe.data.entities.Schedule;
import com.aws.tappluxe.services.ScheduleService;
import com.aws.tappluxe.services.utils.EntityDtoConverter;
import com.querydsl.core.BooleanBuilder;
import org.springframework.validation.BindingResult;

@Service
@Transactional
public class ScheduleServiceImpl implements ScheduleService {
	
	private static final String FOR_SCHEDULES = "Schedule requests.";

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserUnitSocketDao userUnitSocketDao;
	
	@Autowired
	private ScheduleDao scheduleDao;

	@Autowired
	private MessageSource msgSource;

	private QUser userPredicate;

	private QSchedule schedulePredicate;

	@Autowired
	public ScheduleServiceImpl() {
		this.userPredicate = QUser.user;
		this.schedulePredicate = QSchedule.schedule;
	}

	@Override
	public ResponseEntity<?> all(String email) {
		List<ScheduleDetailsResponse> schedules = new ArrayList<>();
		scheduleDao.findAll(schedulePredicate.user.email.eq(email))
			.forEach(schedule -> {
				schedules.add(EntityDtoConverter.toDto(schedule));
			});
		return ResponseEntity.ok(schedules);
	}

	@Override
	public ResponseEntity<?> create(ScheduleCreateRequest scheduleCreate, String email, BindingResult result) {
		GeneralResponse response = new GeneralResponse(FOR_SCHEDULES);
		if (!result.hasErrors()) {
			Schedule schedule = new Schedule();
			schedule.setName(scheduleCreate.getName());
			schedule.setTime(scheduleCreate.getTime());
			schedule.setTimeBeforeNotification(scheduleCreate.getTimeBeforeNotification());
			schedule.setRepeat(scheduleCreate.getRepeat());
			schedule.setUser(userDao.findOne(userPredicate.email.eq(email)));
			schedule.setActive(true);
			scheduleDao.saveAndFlush(schedule);
			response.setMessage(msgSource.getMessage("Success.schedule.create", null, LocaleContextHolder.getLocale()));
			return ResponseEntity.ok(response);
		}
		throw new InvalidFormException(result);
	}

	@Override
	public ResponseEntity<?> show(Long id, String email) {
		GeneralResponse response = new GeneralResponse(FOR_SCHEDULES);
		BooleanBuilder booleanPredicate = new BooleanBuilder(schedulePredicate.id.eq(id));
		booleanPredicate.and(schedulePredicate.user.email.eq(email));
		if (scheduleDao.exists(booleanPredicate)) {
			return ResponseEntity.ok(EntityDtoConverter.toDto(scheduleDao.findOne(booleanPredicate)));
		}
		response.setMessage(msgSource.getMessage("NotExist.schedule",
				new Object[] {id},
				LocaleContextHolder.getLocale()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

	@Override
	public ResponseEntity<?> update(Long id, String email, ScheduleUpdateRequest scheduleUpdate) {
		GeneralResponse response = new GeneralResponse(FOR_SCHEDULES);
		BooleanBuilder booleanPredicate = new BooleanBuilder(schedulePredicate.id.eq(id));
		booleanPredicate.and(schedulePredicate.user.email.eq(email));
		if (scheduleDao.exists(booleanPredicate)) {
			Schedule schedule = scheduleDao.findOne(booleanPredicate);
			if (StringUtils.isNotEmpty(scheduleUpdate.getName())) {
				schedule.setName(scheduleUpdate.getName());
			}
			if (StringUtils.isNotEmpty(scheduleUpdate.getRepeat())) {
				schedule.setRepeat(scheduleUpdate.getRepeat());
			}
			if (scheduleUpdate.getTime() != null) {
				schedule.setTime(scheduleUpdate.getTime());
			}
			if (scheduleUpdate.getTimeBeforeNotification() != null) {
				schedule.setTimeBeforeNotification(scheduleUpdate.getTimeBeforeNotification());
			}
			if (scheduleUpdate.getActive() != null) {
				schedule.setActive(scheduleUpdate.getActive());
			}
			scheduleDao.saveAndFlush(schedule);
			response.setMessage(msgSource.getMessage("Success.schedule.update", null, LocaleContextHolder.getLocale()));
			return ResponseEntity.ok(response);
		}
		response.setMessage(msgSource.getMessage("NotExist.schedule",
				new Object[] {id},
				LocaleContextHolder.getLocale()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

	@Override
	public ResponseEntity<?> delete(Long id, String email) {
		GeneralResponse response = new GeneralResponse(FOR_SCHEDULES);
		BooleanBuilder booleanPredicate = new BooleanBuilder(schedulePredicate.id.eq(id));
		booleanPredicate.and(schedulePredicate.user.email.eq(email));
		Schedule schedule = scheduleDao.findOne(booleanPredicate);
		if (schedule != null) {
			if (schedule.getUserUnitSockets() != null) {
				schedule.getUserUnitSockets().forEach(unitSocket -> {
					unitSocket.setSchedule(null);
					userUnitSocketDao.save(unitSocket);
				});
			}
			scheduleDao.delete(id);
			response.setMessage(msgSource.getMessage("Success.schedule.delete", null, LocaleContextHolder.getLocale()));
			return ResponseEntity.ok(response);
		}
		response.setMessage(msgSource.getMessage("NotExist.schedule",
				new Object[] {id},
				LocaleContextHolder.getLocale()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

}
