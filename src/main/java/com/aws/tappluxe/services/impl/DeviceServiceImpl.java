package com.aws.tappluxe.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.aws.tappluxe.data.entities.*;
import com.aws.tappluxe.exceptions.InvalidFormException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aws.tappluxe.data.dao.DeviceDao;
import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dao.UserUnitSocketDao;
import com.aws.tappluxe.data.dto.requests.DeviceCreateRequest;
import com.aws.tappluxe.data.dto.requests.DeviceUpdateRequest;
import com.aws.tappluxe.data.dto.responses.DeviceDetailsResponse;
import com.aws.tappluxe.data.dto.responses.GeneralResponse;
import com.aws.tappluxe.services.DeviceService;
import com.aws.tappluxe.services.utils.EntityDtoConverter;
import com.querydsl.core.BooleanBuilder;
import org.springframework.validation.BindingResult;

@Service
@Transactional
public class DeviceServiceImpl implements DeviceService {

	private static final String FOR_DEVICES = "Device Requests.";

	@Autowired
	private UserUnitSocketDao userUnitSocketDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private DeviceDao deviceDao;
	
	@Autowired
	private MessageSource msgSource;

	private QDevice devicePredicate;

	private QUserUnitSocket userUnitSocketPredicate;

	private QUser userPredicate;

	@Autowired
	public DeviceServiceImpl() {
		this.devicePredicate = QDevice.device;
		this.userUnitSocketPredicate = QUserUnitSocket.userUnitSocket;
		this.userPredicate = QUser.user;
	}

	@Override
	public ResponseEntity<?> all(Boolean attachedTo, String email) {
		List<DeviceDetailsResponse> devices = new ArrayList<>();
		BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.user.email.eq(email));
		if (attachedTo != null) {
			if (attachedTo) {
				booleanPredicate.and(devicePredicate.userUnitSocket.isNotNull());
			} else {
				booleanPredicate.and(devicePredicate.userUnitSocket.isNull());
			}
		}
		deviceDao.findAll(booleanPredicate).forEach(device -> {
			devices.add(EntityDtoConverter.toDto(device));
		});
		return ResponseEntity.ok(devices);
	}

	@Override
	public ResponseEntity<?> create(DeviceCreateRequest deviceCreate, String email, BindingResult result) {
		GeneralResponse response = new GeneralResponse(FOR_DEVICES);
		UserUnitSocket unitSocket = null;
		if (deviceDao.exists(devicePredicate.name.eq(deviceCreate.getName()))) {
			result.rejectValue("name", "Conflict.device.name",
					new Object[] {deviceCreate.getName()},
					null);
		}
		if (deviceCreate.getUserUnitSocketId() != null) {
			unitSocket = userUnitSocketDao.findOne(deviceCreate.getUserUnitSocketId());
			if (unitSocket == null) {
				result.rejectValue("userUnitSocketId", "NotExist.userUnitSocket",
						new Object[] {deviceCreate.getUserUnitSocketId()},
						null);
			} else {
				if (unitSocket.getDevice() != null) {
					result.rejectValue("userUnitSocketId", "Conflict.device.socket",
							new Object[] {deviceCreate.getUserUnitSocketId()},
							null);
				}
			}
		}
		if (!result.hasErrors()) {
			Device device = new Device();
			device.setName(deviceCreate.getName());
			device.setUser(userDao.findOne(userPredicate.email.eq(email)));
			device.setUserUnitSocket(unitSocket);
			deviceDao.saveAndFlush(device);
			response.setMessage(msgSource.getMessage("Success.device.create",
					null,
					LocaleContextHolder.getLocale()));
			return ResponseEntity.ok(response);
		}
		throw new InvalidFormException(result);
	}

	@Override
	public ResponseEntity<?> show(Long id, String email) {
		GeneralResponse response = new GeneralResponse(FOR_DEVICES);
		BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.id.eq(id));
		booleanPredicate.and(devicePredicate.user.email.eq(email));
		Device device = deviceDao.findOne(booleanPredicate);
		if (device != null) {
			return ResponseEntity.ok(EntityDtoConverter.toDto(device));
		}
		response.setMessage(msgSource.getMessage("NotExist.device",
				new Object[] {id},
				LocaleContextHolder.getLocale()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

	@Override
	public ResponseEntity<?> update(Long id, String email, DeviceUpdateRequest updateRequest, BindingResult result) {
		GeneralResponse response = new GeneralResponse(FOR_DEVICES);
		HttpStatus httpStatus = HttpStatus.NOT_FOUND;
		BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.id.eq(id));
		booleanPredicate.and(devicePredicate.user.email.eq(email));
		Device device = deviceDao.findOne(booleanPredicate);
		if (device != null) {
			if (StringUtils.isNotEmpty(updateRequest.getName())) {
				if (device.getName().equals(updateRequest.getName())) {
					result.rejectValue("name", "Equal.device.name",
							new Object[] {updateRequest.getName()},
							null);
				} else if (deviceDao.exists(devicePredicate.name.eq(updateRequest.getName())
						.and(devicePredicate.ne(device))
						.and(devicePredicate.user.email.eq(email)))) {
					result.rejectValue("name", "Conflict.device.name",
							new Object[] {updateRequest.getName()},
							null);
				} else {
					device.setName(updateRequest.getName());
				}
			}
			if (updateRequest.getUserUnitSocketId() != null) {
				if (updateRequest.getUserUnitSocketId() == -1) {
					device.setUserUnitSocket(null);
				} else {
					UserUnitSocket unitSocket = userUnitSocketDao.findOne(userUnitSocketPredicate.id.eq(updateRequest.getUserUnitSocketId())
							.and(userUnitSocketPredicate.userUnit.user.email.eq(email)));
					if (unitSocket != null) {
						if (unitSocket.getDevice() == null) {
							device.setUserUnitSocket(unitSocket);
						} else {
							result.rejectValue("userUnitSocketId", "Conflict.device.socket",
									new Object[] {updateRequest.getUserUnitSocketId()},
									null);
						}
					} else {
						result.rejectValue("userUnitSocketId", "NotExist.userUnitSocket",
								new Object[] {updateRequest.getUserUnitSocketId()},
								null);
					}
				}
			}
			if (!result.hasErrors()) {
				deviceDao.saveAndFlush(device);
				response.setMessage(msgSource.getMessage("Success.device.update",
						null,
						LocaleContextHolder.getLocale()));
				httpStatus = HttpStatus.OK;
			} else {
				throw new InvalidFormException(result);
			}
		} else {
			response.setMessage(msgSource.getMessage("NotExist.device",
					new Object[] {id},
					LocaleContextHolder.getLocale()));
		}
		return ResponseEntity.status(httpStatus).body(response);
	}

	@Override
	public ResponseEntity<?> delete(Long id, String email) {
		GeneralResponse response = new GeneralResponse(FOR_DEVICES);
		BooleanBuilder booleanPredicate = new BooleanBuilder(devicePredicate.id.eq(id));
		booleanPredicate.and(devicePredicate.user.email.eq(email));
		Device device = deviceDao.findOne(booleanPredicate);
		if (device != null) {
			deviceDao.delete(device);
			response.setMessage(msgSource.getMessage("Success.device.deleted",
					null,
					LocaleContextHolder.getLocale()));
			return ResponseEntity.ok(response);
		}
		response.setMessage(msgSource.getMessage("NotExist.device",
				null,
				LocaleContextHolder.getLocale()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

}
