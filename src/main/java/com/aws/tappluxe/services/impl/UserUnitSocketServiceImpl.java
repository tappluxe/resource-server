package com.aws.tappluxe.services.impl;

import com.aws.tappluxe.data.dao.*;
import com.aws.tappluxe.data.dto.responses.GeneralResponse;
import com.aws.tappluxe.data.entities.*;
import com.aws.tappluxe.services.*;
import com.aws.tappluxe.services.utils.EntityDtoConverter;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

/**
 * Created by franc.margallo on 3/11/2017.
 */
@Service("userUnitSocketService")
@Transactional
public class UserUnitSocketServiceImpl implements UserUnitSocketService {

    private static final String FOR_UNIT_SOCKET = "Unit socket requests.";

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserUnitSocketDao userUnitSocketDao;

    @Autowired
    private UserUnitSocketStatusDao userUnitSocketStatusDao;

    @Autowired
    private ScheduleDao scheduleDao;

    @Autowired
    private SocketStateChangeRequestService socketStateChangeRequestService;

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private MessageSource msgSource;

    private QUser userPredicate;

    private QUserUnit userUnitPredicate;

    private QUserUnitSocket userUnitSocketPredicate;

    @Autowired
    public UserUnitSocketServiceImpl() {
        this.userPredicate = QUser.user;
        this.userUnitPredicate = QUserUnit.userUnit;
        this.userUnitSocketPredicate = QUserUnitSocket.userUnitSocket;
    }

    @Override
    public void create(UserUnit userUnit, Integer numOfSockets) {
        for (int i = 0; i < numOfSockets; i++) {
            UserUnitSocket socket = new UserUnitSocket();
            socket.setSocketNumber(i + 1);
            socket.setUserUnit(userUnit);
            socket = userUnitSocketDao.saveAndFlush(socket);
            pushState(socket, false);
        }
    }

    @Override
    public ResponseEntity<?> show(Long userUnitId, Long socketId, String email) {
        GeneralResponse response = new GeneralResponse(FOR_UNIT_SOCKET);
        BooleanBuilder booleanPredicate = new BooleanBuilder(userUnitSocketPredicate.id.eq(socketId));
        booleanPredicate.and(userUnitPredicate.id.eq(userUnitId));
        booleanPredicate.and(userPredicate.email.eq(email));
        HibernateQuery<UserUnitSocket> socketExistsQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
        UserUnitSocket unitSocket = socketExistsQuery.from(userUnitSocketPredicate)
                .innerJoin(userUnitSocketPredicate.userUnit, userUnitPredicate)
                .innerJoin(userUnitPredicate.user, userPredicate)
                .where(booleanPredicate)
                .groupBy(userUnitSocketPredicate)
                .select(userUnitSocketPredicate)
                .fetchOne();
        if (unitSocket != null) {
            return ResponseEntity.ok(EntityDtoConverter.toDto(unitSocket));
        }
        response.setMessage(msgSource.getMessage("NotExist.userUnit",
                new Object[]{userUnitId},
                LocaleContextHolder.getLocale()));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

    @Override
    public ResponseEntity<?> changeState(Long userUnitId, Long socketId, Boolean state, String email) {
        GeneralResponse response = new GeneralResponse(FOR_UNIT_SOCKET);
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        BooleanBuilder booleanPredicate = new BooleanBuilder(userUnitSocketPredicate.id.eq(socketId));
        booleanPredicate.and(userUnitPredicate.id.eq(userUnitId));
        booleanPredicate.and(userPredicate.email.eq(email));
        HibernateQuery<UserUnitSocket> socketExistsQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
        UserUnitSocket unitSocket = socketExistsQuery.from(userUnitSocketPredicate)
                .innerJoin(userUnitSocketPredicate.userUnit, userUnitPredicate)
                .innerJoin(userUnitPredicate.user, userPredicate)
                .where(booleanPredicate)
                .groupBy(userUnitSocketPredicate)
                .select(userUnitSocketPredicate)
                .fetchOne();
        SocketStateChangeRequest socketStateChangeRequest;
        if (unitSocket != null) {
            if (socketStateChangeRequestService.readyToAccept(unitSocket)) {
                socketStateChangeRequest = socketStateChangeRequestService.create(
                        userDao.findOne(userPredicate.email.eq(email)),
                        unitSocket,
                        state);
                try {
                    byte result = socketStateChangeRequestService.execute(socketStateChangeRequest);
                    if (result == 1) {
                        response.setMessage(msgSource.getMessage("Success.unitSocket.changeState",
                                new Object[]{state ? "on" : "off"},
                                LocaleContextHolder.getLocale()));
                        httpStatus = HttpStatus.OK;
                    } else {
                        socketStateChangeRequestService.timedOut(socketStateChangeRequest.getId());
                        response.setMessage(msgSource.getMessage("Fail.unit.connect",
                                null,
                                LocaleContextHolder.getLocale()));
                        httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
                    }
                } catch (InterruptedException e) {
                    response.setMessage(msgSource.getMessage("Error.internal",
                            null,
                            LocaleContextHolder.getLocale()));
                    httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                }
            } else {
                response.setMessage(msgSource.getMessage("CoolDown.unit.request",
                        null,
                        LocaleContextHolder.getLocale()));
                httpStatus = HttpStatus.TOO_MANY_REQUESTS;
            }
        } else {
            response.setMessage(msgSource.getMessage("NotExist.userUnit",
                    new Object[]{userUnitId},
                    LocaleContextHolder.getLocale()));
        }
        return ResponseEntity.status(httpStatus).body(response);
    }

    @Override
    public ResponseEntity<?> manageSchedule(Long userUnitId, Long userUnitSocketId, Long scheduleId, String email) {
        GeneralResponse response = new GeneralResponse(FOR_UNIT_SOCKET);
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        BooleanBuilder booleanPredicate = new BooleanBuilder(userUnitSocketPredicate.id.eq(userUnitSocketId));
        booleanPredicate.and(userUnitPredicate.id.eq(userUnitId));
        booleanPredicate.and(userPredicate.email.eq(email));
        HibernateQuery<UserUnitSocket> socketExistsQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
        UserUnitSocket unitSocket = socketExistsQuery.from(userUnitSocketPredicate)
                .innerJoin(userUnitSocketPredicate.userUnit, userUnitPredicate)
                .innerJoin(userUnitPredicate.user, userPredicate)
                .where(booleanPredicate)
                .groupBy(userUnitSocketPredicate)
                .select(userUnitSocketPredicate)
                .fetchOne();
        if (unitSocket == null) {
            response.setMessage(msgSource.getMessage("NotExist.userUnit",
                    new Object[] {userUnitId},
                    LocaleContextHolder.getLocale()));
        } else {
            if (scheduleId == -1) {
                unitSocket.setSchedule(null);
                userUnitSocketDao.save(unitSocket);
                response.setMessage(msgSource.getMessage("Success.unitSocket.assignSchedule",
                        null,
                        LocaleContextHolder.getLocale()));
                httpStatus = HttpStatus.OK;
            } else {
                Schedule schedule = scheduleDao.findOne(scheduleId);
                if (schedule != null) {
                    unitSocket.setSchedule(schedule);
                    userUnitSocketDao.save(unitSocket);
                    response.setMessage(msgSource.getMessage("Success.unitSocket.assignSchedule",
                            null,
                            LocaleContextHolder.getLocale()));
                    httpStatus = HttpStatus.OK;
                } else {
                    response.setMessage(msgSource.getMessage("NotExist.schedule",
                            new Object[] {scheduleId},
                            LocaleContextHolder.getLocale()));
                }
            }

        }
        return ResponseEntity.status(httpStatus).body(response);
    }

    @Override
    public UserUnitSocketStatus pushState(UserUnitSocket unitSocket, Boolean state) {
        UserUnitSocketStatus status = new UserUnitSocketStatus();
        status.setUserUnitSocket(unitSocket);
        status.setUpdateDate(new Date());
        status.setState(state);
        return userUnitSocketStatusDao.save(status);
    }

}
