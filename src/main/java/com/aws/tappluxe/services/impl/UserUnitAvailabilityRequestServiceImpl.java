package com.aws.tappluxe.services.impl;

import com.aws.tappluxe.data.dao.UserUnitAvailabilityRequestDao;
import com.aws.tappluxe.data.entities.QUserUnitAvailabilityRequest;
import com.aws.tappluxe.data.entities.UserUnit;
import com.aws.tappluxe.data.entities.UserUnitAvailabilityRequest;
import com.aws.tappluxe.services.UserUnitAvailabilityRequestService;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by franc.margallo on 3/21/2017.
 */
@Service
@Transactional(Transactional.TxType.REQUIRES_NEW)
public class UserUnitAvailabilityRequestServiceImpl implements UserUnitAvailabilityRequestService {

    @Value("${mqtt.coolDown}")
    private Integer coolDown;

    @Autowired
    private UserUnitAvailabilityRequestDao userUnitAvailabilityRequestDao;

    @Autowired
    private SessionFactory sessionFactory;

    private QUserUnitAvailabilityRequest unitAvailabilityRequestPredicate;

    @Autowired
    public UserUnitAvailabilityRequestServiceImpl() {
        this.unitAvailabilityRequestPredicate = QUserUnitAvailabilityRequest.userUnitAvailabilityRequest;
    }

    @Override
    public UserUnitAvailabilityRequest create(UserUnit userUnit) {
        UserUnitAvailabilityRequest request = new UserUnitAvailabilityRequest();
        request.setUserUnit(userUnit);
        request.setRequestTime(new Date());
        request = userUnitAvailabilityRequestDao.save(request);
        return request;
    }

    @Override
    public UserUnitAvailabilityRequest show(Long requestId) {
        return userUnitAvailabilityRequestDao.findOne(requestId);
    }

    @Override
    public boolean readyToAccept(UserUnit userUnit) {
        HibernateQuery<UserUnitAvailabilityRequest> hibernateQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
        UserUnitAvailabilityRequest lastRequest = hibernateQuery.from(unitAvailabilityRequestPredicate)
                .where(unitAvailabilityRequestPredicate.userUnit.eq(userUnit))
                .orderBy(unitAvailabilityRequestPredicate.requestTime.desc())
                .fetchFirst();
        if (lastRequest != null) {
            LocalDateTime requestTime = LocalDateTime.ofInstant(lastRequest.getRequestTime().toInstant(), ZoneId.systemDefault());
            return Duration.between(requestTime, LocalDateTime.now()).getSeconds() > coolDown;
        }
        return true;
    }

}
