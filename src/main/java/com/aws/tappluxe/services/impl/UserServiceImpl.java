package com.aws.tappluxe.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dto.requests.UserUpdateRequest;
import com.aws.tappluxe.data.dto.responses.GeneralResponse;
import com.aws.tappluxe.data.dto.responses.UserDetailsResponse;
import com.aws.tappluxe.data.entities.QUser;
import com.aws.tappluxe.data.entities.User;
import com.aws.tappluxe.exceptions.InvalidFormException;
import com.aws.tappluxe.services.UserService;
import com.aws.tappluxe.services.utils.EntityDtoConverter;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	private static final String FOR_USER = "User Requests.";
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private MessageSource msgSource;

	private QUser userPredicate;

	public UserServiceImpl() {
		this.userPredicate = QUser.user;
	}

	@Override
	public ResponseEntity<?> all() {	
		List<UserDetailsResponse> body = new ArrayList<>();
		userDao.findAll().forEach(user -> {
			body.add(EntityDtoConverter.toDto(user));
		});
		return ResponseEntity.ok(body);
	}

	@Override
	public ResponseEntity<?> show(String email) {
		GeneralResponse response = new GeneralResponse(FOR_USER);
		if (userDao.exists(userPredicate.email.eq(email))) {
			return ResponseEntity.ok(EntityDtoConverter.toDto(userDao.findOne(userPredicate.email.eq(email))));
		}
		response.setMessage(msgSource.getMessage("NotExist.user",
				new Object[] {email},
				LocaleContextHolder.getLocale()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

	@Override
	public ResponseEntity<?> update(String email, UserUpdateRequest userUpdate, BindingResult result) {
		GeneralResponse response = new GeneralResponse(FOR_USER);
		HttpStatus httpStatus = HttpStatus.OK;
		response.setMessage("Update successful.");
		if (userDao.exists(userPredicate.email.eq(email))) {
			User user = userDao.findOne(userPredicate.email.eq(email));
			if (StringUtils.isNotEmpty(userUpdate.getEmail())) {
				if (userDao.exists(userPredicate.email.eq(userUpdate.getEmail()))) {
					result.rejectValue("email", "Conflict.user.email");
				} else if (userUpdate.getConfirmPassword() == null) {
					result.rejectValue("confirmPassword", "NotEqual.user.confirmPassword");
				} else {
					if (!encoder.matches(userUpdate.getConfirmPassword(), user.getPassword())) {
						result.rejectValue("confirmPassword", "NotEqual.user.confirmPassword");
					} else {
						user.setEmail(userUpdate.getEmail());
					}
				}
			}
			if (StringUtils.isNotEmpty(userUpdate.getOldPassword())) {
				if (!encoder.matches(userUpdate.getOldPassword(), user.getPassword())) {
					result.rejectValue("oldPassword", "NotEqual.user.oldPassword");
				} else {
					user.setPassword(encoder.encode(userUpdate.getNewPassword()));
				}
			}
			if (StringUtils.isNotEmpty(userUpdate.getFirstName())) {
				user.setFirstName(userUpdate.getFirstName());
			}
			if (StringUtils.isNotEmpty(userUpdate.getLastName())) {
				user.setLastName(userUpdate.getLastName());
			}
			if (StringUtils.isNotEmpty(userUpdate.getPhone())) {
				user.setPhone(userUpdate.getPhone());
			}
			if (!result.hasErrors()) {
				response.setMessage(msgSource.getMessage("Success.user.update",
						new Object[] {email},
						LocaleContextHolder.getLocale()));
				userDao.saveAndFlush(user);
			} else {
				throw new InvalidFormException(result);
			}
		} else {
			response.setMessage(msgSource.getMessage("NotExist.user",
					new Object[] {email},
					LocaleContextHolder.getLocale()));
			httpStatus = HttpStatus.NOT_FOUND;
		}
		return ResponseEntity.status(httpStatus).body(response);
	}

	@Override
	public ResponseEntity<?> delete(String email) {
		GeneralResponse response = new GeneralResponse(FOR_USER);
		if (userDao.exists(userPredicate.email.eq(email))) {
			userDao.delete(userDao.findOne(userPredicate.email.eq(email)));
			response.setMessage(msgSource.getMessage("Success.user.delete",
					new Object[] {email},
					LocaleContextHolder.getLocale()));
			return ResponseEntity.ok(response);
		}
		response.setMessage(msgSource.getMessage("NotExist.user",
				new Object[] {email},
				LocaleContextHolder.getLocale()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

}
