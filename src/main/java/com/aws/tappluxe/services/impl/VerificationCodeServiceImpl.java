package com.aws.tappluxe.services.impl;

import java.util.Random;

import javax.transaction.Transactional;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dao.VerificationCodeDao;
import com.aws.tappluxe.data.dto.responses.GeneralResponse;
import com.aws.tappluxe.data.entities.QUser;
import com.aws.tappluxe.data.entities.QVerificationCode;
import com.aws.tappluxe.data.entities.User;
import com.aws.tappluxe.data.entities.VerificationCode;
import com.aws.tappluxe.services.EmailService;
import com.aws.tappluxe.services.SMSMessageService;
import com.aws.tappluxe.services.VerificationCodeService;
import com.querydsl.core.types.Predicate;

@Service
@Transactional
public class VerificationCodeServiceImpl implements VerificationCodeService {
	
	private static final String FOR_VERIFICATION_CODE = "Verification Code Requests.";
	
	@Autowired
	public VerificationCodeDao verificationCodeDao;
	
	@Autowired
	public EmailService emailService;
	
	@Autowired
	public UserDao userDao;
	
	@Autowired
	public SMSMessageService smsMessageService;

	private	QUser userPredicate;

	private QVerificationCode verificationCodePredicate;

	private Random rand;

	@Autowired
	public VerificationCodeServiceImpl() {
		this.userPredicate = QUser.user;
		this.verificationCodePredicate = QVerificationCode.verificationCode;
		this.rand = new Random();
	}

	@Override
	public VerificationCode generateVerificationCode(String email) {
		if (userDao.exists(userPredicate.email.eq(email))) {
			User user = userDao.findOne(userPredicate.email.eq(email));
			Predicate predicate = verificationCodePredicate.user.email.eq(email).and(verificationCodePredicate.isValid.eq(true));
			if (verificationCodeDao.exists(predicate)){
				VerificationCode verificationCode = verificationCodeDao.findOne(predicate);
				verificationCode.setValid(false);
				verificationCodeDao.saveAndFlush(verificationCode);
			}
			String code = String.format("%04d", rand.nextInt(10000));
			VerificationCode newCode = new VerificationCode(user, code);
			newCode.setValid(true);
			verificationCodeDao.saveAndFlush(newCode);
			return newCode;
		}
		return null;
	}

	@Override
	public ResponseEntity<?> checkVerificationCode(String code, String email) {
		GeneralResponse response = new GeneralResponse(FOR_VERIFICATION_CODE);
		HttpStatus status;
		Predicate userEmailPredicate = userPredicate.email.eq(email); 
		if (userDao.exists(userEmailPredicate)) {
			User user = userDao.findOne(userEmailPredicate);
			Predicate predicate = verificationCodePredicate.user.eq(user)
					.and(verificationCodePredicate.isValid.eq(true))
					.and(verificationCodePredicate.code.eq(code));
			if (!verificationCodeDao.exists(predicate)){
				response.setMessage("Verification code entered is incorrect.");
				status = HttpStatus.PRECONDITION_FAILED;
			} else {
				VerificationCode verificationCode = verificationCodeDao.findOne(predicate);
				user.setVerified(true);
				verificationCode.setValid(false);
				userDao.saveAndFlush(user);
				verificationCodeDao.saveAndFlush(verificationCode);
				response.setMessage("Verification code entered is correct. User has been verified. ");
				status = HttpStatus.OK;
			}
		} else {
			response.setMessage("User with email " + email + " doesn't exist.");
			status = HttpStatus.PRECONDITION_FAILED;
		}
		
		return ResponseEntity.status(status).body(response);
	}

	@Override
	public ResponseEntity<?> resendCode(String email) {
		VerificationCode code = generateVerificationCode(email);
		GeneralResponse response = new GeneralResponse(FOR_VERIFICATION_CODE);
		HttpStatus status;
		if (code != null) {
			User user = userDao.findOne(userPredicate.email.eq(email));
			String text = "Tappluxe Verification Code: " + code.getCode();
			text += "\n\n\nCode expires in " + VerificationCode.validity + " hours.";
			emailService.createEmail(email, "Tappluxe - Email Verification", text);
			smsMessageService.createSMSMessage(user.getPhone(), text);
			response.setMessage("New code successfully generated.");
			status = HttpStatus.OK;
		} else {
			response.setMessage("User with email \"" + email+  "\" not found.");
			status = HttpStatus.NOT_FOUND;
		}
		return ResponseEntity.status(status).body(response);
	}

	@Override
	public ResponseEntity<?> isVerified(String email) {
		GeneralResponse response = new GeneralResponse(FOR_VERIFICATION_CODE);
		HttpStatus status;
		Predicate userEmailPredicate = userPredicate.email.eq(email); 
		if (userDao.exists(userEmailPredicate)) {
			User user = userDao.findOne(userEmailPredicate);
			JSONObject obj = new JSONObject();
			obj.put("verified", user.isVerified());
			return ResponseEntity.ok(obj);
		} else {
			response.setMessage("User with email \"" + email+  "\" not found.");
			status = HttpStatus.NOT_FOUND;
		}	
		return ResponseEntity.status(status).body(response);
	}

}
