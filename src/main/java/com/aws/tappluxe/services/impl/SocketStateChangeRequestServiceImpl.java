package com.aws.tappluxe.services.impl;

import com.aws.tappluxe.data.dao.SocketStateChangeRequestDao;
import com.aws.tappluxe.data.entities.QSocketStateChangeRequest;
import com.aws.tappluxe.data.entities.SocketStateChangeRequest;
import com.aws.tappluxe.data.entities.User;
import com.aws.tappluxe.data.entities.UserUnitSocket;
import com.aws.tappluxe.services.MqttService;
import com.aws.tappluxe.services.SocketStateChangeRequestService;
import com.aws.tappluxe.services.UserUnitSocketService;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by franc.margallo on 3/18/2017.
 */
@Service
@Transactional(Transactional.TxType.REQUIRES_NEW)
public class SocketStateChangeRequestServiceImpl implements SocketStateChangeRequestService {

    @Value("${mqtt.responseTime}")
    private Integer mqttResponseTime;

    @Value("${mqtt.retries}")
    private Integer mqttRetries;

    @Value("${mqtt.coolDown}")
    private Integer coolDown;

    @Autowired
    private SocketStateChangeRequestDao socketStateChangeRequestDao;

    @Autowired
    private UserUnitSocketService userUnitSocketService;

    @Autowired
    private SocketStateChangeRequestService socketStateChangeRequestService;

    @Autowired
    private MqttService mqttService;

    @Autowired
    private SessionFactory sessionFactory;

    private QSocketStateChangeRequest stateChangeRequestPredicate;

    @Autowired
    public SocketStateChangeRequestServiceImpl() {
        this.stateChangeRequestPredicate = QSocketStateChangeRequest.socketStateChangeRequest;
    }

    @Override
    public SocketStateChangeRequest create(User user, UserUnitSocket unitSocket, Boolean state) {
        SocketStateChangeRequest socketStateChangeRequest = new SocketStateChangeRequest();
        socketStateChangeRequest.setDateRequested(new Date());
        socketStateChangeRequest.setUser(user);
        socketStateChangeRequest.setUserUnitSocket(unitSocket);
        socketStateChangeRequest.setState(state);
        return socketStateChangeRequestDao.save(socketStateChangeRequest);
    }

    @Override
    public SocketStateChangeRequest show(Long requestId, Boolean timedOut) {
        BooleanBuilder booleanPredicate = new BooleanBuilder(stateChangeRequestPredicate.id.eq(requestId));
        booleanPredicate.and(stateChangeRequestPredicate.timedOut.eq(timedOut));
        return socketStateChangeRequestDao.findOne(requestId);
    }

    @Override
    public byte execute(SocketStateChangeRequest request) throws InterruptedException {
        byte status = 0;
        mqttService.sendChangeSocketMessage(request);
        int triesCtr = 0;
        do {
            Thread.sleep(mqttResponseTime);
            request = socketStateChangeRequestService.show(request.getId(), false);
            triesCtr++;
        } while (triesCtr < mqttRetries && !request.getAcknowledged());
        if (request.getAcknowledged()) {
            userUnitSocketService.pushState(request.getUserUnitSocket(), request.getState());
            status = 1;
        }
        return status;
    }

    @Override
    public boolean readyToAccept(UserUnitSocket unitSocket) {
        HibernateQuery<SocketStateChangeRequest> hibernateQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
        SocketStateChangeRequest lastRequest = hibernateQuery.from(stateChangeRequestPredicate)
                .where(stateChangeRequestPredicate.userUnitSocket.eq(unitSocket))
                .orderBy(stateChangeRequestPredicate.dateRequested.desc())
                .fetchFirst();
        if (lastRequest != null) {
            LocalDateTime requestTime = LocalDateTime.ofInstant(lastRequest.getDateRequested().toInstant(), ZoneId.systemDefault());
            return Duration.between(requestTime, LocalDateTime.now()).getSeconds() > coolDown;
        }
        return true;
    }

    @Override
    public void timedOut(Long requestId) {
        SocketStateChangeRequest request = socketStateChangeRequestDao.findOne(requestId);
        request.setTimedOut(true);
        socketStateChangeRequestDao.save(request);
    }

}
