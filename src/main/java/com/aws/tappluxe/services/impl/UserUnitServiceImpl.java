package com.aws.tappluxe.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.aws.tappluxe.data.entities.*;
import com.aws.tappluxe.exceptions.InvalidFormException;
import com.aws.tappluxe.services.MqttService;
import com.aws.tappluxe.services.UserUnitAvailabilityRequestService;
import com.aws.tappluxe.services.UserUnitSocketService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aws.tappluxe.data.dao.ManufacturedUnitDao;
import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dao.UserUnitDao;
import com.aws.tappluxe.data.dto.requests.UserUnitCreateRequest;
import com.aws.tappluxe.data.dto.requests.UserUnitUpdateRequest;
import com.aws.tappluxe.data.dto.responses.GeneralResponse;
import com.aws.tappluxe.data.dto.responses.UserUnitDetailsResponse;
import com.aws.tappluxe.data.entities.ManufacturedUnit;
import com.aws.tappluxe.data.entities.QManufacturedUnit;
import com.aws.tappluxe.data.entities.QUser;
import com.aws.tappluxe.data.entities.QUserUnit;
import com.aws.tappluxe.data.entities.UserUnit;
import com.aws.tappluxe.services.UserUnitService;
import com.aws.tappluxe.services.utils.EntityDtoConverter;
import com.querydsl.core.BooleanBuilder;
import org.springframework.validation.BindingResult;

@Service
@Transactional
public class UserUnitServiceImpl implements UserUnitService {

    private static final String FOR_USER_UNIT = "User Unit Requests.";

    @Value("${mqtt.responseTime}")
    private Integer mqttResponseTime;

    @Value("${mqtt.retries}")
    private Integer mqttRetries;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserUnitDao userUnitDao;

    @Autowired
    private ManufacturedUnitDao manufacturedUnitDao;

    @Autowired
    private UserUnitSocketService userUnitSocketService;

    @Autowired
    private UserUnitAvailabilityRequestService userUnitAvailabilityRequestService;

    @Autowired
    private MqttService mqttService;

    @Autowired
    private MessageSource msgSource;

    private QUser userPredicate;

    private QUserUnit userUnitPredicate;

    @Autowired
    public UserUnitServiceImpl() {
        this.userPredicate = QUser.user;
        this.userUnitPredicate = QUserUnit.userUnit;
    }

    @Override
    public ResponseEntity<?> all(String email) {
        List<UserUnitDetailsResponse> response = new ArrayList<>();
        userUnitDao.findAll(userUnitPredicate.user.email.eq(email))
                .forEach(userUnit -> {
                    response.add(EntityDtoConverter.toDto(userUnit));
                });
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<?> create(UserUnitCreateRequest userUnitCreate, String email, BindingResult result) {
        String productKey = userUnitCreate.getProductKey();
        QManufacturedUnit manufacturedUnitPredicate = QManufacturedUnit.manufacturedUnit; 
        BooleanBuilder booleanPredicateConflict = new BooleanBuilder(userUnitPredicate.name.eq(userUnitCreate.getName()));
        booleanPredicateConflict.and(userUnitPredicate.user.email.eq(email));
        if (!manufacturedUnitDao.exists(manufacturedUnitPredicate.productKey.eq(productKey))) {
            result.rejectValue("productId", "NotExist.manufacturedUnit",
                    new Object[]{productKey},
                    null);
        }
        if (userUnitDao.exists(userUnitPredicate.manufacturedUnit.productKey.eq(productKey))) {
            result.rejectValue("productId", "Conflict.userUnit.productId",
                    null,
                    null);
        }
        if (userUnitDao.exists(booleanPredicateConflict)) {
            result.rejectValue("name", "Conflict.userUnit.name",
                    new Object[]{userUnitCreate.getName()},
                    null);
        }
        if (!result.hasErrors()) {
            UserUnit userUnit = new UserUnit();
            ManufacturedUnit manufacturedUnit = manufacturedUnitDao.findOne(manufacturedUnitPredicate.productKey.eq(productKey));
            userUnit.setName(userUnitCreate.getName());
            userUnit.setRegistrationDate(new Date());
            userUnit.setManufacturedUnit(manufacturedUnit);
            userUnit.setUser(userDao.findOne(userPredicate.email.eq(email)));
            userUnit = userUnitDao.saveAndFlush(userUnit);
            userUnitSocketService.create(userUnit, manufacturedUnit.getProductLine().getNumOfSockets());
            GeneralResponse response = new GeneralResponse(FOR_USER_UNIT);
            response.setMessage(msgSource.getMessage("Success.userUnit.create", null, LocaleContextHolder.getLocale()));
            return ResponseEntity.ok(response);
        }
        throw new InvalidFormException(result);
    }

    @Override
    public ResponseEntity<?> show(Long id, String email) {
        BooleanBuilder booleanPredicate = new BooleanBuilder(userUnitPredicate.id.eq(id));
        booleanPredicate.and(userUnitPredicate.user.email.eq(email));
        if (userUnitDao.exists(booleanPredicate)) {
            UserUnit userUnit = userUnitDao.findOne(booleanPredicate);
            return ResponseEntity.ok(EntityDtoConverter.toDto(userUnit));
        }
        GeneralResponse response = new GeneralResponse(FOR_USER_UNIT);
        response.setMessage(msgSource.getMessage("NotExist.userUnit", new Object[]{id}, LocaleContextHolder.getLocale()));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

    @Override
    public ResponseEntity<?> update(Long id, String email, UserUnitUpdateRequest userUnitUpdate, BindingResult result) {
        GeneralResponse response = new GeneralResponse(FOR_USER_UNIT);
        BooleanBuilder booleanPredicateExists = new BooleanBuilder(userUnitPredicate.id.eq(id));
        booleanPredicateExists.and(userUnitPredicate.user.email.eq(email));
        if (userUnitDao.exists(booleanPredicateExists)) {
            if (StringUtils.isNotEmpty(userUnitUpdate.getName())) {
                BooleanBuilder booleanPredicateConflict = new BooleanBuilder(userUnitPredicate.name.eq(userUnitUpdate.getName()))
                        .and(userUnitPredicate.user.email.eq(email));
                if (StringUtils.isNotEmpty(userUnitUpdate.getName()) && userUnitDao.exists(booleanPredicateConflict)) {
                    result.rejectValue("name", "Conflict.userUnit.name",
                            new Object[]{userUnitUpdate.getName()},
                            null);
                }
            }
            if (!result.hasErrors()) {
                UserUnit userUnit = userUnitDao.findOne(id);
                if (StringUtils.isNotEmpty(userUnitUpdate.getName())) {
                    userUnit.setName(userUnitUpdate.getName());
                }
                if (StringUtils.isNotEmpty(userUnitUpdate.getSsid())) {
                    userUnit.setSsid(userUnitUpdate.getSsid());
                }
                userUnitDao.saveAndFlush(userUnit);
                response.setMessage(msgSource.getMessage("Success.userUnit.update", null, LocaleContextHolder.getLocale()));
                return ResponseEntity.ok(response);
            }
            throw new InvalidFormException(result);
        }
        response.setMessage(msgSource.getMessage("NotExist.userUnit", new Object[]{id}, LocaleContextHolder.getLocale()));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

    @Override
    public ResponseEntity<?> delete(Long id, String email) {
        GeneralResponse response = new GeneralResponse(FOR_USER_UNIT);
        BooleanBuilder booleanPredicate = new BooleanBuilder(userUnitPredicate.id.eq(id));
        booleanPredicate.and(userUnitPredicate.user.email.eq(email));
        if (userUnitDao.exists(booleanPredicate)) {
            userUnitDao.delete(id);
            response.setMessage(msgSource.getMessage("Success.userUnit.deleted", null, LocaleContextHolder.getLocale()));
            return ResponseEntity.ok(response);
        }
        response.setMessage(msgSource.getMessage("NotExist.userUnit", new Object[]{id}, LocaleContextHolder.getLocale()));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

    @Override
    public ResponseEntity<?> availability(Long id, String email) {
        GeneralResponse response = new GeneralResponse(FOR_USER_UNIT);
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        BooleanBuilder booleanPredicate = new BooleanBuilder(userUnitPredicate.id.eq(id));
        booleanPredicate.and(userUnitPredicate.user.email.eq(email));
        UserUnit userUnit = userUnitDao.findOne(booleanPredicate);
        if (userUnit != null) {
            if (userUnitAvailabilityRequestService.readyToAccept(userUnit)) {
                UserUnitAvailabilityRequest request = userUnitAvailabilityRequestService.create(userUnit);
                mqttService.sendUnitAvailabilityStateMessage(request);
                boolean internalError = false;
                int triesCtr = 0;
                do {
                    try {
                        Thread.sleep(mqttResponseTime);
                    } catch (InterruptedException e) {
                        internalError = true;
                    }
                    request = userUnitAvailabilityRequestService.show(request.getId());
                    triesCtr++;
                } while (triesCtr < mqttRetries && !request.getAcknowledged());
                if (request.getAcknowledged()) {
                    response.setMessage(msgSource.getMessage("Success.unit.connect",
                            null,
                            LocaleContextHolder.getLocale()));
                    httpStatus = HttpStatus.OK;
                } else if (internalError) {
                    response.setMessage(msgSource.getMessage("Error.internal",
                            null,
                            LocaleContextHolder.getLocale()));
                    httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                } else {
                    response.setMessage(msgSource.getMessage("Fail.unit.connect",
                            null,
                            LocaleContextHolder.getLocale()));
                    httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
                }
            } else {
                response.setMessage(msgSource.getMessage("CoolDown.unit.request",
                        null,
                        LocaleContextHolder.getLocale()));
                httpStatus = HttpStatus.TOO_MANY_REQUESTS;
            }
        } else {
            response.setMessage(msgSource.getMessage("NotExist.userUnit",
                    new Object[]{id},
                    LocaleContextHolder.getLocale()));
        }
        return ResponseEntity.status(httpStatus).body(response);
    }

}
