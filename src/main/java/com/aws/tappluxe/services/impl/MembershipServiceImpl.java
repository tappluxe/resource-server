package com.aws.tappluxe.services.impl;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.aws.tappluxe.data.dao.ManufacturedUnitDao;
import com.aws.tappluxe.data.dao.UserDao;
import com.aws.tappluxe.data.dao.UserUnitDao;
import com.aws.tappluxe.data.dto.requests.ResetPasswordRequest;
import com.aws.tappluxe.data.dto.requests.UserCreateRequest;
import com.aws.tappluxe.data.dto.responses.GeneralResponse;
import com.aws.tappluxe.data.entities.ManufacturedUnit;
import com.aws.tappluxe.data.entities.QManufacturedUnit;
import com.aws.tappluxe.data.entities.QUser;
import com.aws.tappluxe.data.entities.QUserUnit;
import com.aws.tappluxe.data.entities.User;
import com.aws.tappluxe.data.entities.UserUnit;
import com.aws.tappluxe.data.entities.VerificationCode;
import com.aws.tappluxe.events.OnRegistrationCompleteEvent;
import com.aws.tappluxe.exceptions.InvalidFormException;
import com.aws.tappluxe.services.EmailService;
import com.aws.tappluxe.services.MembershipService;
import com.aws.tappluxe.services.SMSMessageService;
import com.aws.tappluxe.services.UserUnitSocketService;
import com.aws.tappluxe.services.VerificationCodeService;

@Service
@Transactional
public class MembershipServiceImpl implements MembershipService {
	
	private static final String FOR_MEMBERSHIP = "Membership Requests.";
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private ManufacturedUnitDao manufacturedUnitDao;
	
	@Autowired
	private UserUnitDao userUnitDao;

	@Autowired
	private UserUnitSocketService userUnitSocketService;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private SMSMessageService smsMessageService;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private VerificationCodeService verificationCodeService;
	
	@Autowired
	public ApplicationEventPublisher eventPublisher;

	@Autowired
	private MessageSource msgSource;

	private QUserUnit userUnitPredicate;

	private QUser userPredicate;

	private QManufacturedUnit manufacturedUnitPredicate;

	@Autowired
	public MembershipServiceImpl() {
		this.userUnitPredicate = QUserUnit.userUnit;
		this.userPredicate = QUser.user;
		this.manufacturedUnitPredicate = QManufacturedUnit.manufacturedUnit;
	}

	@Override
	public ResponseEntity<?> register(UserCreateRequest userCreate, BindingResult result) {
		GeneralResponse response = new GeneralResponse(FOR_MEMBERSHIP);
		String productKey = userCreate.getUserUnit().getProductKey();
		if (userDao.exists(userPredicate.email.eq(userCreate.getEmail()))) {
			result.rejectValue("email", "Conflict.user.email", null, null);
		}
		if (!manufacturedUnitDao.exists(manufacturedUnitPredicate.productKey.eq(productKey))) {
			result.rejectValue("userUnit.productId", "NotExist.manufacturedUnit",
					new Object[] {userCreate.getUserUnit().getProductKey()},
					null);
		}
		if (userUnitDao.exists(userUnitPredicate.manufacturedUnit.productKey.eq(userCreate.getUserUnit().getProductKey()))) {
			result.rejectValue("userUnit.productId", "Conflict.userUnit.productId",
					null,
					null);
		}
		if (!result.hasErrors()) {
			//create user
			User user = new User();
			user.setEmail(userCreate.getEmail());
			user.setPassword(encoder.encode(userCreate.getPassword()));
			user.setFirstName(userCreate.getFirstName());
			user.setLastName(userCreate.getLastName());
			user.setPhone(userCreate.getPhone());
			user.setRegistrationDate(new Date());
			userDao.saveAndFlush(user);
			//user unit
			UserUnit userUnit = new UserUnit();
			ManufacturedUnit manufacturedUnit = manufacturedUnitDao.findOne((manufacturedUnitPredicate.productKey.eq(productKey)));
			userUnit.setName(userCreate.getUserUnit().getName());
			userUnit.setRegistrationDate(new Date());
			userUnit.setManufacturedUnit(manufacturedUnit);
			userUnit.setUser(user);
			userUnit = userUnitDao.saveAndFlush(userUnit);
			userUnitSocketService.create(userUnit, manufacturedUnit.getProductLine().getNumOfSockets());
			response.setMessage(msgSource.getMessage("Success.membership.create",
					null,
					LocaleContextHolder.getLocale()));
			eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user));
			return ResponseEntity.ok(response);
		}
		throw new InvalidFormException(result);
	}

	@Override
	public ResponseEntity<?> checkEmail(String email) {
		GeneralResponse response = new GeneralResponse(FOR_MEMBERSHIP);
		HttpStatus httpStatus = HttpStatus.OK;
		response.setMessage(msgSource.getMessage("Valid.membership.email",
				null,
				LocaleContextHolder.getLocale()));
		if (userDao.exists(userPredicate.email.eq(email))){
			response.setMessage(msgSource.getMessage("Invalid.membership.email",
					null,
					LocaleContextHolder.getLocale()));
			httpStatus = HttpStatus.NOT_FOUND;
		}
		return ResponseEntity.status(httpStatus).body(response);
	}

	@Override
	public ResponseEntity<?> checkProductId(String productKey) {
		GeneralResponse response = new GeneralResponse(FOR_MEMBERSHIP);
		HttpStatus httpStatus = HttpStatus.OK;
		if (!manufacturedUnitDao.exists((manufacturedUnitPredicate.productKey.eq(productKey)))) {
			response.setMessage(msgSource.getMessage("NotExist.membership.unit",
					null,
					LocaleContextHolder.getLocale()));
			httpStatus = HttpStatus.NOT_FOUND;
		} else if (userUnitDao.exists(userUnitPredicate.manufacturedUnit.productKey.eq(productKey))) {
			response.setMessage(msgSource.getMessage("Conflict.membership.unit",
					null,
					LocaleContextHolder.getLocale()));
			httpStatus = HttpStatus.CONFLICT;
		} else {
			response.setMessage(msgSource.getMessage("Valid.membership.unit",
					null,
					LocaleContextHolder.getLocale()));
		}
		return ResponseEntity.status(httpStatus).body(response);
	}

	@Override
	public ResponseEntity<?> forgotPassword(String email) {
		HttpStatus status;
		GeneralResponse response = new GeneralResponse(FOR_MEMBERSHIP);
		if (userDao.exists(userPredicate.email.eq(email))) {
			User user = userDao.findOne(userPredicate.email.eq(email));
			VerificationCode code = verificationCodeService.generateVerificationCode(email);
			if (code != null) {
				String text = "Pass Code: " + code.getCode();
				text += "\n\n\nCode expires in " + VerificationCode.validity + " hours.";
				emailService.createEmail(user.getEmail(), "Tappluxe - Forgot Password", text);
				smsMessageService.createSMSMessage(user.getPhone(), text);
				response.setMessage("Success generating code.");
				status = HttpStatus.OK;
			} else {
				response.setMessage("Error generating code.");
				status = HttpStatus.PRECONDITION_FAILED;
			}
		} else {
			response.setMessage("User with email: \"" + email + "\" doesn't exist");
			status = HttpStatus.NOT_FOUND;
		}
		return ResponseEntity.status(status).body(response);
	}

	@Override
	public ResponseEntity<?> resetPassword(ResetPasswordRequest request) {
		GeneralResponse response = new GeneralResponse(FOR_MEMBERSHIP);
		String email = request.getEmail();
		HttpStatus status;
		if (userDao.exists(userPredicate.email.eq(email))) {
			User user = userDao.findOne(userPredicate.email.eq(email));
			if (request.getNewPassword().equals(request.getConfirmPassword())) {
				user.setPassword(encoder.encode(request.getNewPassword()));
				userDao.saveAndFlush(user);
				response.setMessage("Password successfully updated.");
				status = HttpStatus.OK;
			} else {
				response.setMessage("Passwords do not match.");
				status = HttpStatus.PRECONDITION_FAILED;
			}
		} else {
			response.setMessage(msgSource.getMessage("NotExist.user",
					new Object[] {email},
					LocaleContextHolder.getLocale()));
			status = HttpStatus.NOT_FOUND;
		}
		return ResponseEntity.status(status).body(response);
	}

}
