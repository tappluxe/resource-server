package com.aws.tappluxe.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.aws.tappluxe.exceptions.InvalidFormException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aws.tappluxe.data.dao.ProductLineDao;
import com.aws.tappluxe.data.dto.requests.ProductLineCreateRequest;
import com.aws.tappluxe.data.dto.requests.ProductLineUpdateRequest;
import com.aws.tappluxe.data.dto.responses.GeneralResponse;
import com.aws.tappluxe.data.dto.responses.ProductLineDetailsResponse;
import com.aws.tappluxe.data.entities.ProductLine;
import com.aws.tappluxe.data.entities.QProductLine;
import com.aws.tappluxe.services.ProductLineService;
import com.aws.tappluxe.services.utils.EntityDtoConverter;
import org.springframework.validation.BindingResult;

@Service
@Transactional
public class ProductLineServiceImpl implements ProductLineService {

	private static final String FOR_PRODUCT_LINES = "Product Line Requests.";
	
	@Autowired
	private ProductLineDao productLineDao;

	@Autowired
	private MessageSource msgSource;

	private QProductLine productLinePredicate;

	@Autowired
	public ProductLineServiceImpl() {
		this.productLinePredicate = QProductLine.productLine;
	}

	@Override
	public ResponseEntity<?> all() {
		List<ProductLineDetailsResponse> response = new ArrayList<>();
		productLineDao.findAll().forEach(productLine -> {
			response.add(EntityDtoConverter.toDto(productLine));
		});
		return ResponseEntity.ok(response);
	}

	@Override
	public ResponseEntity<?> create(ProductLineCreateRequest productLineCreate, BindingResult result) {
		GeneralResponse response = new GeneralResponse(FOR_PRODUCT_LINES);
		if (productLineDao.exists(productLinePredicate.name.eq(productLineCreate.getName()))) {
			response.setMessage(msgSource.getMessage("Conflict.productLine.name", null, LocaleContextHolder.getLocale()));
			return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
		} else {
			if (!result.hasErrors()) {
				ProductLine productLine = new ProductLine();
				productLine.setName(productLineCreate.getName());
				productLine.setNumOfSockets(productLineCreate.getNumOfSockets());
				productLine.setLoadCapacity(productLineCreate.getLoadCapacity());
				productLineDao.saveAndFlush(productLine);
				response.setMessage(msgSource.getMessage("Success.productLine.create", null, LocaleContextHolder.getLocale()));
				return ResponseEntity.ok(response);
			}
			throw new InvalidFormException(result);
		}
	}

	@Override
	public ResponseEntity<?> show(Long id) {
		GeneralResponse response = new GeneralResponse(FOR_PRODUCT_LINES);
		if (productLineDao.exists(id)) {
			return ResponseEntity.ok(EntityDtoConverter.toDto(productLineDao.findOne(id)));
		} else {
			response.setMessage(msgSource.getMessage("NotExist.productLine",
					new Object[] {id},
					LocaleContextHolder.getLocale()));
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
	}

	@Override
	public ResponseEntity<?> update(Long id, ProductLineUpdateRequest productLineUpdate, BindingResult result) {
		GeneralResponse response = new GeneralResponse(FOR_PRODUCT_LINES);
		HttpStatus httpStatus = HttpStatus.OK;
		QProductLine productLinePredicate = QProductLine.productLine;
		if (!productLineDao.exists(id)) {
			response.setMessage(msgSource.getMessage("NotExist.productLine",
					new Object[] {id},
					LocaleContextHolder.getLocale()));
			httpStatus = HttpStatus.NOT_FOUND;
		} else if (StringUtils.isNotEmpty(productLineUpdate.getName()) 
					&& productLineDao.exists(productLinePredicate.name.eq(productLineUpdate.getName()))) {
			response.setMessage(msgSource.getMessage("Conflict.productLine.name", null, LocaleContextHolder.getLocale()));
			httpStatus = HttpStatus.CONFLICT;
		} else {
			if (!result.hasErrors()) {
				ProductLine productLine = productLineDao.findOne(id);
				if (StringUtils.isNotEmpty(productLineUpdate.getName())) {
					productLine.setName(productLineUpdate.getName());
				}
				if (productLineUpdate.getNumOfSockets() != null) {
					productLine.setNumOfSockets(productLineUpdate.getNumOfSockets());
				}
				if (productLineUpdate.getLoadCapacity() != null) {
					productLine.setLoadCapacity(productLineUpdate.getLoadCapacity());
				}
				productLineDao.saveAndFlush(productLine);
				response.setMessage(msgSource.getMessage("Success.productLine.update",
						null,
						LocaleContextHolder.getLocale()));
			} else {
				throw new InvalidFormException(result);
			}
		}
		return ResponseEntity.status(httpStatus).body(response);
	}

	@Override
	public ResponseEntity<?> delete(Long id) {
		GeneralResponse response = new GeneralResponse(FOR_PRODUCT_LINES);
		HttpStatus httpStatus = HttpStatus.OK;
		ProductLine productLine = productLineDao.findOne(id);
		if (productLine == null) {
			response.setMessage(msgSource.getMessage("NotExist.productLine",
					new Object[] {id},
					LocaleContextHolder.getLocale()));
			httpStatus = HttpStatus.NOT_FOUND;
		} else if (productLine.getManufacturedUnits() != null && !productLine.getManufacturedUnits().isEmpty()) {
			response.setMessage(msgSource.getMessage("Fail.productLine.delete",
					null,
					LocaleContextHolder.getLocale()));
			httpStatus = HttpStatus.METHOD_NOT_ALLOWED;
		} else {
			productLineDao.delete(id);
			response.setMessage(msgSource.getMessage("Success.productLine.delete",
					null,
					LocaleContextHolder.getLocale()));
		}
		return ResponseEntity.status(httpStatus).body(response);
	}	
	
}
