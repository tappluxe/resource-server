package com.aws.tappluxe.services.impl;

import javax.transaction.Transactional;

import com.aws.tappluxe.data.dao.*;
import com.aws.tappluxe.data.entities.*;
import com.aws.tappluxe.services.SocketStateChangeRequestService;
import com.aws.tappluxe.services.UserUnitAvailabilityRequestService;
import com.aws.tappluxe.services.UserUnitSocketService;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aws.tappluxe.config.MqttConfig.MqttOutbound;
import com.aws.tappluxe.services.MqttService;

@Service
@Transactional
public class MqttServiceImpl implements MqttService {

    private static final String UNIT_CHANGE_SSID = "ussid";

    private static final String STATE_CHANGE = "sc";

    private static final String STATE_CHANGE_BODY = "scb";

    private static final String UNIT_AVAILABILITY_REQUEST = "uar";

    private static final String MANUAL_STATE_CHANGE = "msc";

    private static final String CURRENT_SOCKET_STATE = "css";

    @Autowired
    private UserUnitDao userUnitDao;

    @Autowired
    private SocketStateChangeRequestDao socketStateChangeRequestDao;

    @Autowired
    private UserUnitAvailabilityRequestDao userUnitAvailabilityRequestDao;

    @Autowired
    private SocketStateChangeRequestService socketStateChangeRequestService;

    @Autowired
    private UserUnitSocketService userUnitSocketService;

    @Autowired
    private UserUnitAvailabilityRequestService userUnitAvailabilityRequestService;

    @Autowired
    private MqttOutbound mqttOutbound;

    @Autowired
    private SessionFactory sessionFactory;

    private QManufacturedUnit manufacturedUnitPredicate;

    private QUserUnit userUnitPredicate;

    private QUserUnitSocket userUnitSocketPredicate;

    @Autowired
    public MqttServiceImpl() {
        this.manufacturedUnitPredicate = QManufacturedUnit.manufacturedUnit;
        this.userUnitPredicate = QUserUnit.userUnit;
        this.userUnitSocketPredicate = QUserUnitSocket.userUnitSocket;
    }

    @Override
    public void sendChangeSocketMessage(SocketStateChangeRequest socketStateChangeRequest) {
        HibernateQuery<ManufacturedUnit> manufacturedUnitQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
        ManufacturedUnit manufacturedUnit = manufacturedUnitQuery
                .from(userUnitSocketPredicate)
                .innerJoin(userUnitSocketPredicate.userUnit, userUnitPredicate)
                .innerJoin(userUnitPredicate.manufacturedUnit, manufacturedUnitPredicate)
                .select(manufacturedUnitPredicate)
                .where(userUnitSocketPredicate.eq(socketStateChangeRequest.getUserUnitSocket()))
                .fetchOne();
        String requestMessage = manufacturedUnit.getId()
                + ":" + STATE_CHANGE
                + ":" + socketStateChangeRequest.getId();
        mqttOutbound.sendToMqtt(requestMessage);
    }

    @Override
    public void sendUnitAvailabilityStateMessage(UserUnitAvailabilityRequest userUnitAvailabilityRequest) {
        HibernateQuery<ManufacturedUnit> manufacturedUnitQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
        ManufacturedUnit manufacturedUnit = manufacturedUnitQuery
                .from(userUnitPredicate)
                .innerJoin(userUnitPredicate.manufacturedUnit, manufacturedUnitPredicate)
                .select(manufacturedUnitPredicate)
                .where(userUnitPredicate.eq(userUnitAvailabilityRequest.getUserUnit()))
                .fetchOne();
        String message = manufacturedUnit.getId() + ":" +
                UNIT_AVAILABILITY_REQUEST + ":" +
                userUnitAvailabilityRequest.getId();
        mqttOutbound.sendToMqtt(message);
    }

    @Override
    public void sendCurrentSocketState(UserUnitSocket unitSocket) {
        HibernateQuery<ManufacturedUnit> manufacturedUnitQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
        ManufacturedUnit manufacturedUnit = manufacturedUnitQuery
                .from(userUnitSocketPredicate)
                .innerJoin(userUnitSocketPredicate.userUnit, userUnitPredicate)
                .innerJoin(userUnitPredicate.manufacturedUnit, manufacturedUnitPredicate)
                .select(manufacturedUnitPredicate)
                .where(userUnitSocketPredicate.eq(unitSocket))
                .fetchOne();
        String message = manufacturedUnit.getId() + ":" +
                CURRENT_SOCKET_STATE + ":" +
                unitSocket.getSocketNumber() + ":" +
                unitSocket.getStatusHistory().get(0);
        mqttOutbound.sendToMqtt(message);
    }

    @Override
    public void processInboundMessage(String message) {
        String[] msgTokens = message.split(":");
        if (msgTokens.length == 3 && msgTokens[0].equals(UNIT_CHANGE_SSID)) {
            HibernateQuery<UserUnit> hibernateQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
            UserUnit userUnit = hibernateQuery.from(userUnitPredicate)
                    .innerJoin(userUnitPredicate.manufacturedUnit, manufacturedUnitPredicate)
                    .where(manufacturedUnitPredicate.id.eq(Long.valueOf(msgTokens[1])))
                    .groupBy(userUnitPredicate)
                    .select(userUnitPredicate)
                    .fetchOne();
            if (userUnit != null) {
                userUnit.setSsid(msgTokens[2]);
                userUnitDao.save(userUnit);
            }
        } else if (msgTokens.length == 2 && msgTokens[0].equals(STATE_CHANGE)) {
            SocketStateChangeRequest stateChangeRequest = socketStateChangeRequestService.show(Long.valueOf(msgTokens[1]), false);
            if (socketStateChangeRequestDao != null) {
                stateChangeRequest.setAcknowledged(true);
                this.socketStateChangeRequestDao.save(stateChangeRequest);
            }
        } else if (msgTokens.length == 2 && msgTokens[0].equals(STATE_CHANGE_BODY)) {
            SocketStateChangeRequest request = socketStateChangeRequestService.show(Long.valueOf(msgTokens[1]), false);
            if (request != null) {
                HibernateQuery<ManufacturedUnit> manufacturedUnitQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
                ManufacturedUnit manufacturedUnit = manufacturedUnitQuery
                        .from(userUnitSocketPredicate)
                        .innerJoin(userUnitSocketPredicate.userUnit, userUnitPredicate)
                        .innerJoin(userUnitPredicate.manufacturedUnit, manufacturedUnitPredicate)
                        .select(manufacturedUnitPredicate)
                        .where(userUnitSocketPredicate.eq(request.getUserUnitSocket()))
                        .fetchOne();
                String requestMessage = manufacturedUnit.getId()
                        + ":" + STATE_CHANGE_BODY
                        + ":" + request.getId()
                        + ":" + request.getUserUnitSocket().getSocketNumber()
                        + ":" + (request.getState() ? "ON" : "OFF");
                mqttOutbound.sendToMqtt(requestMessage);
            }
        } else if (msgTokens.length == 4 && msgTokens[0].equals(MANUAL_STATE_CHANGE)) {
            boolean state = msgTokens[3].equals("ON");
            BooleanBuilder booleanPredicate = new BooleanBuilder(manufacturedUnitPredicate.id.eq(Long.valueOf(msgTokens[1])));
            booleanPredicate.and(userUnitSocketPredicate.socketNumber.eq(Integer.valueOf(msgTokens[2])));
            HibernateQuery<UserUnitSocket> socketQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
            UserUnitSocket unitSocket = socketQuery.from(userUnitSocketPredicate)
                    .innerJoin(userUnitSocketPredicate.userUnit, userUnitPredicate)
                    .innerJoin(userUnitPredicate.manufacturedUnit, manufacturedUnitPredicate)
                    .where(booleanPredicate)
                    .groupBy(userUnitSocketPredicate)
                    .select(userUnitSocketPredicate)
                    .fetchOne();
            if (unitSocket != null) {
                userUnitSocketService.pushState(unitSocket, state);
            }
        } else if (msgTokens.length == 2 && msgTokens[0].equals(UNIT_AVAILABILITY_REQUEST)) {
            UserUnitAvailabilityRequest request = userUnitAvailabilityRequestService.show(Long.valueOf(msgTokens[1]));
            if (request != null) {
                request.setAcknowledged(true);
                userUnitAvailabilityRequestDao.save(request);
            }
        } else if (msgTokens.length == 3 && msgTokens[0].equals(CURRENT_SOCKET_STATE)) {
            BooleanBuilder booleanPredicate = new BooleanBuilder(manufacturedUnitPredicate.id.eq(Long.valueOf(msgTokens[1])));
            booleanPredicate.and(userUnitSocketPredicate.socketNumber.eq(Integer.valueOf(msgTokens[2])));
            HibernateQuery<UserUnitSocket> hibernateQuery = new HibernateQuery<>(sessionFactory.getCurrentSession());
            UserUnitSocket unitSocket = hibernateQuery.from(userUnitSocketPredicate)
                    .innerJoin(userUnitSocketPredicate.userUnit, userUnitPredicate)
                    .innerJoin(userUnitPredicate.manufacturedUnit, manufacturedUnitPredicate)
                    .where(booleanPredicate)
                    .groupBy(userUnitSocketPredicate)
                    .select(userUnitSocketPredicate)
                    .fetchOne();
            if (unitSocket != null) {
                sendCurrentSocketState(unitSocket);
            }
        }
    }

}
