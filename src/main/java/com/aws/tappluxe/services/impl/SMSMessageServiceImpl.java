package com.aws.tappluxe.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.aws.tappluxe.data.dao.SMSMessageDao;
import com.aws.tappluxe.data.entities.SMSMessage;
import com.aws.tappluxe.services.SMSMessageService;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
 
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

@Service
@Transactional
public class SMSMessageServiceImpl implements SMSMessageService {
	
	@Autowired
    private TwilioRestClient client;
	
	@Autowired
	public SMSMessageDao smsMessageDao;
	
	@Value("${twilio.number}")
	private String twilioNumber;

	@Override
	public boolean sendSMSMessage(String toNumber, String text) {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("Body", text));
        params.add(new BasicNameValuePair("To", toNumber));
        params.add(new BasicNameValuePair("From", twilioNumber));
 
        MessageFactory messageFactory = client.getAccount().getMessageFactory();

        try {
			messageFactory.create(params);
	        return true;
		} catch (TwilioRestException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public SMSMessage createSMSMessage(String toNumber, String text) {
		SMSMessage sms = new SMSMessage();
		sms.setText(text);
		sms.setToNumber(toNumber);
		sms.setIsSent(false);
		smsMessageDao.saveAndFlush(sms);
		return sms;
	}

}
