package com.aws.tappluxe.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.aws.tappluxe.services.utils.EntityDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aws.tappluxe.data.dao.ManufacturedUnitDao;
import com.aws.tappluxe.data.dao.ProductLineDao;
import com.aws.tappluxe.data.dto.responses.GeneralResponse;
import com.aws.tappluxe.data.dto.responses.ManufacturedUnitDetailsResponse;
import com.aws.tappluxe.data.entities.ManufacturedUnit;
import com.aws.tappluxe.data.entities.ProductLine;
import com.aws.tappluxe.services.ManufacturedUnitService;
import com.aws.tappluxe.services.utils.QRCodeGenerator;

@Service
@Transactional
public class ManufacturedUnitServiceImpl implements ManufacturedUnitService {
	
	private static final String FOR_MANUFACTURED_UNIT = "Manufactured Unit Requests.";
	
	@Autowired
	private ManufacturedUnitDao manufacturedUnitDao;
	
	@Autowired
	private ProductLineDao productLineDao;

	@Autowired
	private MessageSource msgSource;

	@Override
	public ResponseEntity<?> all() {
		List<ManufacturedUnitDetailsResponse> response = new ArrayList<>();
		manufacturedUnitDao.findAll().forEach(manufacturedUnit -> {
			response.add(EntityDtoConverter.toDto(manufacturedUnit));
		});
		return ResponseEntity.ok(response);
	}

	@Override
	public ResponseEntity<?> create(Long productLineId) {
		GeneralResponse response = new GeneralResponse(FOR_MANUFACTURED_UNIT);
		if (productLineDao.exists(productLineId)) {
			ProductLine productLine = productLineDao.findOne(productLineId);
			ManufacturedUnit manufacturedUnit = new ManufacturedUnit();
			manufacturedUnit.setProductLine(productLine);
			manufacturedUnit.setManufacturedDate(new Date());
			manufacturedUnit = manufacturedUnitDao.saveAndFlush(manufacturedUnit);
			QRCodeGenerator.generate(manufacturedUnit.getProductKey());
			return ResponseEntity.ok(EntityDtoConverter.toDto(manufacturedUnit));
		}
		response.setMessage(msgSource.getMessage("NotExist.productLine",
				new Object[] {productLineId},
				LocaleContextHolder.getLocale()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

	@Override
	public ResponseEntity<?> show(Long id) {
		GeneralResponse response = new GeneralResponse(FOR_MANUFACTURED_UNIT);
		if (manufacturedUnitDao.exists(id)) {
			return ResponseEntity.ok(EntityDtoConverter.toDto(manufacturedUnitDao.findOne(id)));
		}
		response.setMessage(msgSource.getMessage("NotExist.manufacturedUnit",
				new Object[] {id},
				LocaleContextHolder.getLocale()));
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

	@Override
	public ResponseEntity<?> delete(Long id) {
		GeneralResponse response = new GeneralResponse(FOR_MANUFACTURED_UNIT);
		HttpStatus httpStatus = HttpStatus.OK;
		ManufacturedUnit manufacturedUnit = manufacturedUnitDao.findOne(id);
		if (manufacturedUnit == null) {
			response.setMessage(msgSource.getMessage("NotExist.manufacturedUnit",
					null,
					LocaleContextHolder.getLocale()));
			httpStatus = HttpStatus.NOT_FOUND;
		} else if (manufacturedUnit.getUserUnit() != null) {
			response.setMessage(msgSource.getMessage("Fail.manufacturedUnit.delete",
					null,
					LocaleContextHolder.getLocale()));
			httpStatus = HttpStatus.METHOD_NOT_ALLOWED;
		} else {
			manufacturedUnitDao.delete(manufacturedUnit);
			response.setMessage(msgSource.getMessage("success.manufacturedUnit.delete",
					null,
					LocaleContextHolder.getLocale()));
		}
		return ResponseEntity.status(httpStatus).body(response);
	}

}
