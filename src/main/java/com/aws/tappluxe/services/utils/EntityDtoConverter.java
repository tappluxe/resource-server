package com.aws.tappluxe.services.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.aws.tappluxe.data.dto.responses.*;
import com.aws.tappluxe.data.entities.*;

public class EntityDtoConverter {

	public static ProductLineDetailsResponse toDto(ProductLine productLine) {
		ProductLineDetailsResponse dto = new ProductLineDetailsResponse();
		dto.setId(productLine.getId());
		dto.setName(productLine.getName());
		dto.setLoadCapacity(productLine.getLoadCapacity());
		dto.setNumOfSockets(productLine.getNumOfSockets());
		return dto;
	}

	public static ManufacturedUnitDetailsResponse toDto(ManufacturedUnit manufacturedUnit) {
		ManufacturedUnitDetailsResponse dto = new ManufacturedUnitDetailsResponse();
		dto.setProductId(manufacturedUnit.getId());
		dto.setProductKey(manufacturedUnit.getProductKey());
		dto.setManufacturedDate(manufacturedUnit.getManufacturedDate());
		return dto;
	}

	public static UserDetailsResponse toDto(User user) {
		UserDetailsResponse dto = new UserDetailsResponse();
		dto.setId(user.getId());
		dto.setEmail(user.getEmail());
		dto.setLastName(user.getLastName());
		dto.setFirstName(user.getFirstName());
		dto.setPhone(user.getPhone());
		dto.setRegistrationDate(user.getRegistrationDate());
		dto.setVerified(user.isVerified());
		return dto;
	}
	
	public static UserUnitDetailsResponse toDto(UserUnit userUnit) {
		UserUnitDetailsResponse dto = new UserUnitDetailsResponse();
		List<UserUnitSocketDetailsResponse> sockets = new ArrayList<>();
		dto.setId(userUnit.getId());
		dto.setName(userUnit.getName());
		dto.setRegistrationDate(userUnit.getRegistrationDate());
		dto.setSsid(userUnit.getSsid());
		userUnit.getUserUnitSocket().forEach(socket -> {
			sockets.add(toDto(socket));
		});
		dto.setSockets(sockets);
		dto.setUserId(userUnit.getUser().getId());
		dto.setManufacturedUnitDetails(toDto(userUnit.getManufacturedUnit()));
		return dto;
	}
	
	public static UserUnitSocketDetailsResponse toDto(UserUnitSocket userUnitSocket) {
		UserUnitSocketDetailsResponse dto = new UserUnitSocketDetailsResponse();
		List<UserUnitSocketStatusDetailsResponse> statuses = new ArrayList<>();
		dto.setId(userUnitSocket.getId());
		dto.setSocketNumber(userUnitSocket.getSocketNumber());
		dto.setUserUnitId(userUnitSocket.getUserUnit().getId());
		dto.setDeviceId(userUnitSocket.getDevice() != null? userUnitSocket.getDevice().getId() : null);
		userUnitSocket.getStatusHistory().forEach(status -> {
			statuses.add(toDto(status));
		});
		dto.setSchedule(userUnitSocket.getSchedule() != null? toDto(userUnitSocket.getSchedule()) : null);
		dto.setStatuses(statuses);
		return dto;
	}
	
	public static UserUnitSocketStatusDetailsResponse toDto(UserUnitSocketStatus userUnitSocketStatus) {
		UserUnitSocketStatusDetailsResponse dto = new UserUnitSocketStatusDetailsResponse();
		dto.setId(userUnitSocketStatus.getId());
		dto.setState(userUnitSocketStatus.getState());
		dto.setUpdateDate(userUnitSocketStatus.getUpdateDate());
		dto.setSocketId(userUnitSocketStatus.getUserUnitSocket().getId());
		return dto;
	}
	
	public static DeviceDetailsResponse toDto(Device device) {
		DeviceDetailsResponse dto = new DeviceDetailsResponse();
		dto.setId(device.getId());
		dto.setName(device.getName());
		dto.setUserUnitSocketId(device.getUserUnitSocket() != null? device.getUserUnitSocket().getId() : null);
		return dto;
	}
	
	public static ScheduleDetailsResponse toDto(Schedule schedule) {
		ScheduleDetailsResponse dto = new ScheduleDetailsResponse();
		dto.setId(schedule.getId());
		dto.setName(schedule.getName());
		dto.setRepeat(schedule.getRepeat());
		dto.setTime(resolveDate(schedule.getTime()));
		dto.setTimeBeforeNotification(schedule.getTimeBeforeNotification());
		dto.setActive(schedule.isActive());
		return dto;
	}
	
	public static Date resolveDate(Date time) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(time); 
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		int mill = cal.get(Calendar.MILLISECOND);
		cal.setTime(new Date());
		cal.set(Calendar.HOUR_OF_DAY,hour);
		cal.set(Calendar.MINUTE,minute);
		cal.set(Calendar.SECOND,second);
		cal.set(Calendar.MILLISECOND,mill);

		Date d = cal.getTime();
		return d;
	}
	
}
