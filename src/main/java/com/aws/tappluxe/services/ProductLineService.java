package com.aws.tappluxe.services;

import org.springframework.http.ResponseEntity;

import com.aws.tappluxe.data.dto.requests.ProductLineCreateRequest;
import com.aws.tappluxe.data.dto.requests.ProductLineUpdateRequest;
import org.springframework.validation.BindingResult;

public interface ProductLineService {

	public ResponseEntity<?> all();
	
	public ResponseEntity<?> create(ProductLineCreateRequest productLineCreate, BindingResult result);
	
	public ResponseEntity<?> show(Long id);
	
	public ResponseEntity<?> update(Long id, ProductLineUpdateRequest productLineUpdate, BindingResult result);
	
	public ResponseEntity<?> delete(Long id);
	
}
