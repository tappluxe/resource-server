package com.aws.tappluxe.services;

import com.aws.tappluxe.data.entities.QueuedEmail;

public interface EmailService {
	
	public QueuedEmail createEmail(String to, String subject, String body);
	
	public boolean sendSimpleMessage(String to, String subject, String body);
}
