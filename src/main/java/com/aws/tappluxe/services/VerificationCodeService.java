package com.aws.tappluxe.services;

import org.springframework.http.ResponseEntity;
import com.aws.tappluxe.data.entities.VerificationCode;

public interface VerificationCodeService {
	
	public VerificationCode generateVerificationCode(String email);
	
	public ResponseEntity<?> checkVerificationCode(String code, String email);
	
	public ResponseEntity<?> resendCode(String email);
	
	public ResponseEntity<?> isVerified(String email);

}
