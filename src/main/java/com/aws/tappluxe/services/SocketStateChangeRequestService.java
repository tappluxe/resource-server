package com.aws.tappluxe.services;

import com.aws.tappluxe.data.entities.SocketStateChangeRequest;
import com.aws.tappluxe.data.entities.User;
import com.aws.tappluxe.data.entities.UserUnitSocket;

/**
 * Created by franc.margallo on 3/18/2017.
 */
public interface SocketStateChangeRequestService {

    public SocketStateChangeRequest create(User user, UserUnitSocket unitSocket, Boolean state);

    public SocketStateChangeRequest show(Long requestId, Boolean timedOut);

    public byte execute(SocketStateChangeRequest request) throws InterruptedException;

    public boolean readyToAccept(UserUnitSocket unitSocket);

    public void timedOut(Long requestId);

}
