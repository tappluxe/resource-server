package com.aws.tappluxe.services;

import java.util.concurrent.CompletableFuture;

import com.aws.tappluxe.data.entities.UserUnit;
import org.springframework.http.ResponseEntity;

import com.aws.tappluxe.data.dto.requests.UserUnitCreateRequest;
import com.aws.tappluxe.data.dto.requests.UserUnitUpdateRequest;
import org.springframework.validation.BindingResult;

public interface UserUnitService {
	
	public ResponseEntity<?> all(String email);
	
	public ResponseEntity<?> create(UserUnitCreateRequest userUnitCreate, String email, BindingResult result);
	
	public ResponseEntity<?> show(Long id, String email);
	
	public ResponseEntity<?> update(Long id, String email, UserUnitUpdateRequest userUnitUpdate, BindingResult result);
	
	public ResponseEntity<?> delete(Long id, String email);

	public ResponseEntity<?> availability(Long id, String email);
	
}
