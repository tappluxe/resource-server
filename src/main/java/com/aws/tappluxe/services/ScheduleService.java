package com.aws.tappluxe.services;

import com.aws.tappluxe.data.dto.requests.ScheduleUpdateRequest;
import org.springframework.http.ResponseEntity;

import com.aws.tappluxe.data.dto.requests.ScheduleCreateRequest;
import org.springframework.validation.BindingResult;

public interface ScheduleService {

	public ResponseEntity<?> all(String email);
	
	public ResponseEntity<?> create(ScheduleCreateRequest scheduleCreate, String email, BindingResult result);
	
	public ResponseEntity<?> show(Long id, String email);
	
	public ResponseEntity<?> update(Long id, String email, ScheduleUpdateRequest scheduleUpdate);
	
	public ResponseEntity<?> delete(Long id, String email);
	
}
