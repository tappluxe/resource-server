package com.aws.tappluxe.services;

import com.aws.tappluxe.data.entities.UserUnit;
import com.aws.tappluxe.data.entities.UserUnitAvailabilityRequest;

/**
 * Created by franc.margallo on 3/21/2017.
 */
public interface UserUnitAvailabilityRequestService {

    public UserUnitAvailabilityRequest create(UserUnit userUnit);

    public UserUnitAvailabilityRequest show(Long requestId);

    public boolean readyToAccept(UserUnit userUnit);

}
