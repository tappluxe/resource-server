package com.aws.tappluxe.services;

import org.springframework.http.ResponseEntity;

public interface ManufacturedUnitService {

	public ResponseEntity<?> all();
	
	public ResponseEntity<?> create(Long productLineId);
	
	public ResponseEntity<?> show(Long id);

	public ResponseEntity<?> delete(Long id);
	
}
