package com.aws.tappluxe.services;

import org.springframework.http.ResponseEntity;

import com.aws.tappluxe.data.dto.requests.DeviceCreateRequest;
import com.aws.tappluxe.data.dto.requests.DeviceUpdateRequest;
import org.springframework.validation.BindingResult;

public interface DeviceService {

	public ResponseEntity<?> all(Boolean attachedTo, String email);
	
	public ResponseEntity<?> create(DeviceCreateRequest deviceCreate, String email, BindingResult result);
	
	public ResponseEntity<?> show(Long id, String email);

	public ResponseEntity<?> update(Long id, String email, DeviceUpdateRequest updateRequest, BindingResult result);
	
	public ResponseEntity<?> delete(Long id, String email);
	
}
