package com.aws.tappluxe.services;

import com.aws.tappluxe.data.entities.UserUnit;
import com.aws.tappluxe.data.entities.UserUnitSocket;
import com.aws.tappluxe.data.entities.UserUnitSocketStatus;
import org.springframework.http.ResponseEntity;
/**
 * Created by franc.margallo on 3/11/2017.
 */
public interface UserUnitSocketService {

    public void create(UserUnit userUnit, Integer numOfSockets);

    public ResponseEntity<?> show(Long userUnit, Long unitSocketId, String email);

    public ResponseEntity<?> changeState(Long userUnitId, Long userUnitSocketId, Boolean state, String email);

    public ResponseEntity<?> manageSchedule(Long userUnitId, Long userUnitSocketId, Long scheduleId, String email);

    public UserUnitSocketStatus pushState(UserUnitSocket unitSocket, Boolean state);

}
