package com.aws.tappluxe.services;

import com.aws.tappluxe.data.entities.SocketStateChangeRequest;
import com.aws.tappluxe.data.entities.UserUnitAvailabilityRequest;
import com.aws.tappluxe.data.entities.UserUnitSocket;

public interface MqttService {

	public void sendChangeSocketMessage(SocketStateChangeRequest socketStateChangeRequest);

	public void sendUnitAvailabilityStateMessage(UserUnitAvailabilityRequest userUnitAvailabilityRequest);

	public void sendCurrentSocketState(UserUnitSocket unitSocket);
	
	public void processInboundMessage(String message);
	
}
