package com.aws.tappluxe.services;

import org.springframework.http.ResponseEntity;

import com.aws.tappluxe.data.dto.requests.ResetPasswordRequest;
import com.aws.tappluxe.data.dto.requests.UserCreateRequest;
import org.springframework.validation.BindingResult;

public interface MembershipService {

	public ResponseEntity<?> register(UserCreateRequest userCreate, BindingResult result);
	
	public ResponseEntity<?> checkEmail(String email);
	
	public ResponseEntity<?> checkProductId(String productId);
	
	public ResponseEntity<?> forgotPassword(String email);
	
	public ResponseEntity<?> resetPassword(ResetPasswordRequest request);
	
}
